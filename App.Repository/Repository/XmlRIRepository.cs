using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IXmlRIRepository
    {

    }
    public sealed class XmlRIRepository : Repository<XmlRI>, IXmlRIRepository
    {
        public XmlRIRepository () : base("XmlRI", "ID") { }

        internal override dynamic Mapping(XmlRI item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				NOBUKMSK = item.NOBUKMSK,
				TGBUKMSK = item.TGBUKMSK,
				KOBAR = item.KOBAR,
				QTY = item.QTY,
				DownloadDate = item.DownloadDate,


            };
        }

        //do some override here

    }

}
