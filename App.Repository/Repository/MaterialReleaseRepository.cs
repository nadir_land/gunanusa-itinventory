using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IMaterialReleaseRepository
    {

    }
    public sealed class MaterialReleaseRepository : Repository<TrMaterialRelease>, IMaterialReleaseRepository
    {
        public MaterialReleaseRepository () : base("TrMaterialRelease", "ID") { }

        internal override dynamic Mapping(TrMaterialRelease item)
        {
			return new
			{
				//property here..

				ID = item.ID,
				OpsiProduksi = item.OpsiProduksi,
				NoWO = item.NoWO,
				TglWO = item.TglWO,
				NoRelease = item.NoRelease,
				TglRelease = item.TglRelease,
				KodeSubkon = item.KodeSubkon,
				NamaSubkon = item.NamaSubkon,
				Alamat = item.Alamat,
				Keterangan = item.Keterangan,
				KodeFG = item.KodeFG,
				NamaFG = item.NamaFG,
				HSExport = item.HSExport,
				SatuanExport = item.SatuanExport,
				QtyOrder = item.QtyOrder,
				KodeBarang = item.KodeBarang,
				NamaBarang = item.NamaBarang,
				Satuan = item.Satuan,
				HSImport = item.HSImport,
				Dibutuhkan = item.Dibutuhkan,
				Terpakai = item.Terpakai,
				Dikeluarkan = item.Dikeluarkan,
				BelumKirim = item.BelumKirim,
				Status = item.Status,


            };
        }

        //do some override here

    }

}
