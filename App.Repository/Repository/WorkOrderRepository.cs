using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IWorkOrderRepository
    {

    }
    public sealed class WorkOrderRepository : Repository<TrWorkOrder>, IWorkOrderRepository
    {
        public WorkOrderRepository () : base("TrWorkOrder", "ID") { }

        internal override dynamic Mapping(TrWorkOrder item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				NoWO = item.NoWO,
				TglWO = item.TglWO,
				KodeCustomer = item.KodeCustomer,
				NamaCustomer = item.NamaCustomer,
				Alamat = item.Alamat,
				NegaraTujuan = item.NegaraTujuan,
				KodeFG = item.KodeFG,
				NamaFG = item.NamaFG,
				HSExport = item.HSExport,
				SatuanExport = item.SatuanExport,
				Foto = item.Foto,
				QtyOrder = item.QtyOrder,
				KodeBarang = item.KodeBarang,
				NamaBarang = item.NamaBarang,
				HSImport = item.HSImport,
				Satuan = item.Satuan,
				Koefisien = item.Koefisien,
				Dibutuhkan = item.Dibutuhkan,
				Status = item.Status,


            };
        }

        //do some override here

    }

}
