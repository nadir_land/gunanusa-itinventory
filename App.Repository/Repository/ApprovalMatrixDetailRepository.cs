﻿using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IApprovalMatrixDetailRepository
    {

    }
    public class ApprovalMatrixDetailRepository : Repository<MsApprovalMatrixDetail>, IApprovalMatrixDetailRepository
    {
        public ApprovalMatrixDetailRepository() : base("MsApprovalMatrixDetail", "ID")
        {
        }

        internal override dynamic Mapping(MsApprovalMatrixDetail item)
        {
            return new
            {
                //property here.. 
                ID = item.ID,
                MatrixID = item.MatrixID,
                RoleID = item.RoleID,
                Sequential = item.Sequential,
                Operator = item.Operator,
                DestinationRole = item.DestinationRole,
                TblCode = item.TblCode,
                Notes = item.Notes,
                IsValidateMember = item.IsValidateMember,
                RoleValidate = item.RoleValidate,
                SLA= item.SLA,
                UnitOfTime = item.UnitOfTime


            };
        }
    }   
}
