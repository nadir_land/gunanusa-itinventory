using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IPenerimaanBarangRepository
    {

    }
    public sealed class PenerimaanBarangRepository : Repository<TrPenerimaanBarang>, IPenerimaanBarangRepository
    {
        public PenerimaanBarangRepository () : base("TrPenerimaanBarang", "ID") { }

        internal override dynamic Mapping(TrPenerimaanBarang item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				ReceiptNo = item.ReceiptNo,
				ReceiptDate = item.ReceiptDate,
				Warehouse = item.Warehouse,
				CAR = item.CAR,
				NoPIB = item.NoPIB,
				TanggalPIB = item.TanggalPIB,
				KPBC = item.KPBC,
				AsalBarang = item.AsalBarang,
				Valuta = item.Valuta,
				Rate = item.Rate,
				Status = item.Status,
				KodeBarang = item.KodeBarang,
				NamaBarang = item.NamaBarang,
				HSImport = item.HSImport,
				Satuan = item.Satuan,
				NoSeri = item.NoSeri,
				TarifBM = item.TarifBM,
				CifPIB = item.CifPIB,
				QtyPIB = item.QtyPIB,
				BmPIB = item.BmPIB,
				PpnPIB = item.PpnPIB,
				QtyRI = item.QtyRI,


            };
        }

        //do some override here

    }

}
