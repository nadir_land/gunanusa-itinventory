using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IPenerimaanBarangItemRepository
    {

    }
    public sealed class PenerimaanBarangItemRepository : Repository<TrPenerimaanBarangItem>, IPenerimaanBarangItemRepository
    {
        public PenerimaanBarangItemRepository () : base("TrPenerimaanBarangItem", "ID") { }

        internal override dynamic Mapping(TrPenerimaanBarangItem item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				PenerimaanBarangID = item.PenerimaanBarangID,
                KodeBarang = item.KodeBarang,
				NamaBarang = item.NamaBarang,
				HSImport = item.HSImport,
				Satuan = item.Satuan,
				NoSeri = item.NoSeri,
				TarifBM = item.TarifBM,
				CifPIB = item.CifPIB,
				QtyPIB = item.QtyPIB,
				BmPIB = item.BmPIB,
				PpnPIB = item.PpnPIB,
				QtyRI = item.QtyRI,


            };
        }

        //do some override here

    }

}
