using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface ILogRepository
    {

    }
    public sealed class LogRepository : Repository<TrLog>, ILogRepository
    {
        public LogRepository () : base("TrLog", "ID") { }

        internal override dynamic Mapping(TrLog item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				Username = item.Username,
				LogTime = item.LogTime,
				ModulName = item.ModulName,
				Activity = item.Activity,


            };
        }

        //do some override here

    }

}
