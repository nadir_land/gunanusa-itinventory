using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IVendorRepository
    {

    }
    public sealed class VendorRepository : Repository<MsVendor>, IVendorRepository
    {
        public VendorRepository () : base("MsVendor", "ID") { }

        internal override dynamic Mapping(MsVendor item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				KodeVendor = item.KodeVendor,
				NamaVendor = item.NamaVendor,
				AlamatVendor = item.AlamatVendor,
				Negara = item.Negara,
				Telp = item.Telp,
				Fax = item.Fax,
				ContactPerson = item.ContactPerson,
				Status = item.Status,


            };
        }

        //do some override here

    }

}
