using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IDeliveryOrderDetailRepository
    {

    }
    public sealed class DeliveryOrderDetailRepository : Repository<TrDeliveryOrderDetail>, IDeliveryOrderDetailRepository
    {
        public DeliveryOrderDetailRepository () : base("TrDeliveryOrderDetail", "ID") { }

        internal override dynamic Mapping(TrDeliveryOrderDetail item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				DOID = item.DOID,
				KodeFG = item.KodeFG,
				NamaFG = item.NamaFG,
				HSExport = item.HSExport,
				Satuan = item.Satuan,
				QtyKirim = item.QtyKirim,
				HargaSatuan = item.HargaSatuan,
				JumlahHarga = item.JumlahHarga,


            };
        }

        //do some override here

    }

}
