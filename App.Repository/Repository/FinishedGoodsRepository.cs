using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IFinishedGoodsRepository
    {

    }
    public sealed class FinishedGoodsRepository : Repository<MsFinishedGoods>, IFinishedGoodsRepository
    {
        public FinishedGoodsRepository () : base("MsFinishedGoods", "ID") { }

        internal override dynamic Mapping(MsFinishedGoods item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				KodeFG = item.KodeFG,
				NamaFG = item.NamaFG,
				GudangFG = item.GudangFG,
				HSExport = item.HSExport,
				Satuan = item.Satuan,
				Keterangan = item.Keterangan,
				Foto = item.Foto,


            };
        }

        //do some override here

    }

}
