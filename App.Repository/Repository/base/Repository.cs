﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Expressions;
using Dapper;
using RepoWrapper;
using System.Data;
using App.Utility;
using App.Utility.Utility;
using System.Reflection;

namespace App.Repository
{
    /// <summary>

    /// Base repository that wraps the Dapper Micro ORM

    /// </summary>

    /// <typeparam name="T"></typeparam>

    /// <remarks>

    /// For more information regarding Dapper see: http://code.google.com/p/dapper-dot-net/

    /// </remarks>

    public abstract class Repository<T> : IRepository<T> where T : EntityBase, IAggregateRoot

    {

        /// <summary>

        /// The _table name

        /// </summary>

        private readonly string _tableName;
        private readonly string _primaryKey;
        protected int commandTimeOut= 7200;



        /// <summary>

        /// Gets the connection.

        /// </summary>

        /// <value>

        /// The connection.

        /// </value>

        internal System.Data.IDbConnection Connection

        {

            get

            {

                return new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);

            }

        }



        /// <summary>

        /// Initializes a new instance of the <see cref="Repository{T}" /> class.

        /// </summary>

        /// <param name="tableName">Name of the table.</param>
        /// <param name="primaryKey">Primary Key of the table.</param>

        public Repository(string tableName, string primaryKey)

        {

            _tableName = tableName;
            _primaryKey = primaryKey;

        }



        /// <summary>

        /// Mapping the object to the insert/update columns.

        /// </summary>

        /// <param name="item">The item.</param>

        /// <returns>The parameters with values.</returns>

        /// <remarks>In the default case, we take the object as is with no custom mapping.</remarks>

        internal virtual dynamic Mapping(T item)

        {

            return item;

        }



        /// <summary>

        /// Adds the specified item.

        /// </summary>

        /// <param name="item">The item.</param>

        public virtual int Add(T item)
        {
            int result = 0;
            try
            {
                using (IDbConnection cn = Connection)
                {
                    var parameters = (object)Mapping(item);
                    cn.Open();
                    int rs = cn.Insert<int>(_tableName, parameters, _primaryKey);
                    result = rs;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
            
            return result;
        }



        /// <summary>

        /// Updates the specified item.

        /// </summary>

        /// <param name="item">The item.</param>

        public virtual int Update(T item)

        {

            int result = 0;
            try
            {
                using (IDbConnection cn = Connection)
                {
                    var parameters = (object)Mapping(item);
                    cn.Open();
                    cn.Update(_tableName, parameters, _primaryKey);
                    result = 1;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;            

        }



        /// <summary>

        /// Removes the specified item.

        /// </summary>

        /// <param name="item">The item.</param>

        public virtual int Remove(int Id)
        {

            int result = 0;
            try
            {
                using (IDbConnection cn = Connection)
                {
                    cn.Open();

                    cn.Execute("DELETE FROM " + _tableName + " WHERE "+_primaryKey+"=@ID", new { ID = Id });

                    result = 1;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            return result;
            
        }



        /// <summary>

        /// Finds by ID.

        /// </summary>

        /// <param name="id">The id.</param>

        /// <returns></returns>

        public virtual T FindByID(int id)

        {

            T item = default(T);



            using (IDbConnection cn = Connection)

            {

                cn.Open();

                item = cn.Query<T>("SELECT * FROM " + _tableName + " WHERE "+ _primaryKey +"=@ID", new { ID = id }).SingleOrDefault();

            }



            return item;

        }


        public virtual T SingelOrDefault(string param, string OrderBy, string AscDesc)
        {
            T item = default(T);



            using (IDbConnection cn = Connection)

            {

                cn.Open();

                item = cn.Query<T>("SELECT * FROM " + _tableName + " WHERE " + param + "Order By " + OrderBy + AscDesc).SingleOrDefault();

            }



            return item;
        }

        /// <summary>

        /// Finds the specified predicate.

        /// </summary>

        /// <param name="predicate">The predicate.</param>

        /// <returns>A list of items</returns>

        public virtual IEnumerable<T> Find(Expression<Func<T, bool>> predicate)

        {

            IEnumerable<T> items = null;



            // extract the dynamic sql query and parameters from predicate

            QueryResult result = DynamicQuery.GetDynamicQuery(_tableName, predicate);



            using (IDbConnection cn = Connection)

            {

                cn.Open();

                items = cn.Query<T>(result.Sql, (object)result.Param);

            }



            return items;

        }

        protected Dictionary<string, string> fields = new Dictionary<string, string>();
        protected virtual string getField(string field)
        {
            string result = field;
            if (fields.ContainsKey(field.ToLower()))
                result = fields[field.ToLower()];
            return result;
        }

        public virtual IEnumerable<T> Find(List<WhereClause> whereClauses = null, string orderBy="ID")
        {
            string wh = "";
            if (whereClauses != null)
            {
                List<string> listWhere = new List<string>();
                foreach (var whereClause in whereClauses)
                {
                    if (whereClause.SqlOperator.ToLower().Contains("like"))
                        listWhere.Add(string.Format("{0} {1} '%'+@{2}+'%'", whereClause.Property, whereClause.SqlOperator, whereClause.Property));
                    else
                        listWhere.Add(string.Format("{0} {1} @{2}", whereClause.Property, whereClause.SqlOperator, whereClause.Property));
                }
                wh = string.Join(" and ", listWhere.ToArray());
            }
            return FindByCriteria(wh, whereClauses, orderBy);
        }

        public virtual IEnumerable<T> FindByCriteria(string criteria, List<WhereClause> whereClauses, string orderBy)
        {
            
            IEnumerable<T> items = null;
            try
            {
                var q = @"select * from " + _tableName + " WITH (NOLOCK)";

                q += @"where 1=1 {0} order by {1}";

                string wh = "";
                var param = new DynamicParameters();
                if (!string.IsNullOrEmpty(criteria))
                {

                    var index = 0;
                    foreach (var whereClause in whereClauses)
                    {
                        var property = getField(whereClause.Property);
                        criteria = criteria.Replace(whereClause.Property, property);
                        if (whereClause.SqlOperator.ToLower() == "in")
                        {
                            var paramValues = whereClause.Value.ToString().Split(',');
                            int cnt = 0;
                            foreach (var paramValue in paramValues)
                            {
                                param.Add("@" + property + cnt.ToString(), paramValue, null, null, null);
                                cnt++;
                            }

                        }
                        else
                            //param.Add("@" + property + index.ToString(), whereClause.Value, null, null, null);
                            param.Add("@" + property, whereClause.Value, null, null, null);
                        index++;
                    }

                    wh = " and " + criteria;
                }
                q = string.Format(q, wh, orderBy);

                using (IDbConnection cn = Connection)
                {
                    cn.Open();
                    items = cn.Query<T>(q, param, null, true, commandTimeOut, CommandType.Text).ToList();
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            return items;
        }


        /// <summary>

        /// Finds all.

        /// </summary>

        /// <returns>All items</returns>

        public virtual IEnumerable<T> FindAll()

        {

            IEnumerable<T> items = null;



            using (IDbConnection cn = Connection)

            {

                cn.Open();

                items = cn.Query<T>("SELECT * FROM " + _tableName);

            }
            
            return items;

        }

        /// <summary>

        /// Finds all With Paging.

        /// </summary>

        /// <returns>All items</returns>
        public virtual IEnumerable<T> FindWithPaging(string criteria,
            List<WhereClause> whereClauses,
            int pageSize = 10,            
            int page = 1,
            string orderBy = "ID")
        {
            int lower = (page - 1) * pageSize;
            int upper = lower + pageSize;

            IEnumerable<T> items = null;
            try
            {
                var q = @"select * from "+_tableName+" WITH (NOLOCK)";
                                
                q += @"where 1=1 {2}
                            order by {3}
                            OFFSET {0} ROWS
							FETCH NEXT {1} ROWS ONLY";

                string wh = "";
                var param = new DynamicParameters();
                if (!string.IsNullOrEmpty(criteria))
                {
                    var index = 0;
                    foreach (var whereClause in whereClauses)
                    {
                        var property = getField(whereClause.Property);
                        criteria = criteria.Replace(whereClause.Property, property);
                        if (whereClause.SqlOperator.ToLower() == "in")
                        {
                            var paramValues = whereClause.Value.ToString().Split(',');
                            int cnt = 0;
                            foreach (var paramValue in paramValues)
                            {
                                param.Add("@" + property + cnt.ToString(), paramValue, null, null, null);
                                cnt++;
                            }

                        }
                        else
                            //param.Add("@" + property + index.ToString(), whereClause.Value, null, null, null);
                            param.Add("@" + property, whereClause.Value, null, null, null);
                        index++;
                    }

                    wh = " and " + criteria;
                }
                q = string.Format(q, lower, upper, wh, orderBy);

                using (IDbConnection cn = Connection)

                {

                    cn.Open();
                    items = cn.Query<T>(q, param, null, true, commandTimeOut, CommandType.Text).ToList();                    
                }

                
            }
            catch (Exception e)
            {
                throw e;
            }
            return items;
        }

        public int Count(string criteria, List<WhereClause> whereClauses)
        {
            int count = 0;
            try
            {
                var q = @"SELECT COUNT ("+ _primaryKey +") as [Count] FROM "+_tableName+ " WITH (NOLOCK) WHERE 1=1 {0}";


                var param = new DynamicParameters();
                if (!string.IsNullOrEmpty(criteria))
                {
                    var index = 0;
                    foreach (var whereClause in whereClauses)
                    {
                        var property = getField(whereClause.Property);
                        criteria = criteria.Replace(whereClause.Property, property);
                        if (whereClause.SqlOperator.ToLower() == "in")
                        {
                            var paramValues = whereClause.Value.ToString().Split(',');
                            int cnt = 0;
                            foreach (var paramValue in paramValues)
                            {
                                param.Add("@" + property + cnt.ToString(), paramValue, null, null, null);
                                cnt++;
                            }

                        }
                        else
                            //param.Add("@" + property + index.ToString(), whereClause.Value, null, null, null);
                            param.Add("@" + property, whereClause.Value, null, null, null);
                        index++;
                    }

                    criteria = " and " + criteria;
                }
                

                q = string.Format(q, criteria);

                using (IDbConnection cn = Connection)
                {
                    cn.Open();
                    count = cn.Query<CountDomain>(q, param).Take(1).SingleOrDefault().Count;
                }                
            }
            catch (Exception e)
            {
                throw e;
            }
            return count;

        }


    }

    public static class DapperExtensions
    {
        public static T Insert<T>(this IDbConnection cnn, string tableName, dynamic param, string primaryKey)
        {
            IEnumerable<T> result = SqlMapper.Query<T>(cnn, DynamicQuery.GetInsertQuery(tableName, param, primaryKey), param);
            return result.First();
        }

        public static void Update(this IDbConnection cnn, string tableName, dynamic param, string primaryKey)
        {
            SqlMapper.Execute(cnn, DynamicQuery.GetUpdateQuery(tableName, param, primaryKey), param);
        }
    }
}
