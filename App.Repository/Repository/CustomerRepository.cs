using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface ICustomerRepository
    {

    }
    public sealed class CustomerRepository : Repository<MsCustomer>, ICustomerRepository
    {
        public CustomerRepository () : base("MsCustomer", "ID") { }

        internal override dynamic Mapping(MsCustomer item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				KodeCustomer = item.KodeCustomer,
				NamaCustomer = item.NamaCustomer,
				AlamatCustomer = item.AlamatCustomer,
				Negara = item.Negara,
				Telp = item.Telp,
				Fax = item.Fax,
				Contact = item.Contact,
				Status = item.Status,


            };
        }

        //do some override here

    }

}
