using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IXmlPMRRepository
    {

    }
    public sealed class XmlPMRRepository : Repository<XmlPMR>, IXmlPMRRepository
    {
        public XmlPMRRepository () : base("XmlPMR", "ID") { }

        internal override dynamic Mapping(XmlPMR item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				NOORDPR = item.NOORDPR,
				NOFGMSK = item.NOFGMSK,
				TGFGMSK = item.TGFGMSK,
				KDPART = item.KDPART,
				QTY = item.QTY,
				DownloadDate = item.DownloadDate,


            };
        }

        //do some override here

    }

}
