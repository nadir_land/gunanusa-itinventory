﻿using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IMenuRepository
    {

    }
    public sealed class MenuRepository : Repository<MsMenus>, IMenuRepository
    {
        public MenuRepository() : base("MsMenu", "ID") { }

        internal override dynamic Mapping(MsMenus item)
        {
            return new
            {
                //property here.. 
                ID = item.ID,
                Link = item.Link,
                DisplayMenu = item.DisplayMenu,
                ClassIcon = item.ClassIcon,
                Parent = item.Parent,
                TypeMenu = item.TypeMenu
            };
        }

    }
}
