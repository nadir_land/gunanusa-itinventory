using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IXmlBOMRepository
    {

    }
    public sealed class XmlBOMRepository : Repository<XmlBOM>, IXmlBOMRepository
    {
        public XmlBOMRepository () : base("XmlBOM", "ID") { }

        internal override dynamic Mapping(XmlBOM item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				BOMNO = item.BOMNO,
				KDPART = item.KDPART,
				BOMQTY = item.BOMQTY,
				KOBAR = item.KOBAR,
				QTY = item.QTY,
				DownloadDate = item.DownloadDate,


            };
        }

        //do some override here

    }

}
