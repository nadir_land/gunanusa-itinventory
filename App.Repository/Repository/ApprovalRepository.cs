﻿using App.Repository.Domain;
using App.Utility.Utility;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{

    public interface IApprovalRepository
    {

    }
    public class ApprovalRepository : Repository<TrApproval>, IApprovalRepository
    {
        public ApprovalRepository() : base("TrApproval", "ID")
        {
        }

        internal override dynamic Mapping(TrApproval item)
        {
            return new
            {
                //property here.. 
                ID = item.ID,
                TransactionID = item.TransactionID,
                TypeTransaction = item.TypeTransaction,
                MatrixID = item.MatrixID,
                DestinationUser = item.DestinationUser,
                CreatedOn = item.CreatedOn,
                SequentialDetail = item.SequentialDetail,
                SequentialMatrix = item.SequentialMatrix,
                StatusDesc=item.StatusDesc,
                CreatedBy = item.CreatedBy,
                StatusID = item.StatusID,
                SubmitToSAPDate=item.SubmitToSAPDate

            };
        }
       
    }
}
