using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IXmlDORepository
    {

    }
    public sealed class XmlDORepository : Repository<XmlDO>, IXmlDORepository
    {
        public XmlDORepository () : base("XmlDO", "ID") { }

        internal override dynamic Mapping(XmlDO item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				NOFGKEL = item.NOFGKEL,
				TGFGKL = item.TGFGKL,
				KDPART = item.KDPART,
				QTY = item.QTY,
				DownloadDate = item.DownloadDate,


            };
        }

        //do some override here

    }

}
