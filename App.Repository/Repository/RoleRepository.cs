﻿using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IRoleRepository
    {

    }
    public sealed class RoleRepository : Repository<MsRole>, IRoleRepository
    {
        public RoleRepository() : base("MsRole","ID") { }

        internal override dynamic Mapping(MsRole item)
        {
            return new
            {
                //property here.. 
                ID = item.ID,               
                RoleName = item.RoleName              
            };
        }

    }
}
