using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IXmlWORepository
    {

    }
    public sealed class XmlWORepository : Repository<XmlWO>, IXmlWORepository
    {
        public XmlWORepository () : base("XmlWO", "ID") { }

        internal override dynamic Mapping(XmlWO item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				NOORDP = item.NOORDP,
				TGORDP = item.TGORDP,
				KDPART = item.KDPART,
				QTY = item.QTY,
				DownloadDate = item.DownloadDate,


            };
        }

        //do some override here

    }

}
