using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IXmlMRRepository
    {

    }
    public sealed class XmlMRRepository : Repository<XmlMR>, IXmlMRRepository
    {
        public XmlMRRepository () : base("XmlMR", "ID") { }

        internal override dynamic Mapping(XmlMR item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				NOORDP = item.NOORDP,
				NOSLIPRM = item.NOSLIPRM,
				TGSLIPRM = item.TGSLIPRM,
				KDPART = item.KDPART,
				KOBAR = item.KOBAR,
				QTY = item.QTY,
				DownloadDate = item.DownloadDate,


            };
        }

        //do some override here

    }

}
