﻿using App.Model;
using App.Repository.Domain;
using App.Utility.Utility;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
   
    public interface IApprovalDetailRepository
    {
        
    }
    public class ApprovalDetailRepository : Repository<TrApprovalDetail>, IApprovalDetailRepository
    {
        public ApprovalDetailRepository() : base("TrApprovalDetail", "ID")
        {
        }

        internal override dynamic Mapping(TrApprovalDetail item)
        {
            return new
            {
                //property here.. 
                ID = item.ID,
                ApprovalID = item.ApprovalID,
                UserID = item.UserID,
                UserName = item.UserName,
                RoleID = item.RoleID,
                RoleName = item.RoleName,
                OrganizationName = item.OrganizationName,
                StartDate = item.StartDate,
                EndDate = item.EndDate,
                SequentialDetail = item.SequentialDetail,
                SequentialMatrix = item.SequentialMatrix,
                MatrixID = item.MatrixID,
                StatusApproval = item.StatusApproval,
                Operator = item.Operator,
                NoteApproval=item.NoteApproval


            };
        }

       
    }
}
