﻿using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IApprovalMatrixRepository
    {

    }
    public class ApprovalMatrixRepository : Repository<MsApprovalMatrix>, IApprovalMatrixRepository
    {
        public ApprovalMatrixRepository() : base("MsApprovalMatrix", "ID")
        {
        }

        internal override dynamic Mapping(MsApprovalMatrix item)
        {
            return new
            {
                //property here.. 
                ID = item.ID,
                PRType = item.PRType,
                PRCategory = item.PRCategory,
                PRJType = item.PRJType,
                Nominal = item.Nominal,
                Min = item.Min,
                Max = item.Max
            };
        }
    }
}
