﻿using App.Repository.Domain;

namespace App.Repository.Repository
{
    public interface IUserRepository
    {

    }
    public sealed class UserRepository : Repository<MsUser>, IUserRepository
    {
        public UserRepository() : base("MsUser", "ID") { }

        internal override dynamic Mapping(MsUser item)
        {
            return new
            {
                //property here..
                ID = item.ID,
                Username = item.Username,
                Password = item.Password,
                NPK = item.NPK,
                FullName = item.FullName,
                Department = item.Department,
                Email = item.Email,
                IsLocked = item.IsLocked,
                IsOnline = item.IsOnline,
                CreatedOn = item.CreatedOn,
                CreatedBy = item.CreatedBy,
                UpdateOn = item.UpdateOn,
                UpdateBy = item.UpdateBy,
                Status = item.Status,
                UserRole = item.UserRole

            };
        }

        //do some override here

    }
}
