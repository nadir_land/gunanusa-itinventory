﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;
using App.Model;
using App.Repository.Domain;

namespace App.Repository.Repository
{
    public interface IBillingRepository
    {
        MessageModel<Billing> GetBillings();
        MessageModel<Billing> GetBillingByID(int id);
        MessageModel<Billing> InsertBilling(Billing billing);
        MessageModel<Billing> DeleteBilling(int id);
        MessageModel<Billing> EditBilling(Billing billing);
    }
    public class BillingRepository : IBillingRepository
    {
        private List<Billing> allBillings;
        private XDocument billingData;

        // constructor
        public BillingRepository()
        {
            allBillings = new List<Billing>();

            billingData = XDocument.Load(HttpContext.Current.Server.MapPath("~/App_Data/Billings.xml"));
            var billings = from billing in billingData.Descendants("item")
                           select new Billing(
                                (int)billing.Element("id"), 
                                billing.Element("customer").Value,
                                billing.Element("type").Value, 
                                (DateTime)billing.Element("date"),
                                billing.Element("description").Value, 
                                (int)billing.Element("hours")
                           );
            allBillings.AddRange(billings.ToList<Billing>());
        }

        // return a list of all billings
        public MessageModel<Billing> GetBillings()
        {
            MessageModel<Billing> message = new MessageModel<Billing>();
            IEnumerable<Billing> list = allBillings;
            message.rows = list;
            return message;
        }

        public MessageModel<Billing> GetBillingByID(int id)
        {
            MessageModel<Billing> message = new MessageModel<Billing>();
            message.data = allBillings.Find(item => item.ID == id);
            return message;
        }

        // Insert Record
        public MessageModel<Billing> InsertBilling(Billing billing)
        {
            MessageModel<Billing> message = new MessageModel<Billing>();
            try
            {
                billing.ID = (int)(from b in billingData.Descendants("item") orderby (int)b.Element("id") descending select (int)b.Element("id")).FirstOrDefault() + 1;
                billingData.Root.Add(new XElement("item", new XElement("id", billing.ID), new XElement("customer", billing.Customer),
                    new XElement("type", billing.Type), new XElement("date", billing.Date.ToShortDateString()), new XElement("description", billing.Description),
                    new XElement("hours", billing.Hours)));
                billingData.Save(HttpContext.Current.Server.MapPath("~/App_Data/Billings.xml"));
                message.success=true;
            }
            catch(Exception e)
            {
                message.message = "Failed Save data. error detail:" + e.Message;
                message.success = false;
            }            
            return message;
        }

        // Delete Record
        public MessageModel<Billing> DeleteBilling(int id)
        {
            MessageModel<Billing> message = new MessageModel<Billing>();
            try
            {
                billingData.Root.Elements("item").Where(i => (int)i.Element("id") == id).Remove();

                billingData.Save(HttpContext.Current.Server.MapPath("~/App_Data/Billings.xml"));
                message.success = true;
            }
            catch (Exception e)
            {
                message.message = "Failed Save data. error detail:" + e.Message;
                message.success = false;
            }
            return message;
        }

        // Edit Record
        public MessageModel<Billing> EditBilling(Billing billing)
        {
            MessageModel<Billing> message = new MessageModel<Billing>();
            try
            {
                XElement node = billingData.Root.Elements("item").Where(i => (int)i.Element("id") == billing.ID).FirstOrDefault();
                node.SetElementValue("customer", billing.Customer);
                node.SetElementValue("type", billing.Type);
                node.SetElementValue("date", billing.Date.ToShortDateString());
                node.SetElementValue("description", billing.Description);
                node.SetElementValue("hours", billing.Hours);

                billingData.Save(HttpContext.Current.Server.MapPath("~/App_Data/Billings.xml"));
                message.success = true;
            }
            catch (Exception e)
            {
                message.message = "Failed Save data. error detail:" + e.Message;
                message.success = false;
            }
            return message;

        }
    }
}
