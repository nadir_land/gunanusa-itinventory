using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IKonversiProductRepository
    {

    }
    public sealed class KonversiProductRepository : Repository<TrKonversiProduct>, IKonversiProductRepository
    {
        public KonversiProductRepository () : base("TrKonversiProduct", "ID") { }

        internal override dynamic Mapping(TrKonversiProduct item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				KodeFG = item.KodeFG,
				NamaFG = item.NamaFG,
				BOM = item.BOM,
				HSExport = item.HSExport,
				SatuanExport = item.SatuanExport,
				Keterangan = item.Keterangan,
				Foto = item.Foto,
				KodeBarang = item.KodeBarang,
				NamaBarang = item.NamaBarang,
				HSImport = item.HSImport,
				Satuan = item.Satuan,
				Koefisien = item.Koefisien,
				Kandungan = item.Kandungan,
				Waste = item.Waste,
				Status = item.Status,
				QtyBom = item.QtyBom,
				QtyBarang = item.QtyBarang,


            };
        }

        //do some override here

    }

}
