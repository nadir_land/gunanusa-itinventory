﻿using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Model;
using Dapper;
using System.Data;

namespace App.Repository.Repository
{

    public interface IUserRoleRepository
    {
        IEnumerable<UserRoleAdvanceModel> ListUsrRoleMenu();
    }
    public sealed class UserRoleRepository : Repository<MsUserRole>, IUserRoleRepository
    {
        public UserRoleRepository() : base("MsUserRole", "ID") { }
        
        internal override dynamic Mapping(MsUserRole item)
        {
            return new
            {
                //property here..
                ID = item.ID,
                RoleID = item.RoleID,
                UserID = item.UserID,
                CreatedOn = item.CreatedOn,
                CreatedBy = item.CreatedBy,
                UpdateOn = item.UpdateOn,
                UpdateBy = item.UpdateBy,
                Deleted = item.Deleted                               
            };
        }

        #region Sample for Overide Base

        public IEnumerable<UserRoleAdvanceModel> ListUsrRoleMenu()
        {
            string sql = @"Select a.ID, a.UserID,a.RoleID, b.Username, c.Rolename, e.DisplayMenu  
                            from MsUserRole a 
                            inner join MsUser b on a.UserID=b.ID
                            inner join MsRole c on a.RoleID=c.ID 
                            inner join MsRoleMenu d on c.Id=d.RoleID
                            inner join MsMenu e on d.MenuID=e.ID";

            IEnumerable<UserRoleAdvanceModel> items = null;
            try
            {
                                
                using (IDbConnection cn = Connection)
                {
                    cn.Open();
                    items = cn.Query<UserRoleAdvanceModel>(sql, null, null, true, commandTimeOut, CommandType.Text).ToList();
                }


            }
            catch (Exception e)
            {
                throw e;
            }
            return items;

        }

        #endregion
    }
}
