using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IFinishGoodsInResultRepository
    {

    }
    public sealed class FinishGoodsInResultRepository : Repository<TrFinishGoodsInResult>, IFinishGoodsInResultRepository
    {
        public FinishGoodsInResultRepository () : base("TrFinishGoodsInResult", "ID") { }

        internal override dynamic Mapping(TrFinishGoodsInResult item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				OpsiProduksi = item.OpsiProduksi,
				NoWO = item.NoWO,
				TglWO = item.TglWO,
				NoResult = item.NoResult,
				TglResult = item.TglResult,
				KodeSubkon = item.KodeSubkon,
				NamaSubkon = item.NamaSubkon,
				Alamat = item.Alamat,
				Keterangan = item.Keterangan,
				KodeFG = item.KodeFG,
				NamaFG = item.NamaFG,
				HSExport = item.HSExport,
				SatuanExport = item.SatuanExport,
				QtyOrder = item.QtyOrder,
				SudahKirim = item.SudahKirim,
				AkanKirim = item.AkanKirim,
				DlmProses = item.DlmProses,
                Status = item.Status

            };
        }

        //do some override here

    }

}
