﻿using App.Repository.Domain;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IRoleMenuRepository
    {
                
    }
    public sealed class RoleMenuRepository : Repository<MsRoleMenu>, IRoleMenuRepository
    {
        public RoleMenuRepository() : base("MsRoleMenu", "ID") { }
        
        internal override dynamic Mapping(MsRoleMenu item)
        {
            return new
            {
                //property here..
                ID = item.ID,
                RoleID = item.RoleID,
                MenuID = item.MenuID                
            };
        }

    }
}
