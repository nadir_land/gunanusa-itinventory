using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IRawMaterialRepository
    {

    }
    public sealed class RawMaterialRepository : Repository<MsRawMaterial>, IRawMaterialRepository
    {
        public RawMaterialRepository () : base("MsRawMaterial", "ID") { }

        internal override dynamic Mapping(MsRawMaterial item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				KodeMaterial = item.KodeMaterial,
				KodeRM = item.KodeRM,
				NamaMaterial = item.NamaMaterial,
				HSImport = item.HSImport,
				QtyEquivalent = item.QtyEquivalent,
				KodeSatuan = item.KodeSatuan,


            };
        }

        //do some override here

    }

}
