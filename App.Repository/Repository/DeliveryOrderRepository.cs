using App.Repository.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Repository
{
    public interface IDeliveryOrderRepository
    {

    }
    public sealed class DeliveryOrderRepository : Repository<TrDeliveryOrder>, IDeliveryOrderRepository
    {
        public DeliveryOrderRepository () : base("TrDeliveryOrder", "ID") { }

        internal override dynamic Mapping(TrDeliveryOrder item)
        {
            return new
            {
                //property here..

                ID = item.ID,
				NoDO = item.NoDO,
				TglDO = item.TglDO,
				NoPEB = item.NoPEB,
				TglPEB = item.TglPEB,
				Valuta = item.Valuta,
				KodeCustomer = item.KodeCustomer,
				NamaCustomer = item.NamaCustomer,
				Alamat = item.Alamat,
				Keterangan = item.Keterangan,
                Status= item.Status

            };
        }

        //do some override here

    }

}
