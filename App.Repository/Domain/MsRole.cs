using App.Repository;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Repository.Domain
{
    public partial class MsRole : EntityBase, IAggregateRoot
    {
        public MsRole()
        {
            this.MsRoleMenus = new List<MsRoleMenu>();            
        }
                
        [Key]
        public int ID { get; set; } //Role ID
        public string RoleName { get; set; }

        public List<MsRoleMenu> MsRoleMenus { get; set; }     
    }
}
