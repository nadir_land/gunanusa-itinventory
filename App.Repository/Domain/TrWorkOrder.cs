using System;

namespace App.Repository.Domain
{
    public partial class TrWorkOrder : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string NoWO { get; set; } 
		public string TglWO { get; set; } 
		public string KodeCustomer { get; set; } 
		public string NamaCustomer { get; set; } 
		public string Alamat { get; set; } 
		public string NegaraTujuan { get; set; } 
		public string KodeFG { get; set; } 
		public string NamaFG { get; set; } 
		public string HSExport { get; set; } 
		public string SatuanExport { get; set; } 
		public byte[] Foto { get; set; } 
		public decimal QtyOrder { get; set; } 
		public string KodeBarang { get; set; } 
		public string NamaBarang { get; set; } 
		public string HSImport { get; set; } 
		public string Satuan { get; set; } 
		public decimal Koefisien { get; set; } 
		public decimal Dibutuhkan { get; set; } 
		public string Status { get; set; } 


    }
}

