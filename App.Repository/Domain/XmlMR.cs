using System;

namespace App.Repository.Domain
{
    public partial class XmlMR : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string NOORDP { get; set; } 
		public string NOSLIPRM { get; set; } 
		public string TGSLIPRM { get; set; } 
		public string KDPART { get; set; } 
		public string KOBAR { get; set; } 
		public string QTY { get; set; } 
		public DateTime DownloadDate { get; set; } 


    }
}

