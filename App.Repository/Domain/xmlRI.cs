using System;

namespace App.Repository.Domain
{
    public partial class XmlRI : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string NOBUKMSK { get; set; } 
		public string TGBUKMSK { get; set; } 
		public string KOBAR { get; set; } 
		public string QTY { get; set; } 
		public DateTime DownloadDate { get; set; } 


    }
}

