using App.Repository.Repository;
using System;

namespace App.Repository.Domain
{
    public partial class MsUserRole : EntityBase,  IAggregateRoot
    {
        public int ID { get; set; }
        public int? UserID { get; set; }
        public int? RoleID { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdateOn { get; set; }
        public string UpdateBy { get; set; }
        public bool? Deleted { get; set; }

        #region Link Table

        private MsRole _role;
        public MsRole Role
        {
            get
            {
                RoleRepository repo = new RoleRepository();
                _role = repo.FindByID(Convert.ToInt16(RoleID));
                return _role;
            }
            set { _role = value; }
        }

        private MsUser _user;
        public MsUser User
        {
            get
            {
                UserRepository repo = new UserRepository();
                _user = repo.FindByID(Convert.ToInt16(UserID));
                return _user;
            }
            set
            {
                _user = value;
            }
        }


        #endregion

    }
}
