using System;

namespace App.Repository.Domain
{
    public partial class MsVendor : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string KodeVendor { get; set; } 
		public string NamaVendor { get; set; } 
		public string AlamatVendor { get; set; } 
		public string Negara { get; set; } 
		public string Telp { get; set; } 
		public string Fax { get; set; } 
		public string ContactPerson { get; set; } 
		public string Status { get; set; } 


    }
}

