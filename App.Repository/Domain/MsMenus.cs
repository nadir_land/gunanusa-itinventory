using App.Repository;
using System.Collections.Generic;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Repository.Domain
{
    public partial class MsMenus : EntityBase, IAggregateRoot
    {
        public MsMenus()
        {
            this.MsRoleMenus = new List<MsRoleMenu>();
        }        
        public int ID { get; set; } //Menu ID
        public string Link { get; set; }
        public string DisplayMenu { get; set; }
        public string ClassIcon { get; set; }
        public int? Parent { get; set; }
        public string TypeMenu { get; set; }

        public List<MsRoleMenu> MsRoleMenus { get; set; }
        
    }
}
