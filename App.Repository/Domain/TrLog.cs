using System;

namespace App.Repository.Domain
{
    public partial class TrLog : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string Username { get; set; } 
		public DateTime LogTime { get; set; } 
		public string ModulName { get; set; } 
		public string Activity { get; set; } 


    }
}

