﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repository.Domain
{
    public partial class TempTable : EntityBase
    { 
        public int UserID { get; set; }
        public string FullName { get; set; }
    }
}
