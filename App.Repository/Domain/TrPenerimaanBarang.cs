using System;

namespace App.Repository.Domain
{
    public partial class TrPenerimaanBarang : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string ReceiptNo { get; set; } 
		public string ReceiptDate { get; set; } 
		public string Warehouse { get; set; } 
		public string CAR { get; set; } 
		public string NoPIB { get; set; } 
		public DateTime TanggalPIB { get; set; } 
		public string KPBC { get; set; } 
		public string AsalBarang { get; set; } 
		public string Valuta { get; set; } 
		public decimal Rate { get; set; } 
		public string Status { get; set; } 
		public string KodeBarang { get; set; } 
		public string NamaBarang { get; set; } 
		public string HSImport { get; set; } 
		public string Satuan { get; set; } 
		public string NoSeri { get; set; } 
		public decimal TarifBM { get; set; } 
		public decimal CifPIB { get; set; } 
		public decimal QtyPIB { get; set; } 
		public decimal BmPIB { get; set; } 
		public decimal PpnPIB { get; set; } 
		public decimal QtyRI { get; set; } 


    }
}

