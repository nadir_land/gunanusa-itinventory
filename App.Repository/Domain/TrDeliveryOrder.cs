using System;

namespace App.Repository.Domain
{
    public partial class TrDeliveryOrder : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string NoDO { get; set; } 
		public string TglDO { get; set; } 
		public string NoPEB { get; set; } 
		public DateTime TglPEB { get; set; } 
		public string Valuta { get; set; } 
		public string KodeCustomer { get; set; } 
		public string NamaCustomer { get; set; } 
		public string Alamat { get; set; } 
		public string Keterangan { get; set; }

        public string Status { get; set; }
    }
}

