using System;

namespace App.Repository.Domain
{
    public partial class TrDeliveryOrderDetail : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public int DOID { get; set; } 
		public string KodeFG { get; set; } 
		public string NamaFG { get; set; } 
		public string HSExport { get; set; } 
		public string Satuan { get; set; } 
		public decimal QtyKirim { get; set; } 
		public decimal HargaSatuan { get; set; } 
		public decimal JumlahHarga { get; set; } 


    }
}

