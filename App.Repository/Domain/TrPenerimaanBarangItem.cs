using System;

namespace App.Repository.Domain
{
    public partial class TrPenerimaanBarangItem : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public int PenerimaanBarangID { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; } 
		public string HSImport { get; set; } 
		public string Satuan { get; set; } 
		public string NoSeri { get; set; } 
		public string TarifBM { get; set; } 
		public string CifPIB { get; set; } 
		public string QtyPIB { get; set; } 
		public string BmPIB { get; set; } 
		public string PpnPIB { get; set; } 
		public string QtyRI { get; set; } 


    }
}

