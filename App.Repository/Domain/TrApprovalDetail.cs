using System;

namespace App.Repository.Domain
{
    public partial class TrApprovalDetail : EntityBase,   IAggregateRoot
    {
        public int ID { get; set; }
        public int ApprovalID { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public int RoleID { get; set; }
        public string RoleName { get; set; }
        public string OrganizationName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int SequentialDetail { get; set; }
        public int SequentialMatrix { get; set; }
        public int MatrixID { get; set; }
        public string StatusApproval { get; set; }

        public string NoteApproval { get; set; }

        public string Operator { get; set; }
    }
}
