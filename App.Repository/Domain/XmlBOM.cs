using System;

namespace App.Repository.Domain
{
    public partial class XmlBOM : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string BOMNO { get; set; } 
		public string KDPART { get; set; } 
		public string BOMQTY { get; set; } 
		public string KOBAR { get; set; } 
		public string QTY { get; set; } 
		public DateTime DownloadDate { get; set; } 


    }
}

