using App.Repository.Domain;
using App.Repository.Repository;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Repository.Domain
{
    public partial class MsRoleMenu : EntityBase, IAggregateRoot
    {        
        public int ID { get; set; } //RoleMenuID
        public int? RoleID { get; set; }
        public int? MenuID { get; set; }

        #region Link Table

        private MsRole _role;        
        public MsRole Role
        {
            get
            {
                RoleRepository repo = new RoleRepository();
                _role = repo.FindByID(Convert.ToInt16(RoleID));
                return _role;
            }
            set { _role = value; }
        }

        private MsMenus _menu;
        public MsMenus Menu
        {
            get
            {
                MenuRepository repo = new MenuRepository();
                _menu = repo.FindByID(Convert.ToInt16(MenuID));
                return _menu;
            }
            set
            {
                _menu = value;
            }
        }


        #endregion
    }
}
