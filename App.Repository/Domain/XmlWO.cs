using System;

namespace App.Repository.Domain
{
    public partial class XmlWO : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string NOORDP { get; set; } 
		public string TGORDP { get; set; } 
		public string KDPART { get; set; } 
		public string QTY { get; set; } 
		public DateTime DownloadDate { get; set; } 


    }
}

