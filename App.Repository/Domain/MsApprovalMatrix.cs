using App.Repository.Repository;
using App.Utility;
using System.Collections.Generic;

namespace App.Repository.Domain
{
    public partial class MsApprovalMatrix : EntityBase,   IAggregateRoot
    {
        public int ID { get; set; }
        public string PRType { get; set; }
        public string PRCategory { get; set; }
        public string PRJType { get; set; }
        public string Nominal { get; set; }
        public double? Min { get; set; }
        public double? Max { get; set; }

        #region Link Table
        
        private IEnumerable<MsApprovalMatrixDetail> _matrixDetail;
        public IEnumerable<MsApprovalMatrixDetail> MatrixDetails
        {
            get
            {
                ApprovalMatrixDetailRepository repo = new ApprovalMatrixDetailRepository();
                List<WhereClause> wh = new List<WhereClause>();
                _matrixDetail = repo.FindByCriteria(" MatrixID='" + ID + "'", wh, "ID");
                return _matrixDetail;
            }
            set { _matrixDetail = value; }
        }

        #endregion
    }
}
