using System;

namespace App.Repository.Domain
{
    public partial class TrMaterialRelease : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string OpsiProduksi { get; set; } 
		public string NoWO { get; set; } 
		public string TglWO { get; set; } 
		public string NoRelease { get; set; }
        public string TglRelease { get; set; }
        public string KodeSubkon { get; set; } 
		public string NamaSubkon { get; set; } 
		public string Alamat { get; set; } 
		public string Keterangan { get; set; } 
		public string KodeFG { get; set; } 
		public string NamaFG { get; set; } 
		public string HSExport { get; set; } 
		public string SatuanExport { get; set; } 
		public decimal QtyOrder { get; set; } 
		public string KodeBarang { get; set; } 
		public string NamaBarang { get; set; } 
		public string Satuan { get; set; } 
		public string HSImport { get; set; } 
		public decimal Dibutuhkan { get; set; } 
		public decimal Terpakai { get; set; } 
		public decimal Dikeluarkan { get; set; } 
		public decimal BelumKirim { get; set; } 
		public string Status { get; set; } 


    }
}

