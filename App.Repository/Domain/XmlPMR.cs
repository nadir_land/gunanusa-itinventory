using System;

namespace App.Repository.Domain
{
    public partial class XmlPMR : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string NOORDPR { get; set; } 
		public string NOFGMSK { get; set; } 
		public string TGFGMSK { get; set; } 
		public string KDPART { get; set; } 
		public string QTY { get; set; } 
		public DateTime DownloadDate { get; set; } 


    }
}

