using App.Repository;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Repository.Domain
{
    public partial class MsUser : EntityBase, IAggregateRoot
    {
        [Key]
        public int ID { get; set; } //User ID        
        public string Username { get; set; }
        public string Password { get; set; }
        public string NPK { get; set; }
        public string FullName { get; set; }
        public string Department { get; set; }
        public string Email { get; set; }
        public bool? IsLocked { get; set; }
        public bool? IsOnline { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? UpdateOn { get; set; }
        public string UpdateBy { get; set; }
        public string Status { get; set; }
        public string UserRole { get; set; }     
    }
}
