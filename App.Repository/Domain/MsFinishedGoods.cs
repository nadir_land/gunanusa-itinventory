using System;

namespace App.Repository.Domain
{
    public partial class MsFinishedGoods : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string KodeFG { get; set; } 
		public string NamaFG { get; set; } 
		public string GudangFG { get; set; } 
		public string HSExport { get; set; } 
		public string Satuan { get; set; } 
		public string Keterangan { get; set; } 
		public byte[] Foto { get; set; } 


    }
}

