using System;

namespace App.Repository.Domain
{
    public partial class TrKonversiProduct : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string KodeFG { get; set; } 
		public string NamaFG { get; set; } 
		public string BOM { get; set; } 
		public string HSExport { get; set; } 
		public string SatuanExport { get; set; } 
		public string Keterangan { get; set; } 
		public byte[] Foto { get; set; } 
		public string KodeBarang { get; set; } 
		public string NamaBarang { get; set; } 
		public string HSImport { get; set; } 
		public string Satuan { get; set; } 
		public decimal Koefisien { get; set; } 
		public decimal Kandungan { get; set; } 
		public decimal Waste { get; set; } 
		public string Status { get; set; } 
		public decimal QtyBom { get; set; } 
		public decimal QtyBarang { get; set; } 


    }
}

