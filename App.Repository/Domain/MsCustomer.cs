using System;

namespace App.Repository.Domain
{
    public partial class MsCustomer : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string KodeCustomer { get; set; } 
		public string NamaCustomer { get; set; } 
		public string AlamatCustomer { get; set; } 
		public string Negara { get; set; } 
		public string Telp { get; set; } 
		public string Fax { get; set; } 
		public string Contact { get; set; } 
		public string Status { get; set; } 


    }
}

