using System;

namespace App.Repository.Domain
{
    public partial class XmlDO : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string NOFGKEL { get; set; } 
		public string TGFGKL { get; set; } 
		public string KDPART { get; set; } 
		public string QTY { get; set; } 
		public DateTime DownloadDate { get; set; } 


    }
}

