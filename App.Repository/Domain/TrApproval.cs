using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using System;
using System.Collections.Generic;

namespace App.Repository.Domain
{
    public partial class TrApproval : EntityBase,   IAggregateRoot
    {
        public int ID { get; set; }
        public int TransactionID { get; set; }
        public string TypeTransaction { get; set; }
        public int MatrixID { get; set; }
        public string DestinationUser { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int SequentialDetail { get; set; }

        public int SequentialMatrix { get; set; }

        public string StatusDesc { get; set; }

        public int StatusID { get; set; }

        public string CreatedBy { get; set; }

        public DateTime SubmitToSAPDate { get; set; }


        #region Link Table

        private MsApprovalMatrix _approvalMatrix;

        public MsApprovalMatrix ApprovalMatrix
        {
            get
            {
                ApprovalMatrixRepository repo = new ApprovalMatrixRepository();                
                _approvalMatrix = repo.FindByID(MatrixID);
                return _approvalMatrix;
            }
            set { _approvalMatrix = value; }
        }

        private IEnumerable<TrApprovalDetail> _approvalDetail;
        public IEnumerable<TrApprovalDetail> ApprovalDetail
        {
            get
            {
                ApprovalDetailRepository repo = new ApprovalDetailRepository();
                List<WhereClause> wh = new List<WhereClause>();
                _approvalDetail = repo.FindByCriteria(" ApprovalID='" + ID + "'", wh, "ID");
                return _approvalDetail;
            }
            set { _approvalDetail = value; }
        }

        #endregion

    }
}
