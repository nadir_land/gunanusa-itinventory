using System;

namespace App.Repository.Domain
{
    public partial class MsRawMaterial : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string KodeMaterial { get; set; } 
		public string KodeRM { get; set; } 
		public string NamaMaterial { get; set; } 
		public string HSImport { get; set; } 
		public decimal QtyEquivalent { get; set; } 
		public string KodeSatuan { get; set; } 


    }
}

