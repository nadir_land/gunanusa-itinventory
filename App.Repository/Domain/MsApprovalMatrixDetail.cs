namespace App.Repository.Domain
{
    public partial class MsApprovalMatrixDetail : EntityBase,   IAggregateRoot
    {
        public int ID { get; set; }
        public int MatrixID { get; set; }
        public string RoleID { get; set; }
        public string Operator { get; set; }
        public int Sequential { get; set; }
        public string DestinationRole { get; set; }
        public string Notes { get; set; }
        public int TblCode { get; set; }        
        public bool IsValidateMember { get; set; }

        public string RoleValidate { get; set; }

        public int SLA { get; set; }
        public string UnitOfTime { get; set; }
    }
}
