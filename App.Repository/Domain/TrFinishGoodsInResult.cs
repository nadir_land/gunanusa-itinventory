using System;

namespace App.Repository.Domain
{
    public partial class TrFinishGoodsInResult : EntityBase,     IAggregateRoot
    {
        
        public int ID { get; set; } 
		public string OpsiProduksi { get; set; } 
		public string NoWO { get; set; } 
		public string TglWO { get; set; } 
		public string NoResult { get; set; } 
		public string TglResult { get; set; } 
		public string KodeSubkon { get; set; } 
		public string NamaSubkon { get; set; } 
		public string Alamat { get; set; } 
		public string Keterangan { get; set; } 
		public string KodeFG { get; set; } 
		public string NamaFG { get; set; } 
		public string HSExport { get; set; } 
		public string SatuanExport { get; set; } 
		public decimal QtyOrder { get; set; } 
		public decimal SudahKirim { get; set; } 
		public decimal AkanKirim { get; set; } 
		public decimal DlmProses { get; set; }

        public string Status { get; set; }
    }
}

