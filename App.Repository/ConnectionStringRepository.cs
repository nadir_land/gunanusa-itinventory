﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace App.Repository
{
    internal class ConnectionStringRepository
    {
        public static string ConnectionString => ConfigurationManager.ConnectionStrings[""].ConnectionString;
    }
}
