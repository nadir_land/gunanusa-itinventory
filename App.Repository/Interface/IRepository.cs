﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IRepository.cs" company="contentedcoder.com">
//   contentedcoder.com
// </copyright>
// <summary>
//   The repository interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace App.Repository
{
    /// <summary>
    /// The repository interface.
    /// </summary>
    /// <typeparam name="T">
    /// The domain entity
    /// </typeparam>
    public interface IRepository<T> where T : EntityBase, IAggregateRoot
    {
        int Add(T item);
        int Remove(int id);
        int Update(T item);
        T FindByID(int id);
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        IEnumerable<T> FindAll();
    }
}