﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IContext.cs" company="contentedcoder.com">
//   contentedcoder.com
// </copyright>
// <summary>
//   The Context interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace App.Repository.UnitOfWork
{
    /// <summary>
    /// The Context interface.
    /// </summary>
    public interface IContext
    {
    }
}