﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics.SymbolStore;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using App.Repository;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;
using NLog.Config;
using NLog.Targets;


namespace App.ReadXML
{
    internal class Program
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            ConfigureLogger();

            try
            {

                // Connection string for your SQL Server database
                string connectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                //folder path
                string readFolder = ConfigurationManager.AppSettings["xmlFolderIn"].ToString();
                string successFolder = ConfigurationManager.AppSettings["xmlFolderSuccess"].ToString();
                string errorFolder = ConfigurationManager.AppSettings["xmlFolderError"].ToString();

                string[] xmlFiles = Directory.GetFiles(readFolder, "*.xml");



                foreach (string xmlFile in xmlFiles)
                {
                    string filename = Path.GetFileNameWithoutExtension(xmlFile);
                    #region RI File
                    if (filename.Contains("RI"))
                    {
                        try
                        {
                            bool success = true; // Flag to track if the data insertion was successful

                            // Load the XML document
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(xmlFile);

                            // Get the root element
                            XmlElement root = xmlDoc.DocumentElement;

                            // Get the Details nodes
                            XmlNodeList detailsNodes = root.SelectNodes("//Details");

                            // Iterate over each Details node
                            foreach (XmlNode detailsNode in detailsNodes)
                            {


                                // Get the attribute values
                                string nobukmsk = detailsNode.Attributes["NOBUKMSK"].Value;
                                string tgbukmsk = detailsNode.Attributes["TGBUKMSK"].Value;
                                string kobar = detailsNode.Attributes["KOBAR"].Value;
                                string qty = detailsNode.Attributes["QTY"].Value;

                                PenerimaanBarangRepository penerimaanBarangRepository = new PenerimaanBarangRepository();
                                TrPenerimaanBarang penerimaanBarang = new TrPenerimaanBarang();
                                //mapping to model
                                penerimaanBarang.ReceiptNo = nobukmsk;
                                penerimaanBarang.ReceiptDate = tgbukmsk;
                                penerimaanBarang.KodeBarang = kobar;
                                //clean data
                                /*
                                 note :
                                    "de-DE" represents the culture identifier for the German culture in Germany. By using CultureInfo.GetCultureInfo("de-DE")
                                    this is because based on data download sample from user.
                                 */
                                decimal _qty = decimal.Parse(qty, NumberStyles.Number | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.GetCultureInfo("de-DE"));
                                penerimaanBarang.QtyRI = _qty;
                                penerimaanBarang.TanggalPIB = DateTime.Now; //default
                                penerimaanBarang.Status = "New"; //default
                                penerimaanBarang.KPBC = "40300"; //default
                                penerimaanBarang.Warehouse = "Gudang RM";
                                //Find Master Data Raw material
                                RawMaterialRepository rawMaterialRepository = new RawMaterialRepository();
                                List<WhereClause> wh2 = new List<WhereClause>();
                                wh2.Add(new WhereClause { Property = "KodeMaterial", Value = kobar });
                                var rawMaterials = rawMaterialRepository.FindByCriteria("KodeMaterial=@KodeMaterial", wh2, "ID").ToList();
                                if (rawMaterials.Count > 0)
                                {
                                    //take the first row
                                    penerimaanBarang.NamaBarang = rawMaterials[0].NamaMaterial;
                                    penerimaanBarang.HSImport = rawMaterials[0].HSImport;
                                    penerimaanBarang.Satuan = rawMaterials[0].KodeSatuan;                                    
                                }

                                //commit
                                var result = penerimaanBarangRepository.Add(penerimaanBarang);
                                if (result > 0)
                                {
                                    success = true;
                                    logger.Info("Successfully Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }
                                else
                                {
                                    success = false;
                                    logger.Info("Failed Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }
                            }

                            // Move the XML file to the appropriate folder based on success or failure
                            string destinationFolderPath;
                            if (success)
                            {
                                destinationFolderPath = successFolder;
                            }
                            else
                            {
                                destinationFolderPath = errorFolder;
                            }

                            string destinationFileName = Path.GetFileNameWithoutExtension(xmlFile) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(xmlFile);
                            string destinationFilePath = Path.Combine(destinationFolderPath, destinationFileName);
                            File.Move(xmlFile, destinationFilePath);
                        }
                        catch (Exception ex)
                        {                            
                            logger.Error("An error occurred Processing " + filename + ".xml, Msg detail: " + ex.Message);

                        }

                        
                    }
                    #endregion

                    #region BOM File
                    if (filename.Contains("BOM"))
                    {

                        try
                        {

                            #region Process Xml Data BOM
                            bool success = true; // Flag to track if the data insertion was successful

                            // Load the XML document
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(xmlFile);

                            // Get the root element
                            XmlElement root = xmlDoc.DocumentElement;

                            // Get the Details nodes
                            XmlNodeList detailsNodes = root.SelectNodes("//Details");

                            // Iterate over each Details node
                            foreach (XmlNode detailsNode in detailsNodes)
                            {


                                // Get the attribute values XML Source
                                string BOM = detailsNode.Attributes["BOMNO"].Value;
                                string KodeFG = detailsNode.Attributes["KDPART"].Value;
                                string QtyBom = detailsNode.Attributes["BOMQTY"].Value;
                                string KodeBarang = detailsNode.Attributes["KOBAR"].Value;
                                string QtyBarang = detailsNode.Attributes["QTY"].Value;
                                //source from DB Master Finish Goods
                                string NamaFG = "";
                                string HSExport = "";
                                string SatuanExport = "";
                                string Keterangan = "";
                                byte[] Foto = null;
                                //Source from DB Master Raw Material
                                string NamaBarang = "";
                                string HSImport = "";
                                string Satuan = "";


                                //Find Master Data Finish Goods
                                FinishedGoodsRepository finishedGoodsRepository = new FinishedGoodsRepository();
                                List<WhereClause> wh = new List<WhereClause>();
                                wh.Add(new WhereClause { Property = "KodeFG", Value = KodeFG });
                                var finishgoods = finishedGoodsRepository.FindByCriteria("KodeFG=@KodeFG", wh, "ID").ToList();
                                if (finishgoods.Count > 0)
                                {
                                    //take the first row
                                    NamaFG = finishgoods[0].NamaFG;
                                    HSExport = finishgoods[0].HSExport;
                                    SatuanExport = finishgoods[0].Satuan;
                                    Keterangan = finishgoods[0].Keterangan;
                                    Foto = finishgoods[0].Foto;
                                }
                                //end for Finish Goods
                                //Find Master Data Raw material
                                RawMaterialRepository rawMaterialRepository = new RawMaterialRepository();
                                List<WhereClause> wh2 = new List<WhereClause>();
                                wh2.Add(new WhereClause { Property = "KodeMaterial", Value = KodeBarang });
                                var rawMaterials = rawMaterialRepository.FindByCriteria("KodeMaterial=@KodeMaterial", wh2, "ID").ToList();
                                if (rawMaterials.Count > 0)
                                {
                                    //take the first row
                                    NamaBarang = rawMaterials[0].NamaMaterial;
                                    HSImport = rawMaterials[0].HSImport;
                                    Satuan = rawMaterials[0].KodeSatuan;
                                }

                                //Insert data
                                KonversiProductRepository konversi = new KonversiProductRepository();
                                TrKonversiProduct data = new TrKonversiProduct();

                                data.KodeFG = KodeFG;
                                data.NamaFG = NamaFG;
                                data.BOM = BOM;
                                data.HSExport = HSExport;
                                data.SatuanExport = SatuanExport;
                                data.Keterangan = Keterangan;
                                data.Foto = Foto;
                                data.KodeBarang = KodeBarang;
                                data.NamaBarang = NamaBarang;
                                data.HSImport = HSImport;
                                data.Satuan = Satuan;
                                data.Status = "New";
                                data.QtyBom =  decimal.Parse(QtyBom, NumberStyles.Number | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.GetCultureInfo("de-DE"));
                                data.QtyBarang =  decimal.Parse(QtyBarang, NumberStyles.Number | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.GetCultureInfo("de-DE"));
                                var result = konversi.Add(data);
                                if (result > 0)
                                {
                                    success = true;
                                    logger.Info("Successfully Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }
                                else
                                {
                                    success = false;
                                    logger.Info("Failed Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }

                            }

                            // Move the XML file to the appropriate folder based on success or failure
                            string destinationFolderPath;
                            if (success)
                            {
                                destinationFolderPath = successFolder;
                            }
                            else
                            {
                                destinationFolderPath = errorFolder;
                            }

                            string destinationFileName = Path.GetFileNameWithoutExtension(xmlFile) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(xmlFile);
                            string destinationFilePath = Path.Combine(destinationFolderPath, destinationFileName);
                            File.Move(xmlFile, destinationFilePath);
                            #endregion

                        }
                        catch (Exception ex)
                        {                            
                            logger.Error("An error occurred Processing " + filename + ".xml, Msg detail: " + ex.Message);

                        }
                    }
                    #endregion

                    #region WO File
                    if (filename.Contains("Work Order"))
                    {

                        try
                        {

                            #region Process Xml Data Work Order
                            bool success = true; // Flag to track if the data insertion was successful

                            // Load the XML document
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(xmlFile);

                            // Get the root element
                            XmlElement root = xmlDoc.DocumentElement;

                            // Get the Details nodes
                            XmlNodeList detailsNodes = root.SelectNodes("//Details");

                            // Iterate over each Details node
                            foreach (XmlNode detailsNode in detailsNodes)
                            {


                                // Get the attribute values XML Source
                                string NoWO = detailsNode.Attributes["NOORDP"].Value;
                                string TglWO = detailsNode.Attributes["TGORDP"].Value;
                                string KodeFG = detailsNode.Attributes["KDPART"].Value;
                                string Qty = detailsNode.Attributes["QTY"].Value;
                                //source from DB Master Finish Goods
                                string NamaFG = "";
                                string HSExport = "";
                                string SatuanExport = "";                                
                                byte[] Foto = null;
                                //Source from DB Master Raw Material
                                string KodeBarang = "";
                                string NamaBarang = "";
                                string HSImport = "";
                                string Satuan = "";
                                decimal Koefisien = 0;

                                //Find BOM (Konversi)
                                KonversiProductRepository konversiProductRepository = new KonversiProductRepository();
                                List<WhereClause> wh2 = new List<WhereClause>();
                                wh2.Add(new WhereClause { Property = "KodeFG", Value = KodeFG });
                                var konversiProducts = konversiProductRepository.FindByCriteria("KodeFG=@KodeFG AND Status='Approve'", wh2, "ID").ToList();
                                if (konversiProducts.Count > 0)
                                {
                                    //take the first row
                                    NamaFG = konversiProducts[0].NamaFG;
                                    HSExport = konversiProducts[0].HSExport;
                                    SatuanExport = konversiProducts[0].Satuan;
                                    Foto = konversiProducts[0].Foto;
                                    KodeBarang = konversiProducts[0].KodeBarang;
                                    NamaBarang = konversiProducts[0].NamaBarang;
                                    HSImport = konversiProducts[0].HSImport;
                                    Satuan = konversiProducts[0].Satuan;
                                    Koefisien = konversiProducts[0].Koefisien;                                    
                                }

                                //Insert data
                                WorkOrderRepository workOrderRepository = new WorkOrderRepository();
                                TrWorkOrder data = new TrWorkOrder();
                                data.NoWO = NoWO;
                                data.TglWO = TglWO;
                                decimal _qty = decimal.Parse(Qty, NumberStyles.Number | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.GetCultureInfo("de-DE"));
                                data.QtyOrder = _qty;
                                data.KodeFG = KodeFG;
                                data.NamaFG = NamaFG;
                                
                                data.HSExport = HSExport;
                                data.SatuanExport = SatuanExport;                                
                                data.Foto = Foto;
                                data.KodeBarang = KodeBarang;
                                data.NamaBarang = NamaBarang;
                                data.HSImport = HSImport;
                                data.Satuan = Satuan;
                                data.Status = "New";
                                data.Koefisien = Koefisien;
                                data.Dibutuhkan = Koefisien * _qty;
                                var result = workOrderRepository.Add(data);
                                if (result > 0)
                                {
                                    success = true;
                                    logger.Info("Successfully Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }
                                else
                                {
                                    success = false;
                                    logger.Info("Failed Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }

                            }

                            // Move the XML file to the appropriate folder based on success or failure
                            string destinationFolderPath;
                            if (success)
                            {
                                destinationFolderPath = successFolder;
                            }
                            else
                            {
                                destinationFolderPath = errorFolder;
                            }

                            string destinationFileName = Path.GetFileNameWithoutExtension(xmlFile) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(xmlFile);
                            string destinationFilePath = Path.Combine(destinationFolderPath, destinationFileName);
                            File.Move(xmlFile, destinationFilePath);
                            #endregion

                        }
                        catch (Exception ex)
                        {                            
                            logger.Error("An error occurred Processing " + filename + ".xml, Msg detail: " + ex.Message);

                        }
                    }
                    #endregion

                    #region MR (material Release) File
                    if (filename.Equals("MR"))
                    {

                        try
                        {

                            #region Process Xml Data Material Release
                            bool success = true; // Flag to track if the data insertion was successful

                            // Load the XML document
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(xmlFile);

                            // Get the root element
                            XmlElement root = xmlDoc.DocumentElement;

                            // Get the Details nodes
                            XmlNodeList detailsNodes = root.SelectNodes("//Details");

                            // Iterate over each Details node
                            foreach (XmlNode detailsNode in detailsNodes)
                            {


                                // Get the attribute values XML Source
                                string NoWO = detailsNode.Attributes["NOORDP"].Value;
                                string NoRelease = detailsNode.Attributes["NOSLIPRM"].Value;
                                string TglRelease = detailsNode.Attributes["TGSLIPRM"].Value;
                                string KodeFG = detailsNode.Attributes["KDPART"].Value;
                                string KodeBarang = detailsNode.Attributes["KOBAR"].Value;
                                string Dikeluarkan = detailsNode.Attributes["QTY"].Value;
                                //source from DB Master Finish Goods
                                string NamaFG = "";
                                string HSExport = "";
                                string SatuanExport = "";                                
                                //Source from DB Master Raw Material                                
                                string NamaBarang = "";
                                string HSImport = "";
                                string Satuan = "";
                                decimal Koefisien = 0;

                                //Find Master Data Finish Goods
                                FinishedGoodsRepository finishedGoodsRepository = new FinishedGoodsRepository();
                                List<WhereClause> wh = new List<WhereClause>();
                                wh.Add(new WhereClause { Property = "KodeFG", Value = KodeFG });
                                var finishgoods = finishedGoodsRepository.FindByCriteria("KodeFG=@KodeFG", wh, "ID").ToList();
                                if (finishgoods.Count > 0)
                                {
                                    //take the first row
                                    NamaFG = finishgoods[0].NamaFG;
                                    HSExport = finishgoods[0].HSExport;
                                    SatuanExport = finishgoods[0].Satuan;                                    
                                }
                                //end for Finish Goods
                                //Find Master Data Raw material
                                RawMaterialRepository rawMaterialRepository = new RawMaterialRepository();
                                List<WhereClause> wh2 = new List<WhereClause>();
                                wh2.Add(new WhereClause { Property = "KodeMaterial", Value = KodeBarang });
                                var rawMaterials = rawMaterialRepository.FindByCriteria("KodeMaterial=@KodeMaterial", wh2, "ID").ToList();
                                if (rawMaterials.Count > 0)
                                {
                                    //take the first row
                                    NamaBarang = rawMaterials[0].NamaMaterial;
                                    HSImport = rawMaterials[0].HSImport;
                                    Satuan = rawMaterials[0].KodeSatuan;
                                }

                                //Find WO data
                                decimal QtyOrder = 0;
                                string TglWO = "";
                                decimal Dibutuhkan = 0;
                                WorkOrderRepository workOrderRepository = new WorkOrderRepository();
                                List<WhereClause> wh3 = new List<WhereClause>();
                                wh3.Add(new WhereClause { Property = "NoWO", Value = NoWO });
                                var WOData = workOrderRepository.FindByCriteria("NoWO=@NoWO and Status='Approve'", wh3, "ID").ToList();
                                if (WOData.Count > 0)
                                {
                                    QtyOrder = WOData[0].QtyOrder;
                                    TglWO = WOData[0].TglWO;
                                    Dibutuhkan = WOData[0].Dibutuhkan;
                                }
                                MaterialReleaseRepository materialReleaseRepository = new MaterialReleaseRepository();
                                TrMaterialRelease data = new TrMaterialRelease();
                                data.OpsiProduksi = "PPIC"; //default
                                data.NoWO = NoWO;
                                data.TglWO = TglWO;
                                data.NoRelease = NoRelease;
                                data.TglRelease = TglRelease;                                
                                decimal dikeluarkan = decimal.Parse(Dikeluarkan, NumberStyles.Number | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.GetCultureInfo("de-DE"));
                                data.Dikeluarkan = dikeluarkan;
                                data.KodeFG = KodeFG;
                                data.NamaFG = NamaFG;
                                data.HSExport = HSExport;
                                data.SatuanExport = SatuanExport;
                                data.QtyOrder = QtyOrder;
                                data.KodeBarang = KodeBarang;
                                data.NamaBarang = NamaBarang;
                                data.HSImport = HSImport;
                                data.Satuan = Satuan;
                                data.Status = "New";
                                data.Dibutuhkan = Dibutuhkan;
                                data.BelumKirim = Dibutuhkan - dikeluarkan;
                                var result = materialReleaseRepository.Add(data);
                                if (result > 0)
                                {
                                    success = true;
                                    logger.Info("Successfully Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }
                                else
                                {
                                    success = false;
                                    logger.Info("Failed Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }

                            }

                            // Move the XML file to the appropriate folder based on success or failure
                            string destinationFolderPath;
                            if (success)
                            {
                                destinationFolderPath = successFolder;
                            }
                            else
                            {
                                destinationFolderPath = errorFolder;
                            }

                            string destinationFileName = Path.GetFileNameWithoutExtension(xmlFile) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(xmlFile);
                            string destinationFilePath = Path.Combine(destinationFolderPath, destinationFileName);
                            File.Move(xmlFile, destinationFilePath);
                            #endregion

                        }
                        catch (Exception ex)
                        {                            
                            logger.Error("An error occurred Processing " + filename + ".xml, Msg detail: " + ex.Message);

                        }
                    }
                    #endregion

                    #region PMR (Finish Goods in Result) File
                    if (filename.Equals("PMR"))
                    {

                        try
                        {

                            #region Process Xml Data Material Release
                            bool success = true; // Flag to track if the data insertion was successful

                            // Load the XML document
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(xmlFile);

                            // Get the root element
                            XmlElement root = xmlDoc.DocumentElement;

                            // Get the Details nodes
                            XmlNodeList detailsNodes = root.SelectNodes("//Details");

                            // Iterate over each Details node
                            foreach (XmlNode detailsNode in detailsNodes)
                            {


                                // Get the attribute values XML Source
                                string NoWO = detailsNode.Attributes["NOORDPR"].Value;
                                string NoResult = detailsNode.Attributes["NOFGMSK"].Value;
                                string TglResult = detailsNode.Attributes["TGFGMSK"].Value;
                                string KodeFG = detailsNode.Attributes["KDPART"].Value;
                                string sAkanDikirim = detailsNode.Attributes["QTY"].Value;
                                //source from DB Master Finish Goods
                                string NamaFG = "";
                                string HSExport = "";
                                string SatuanExport = "";
                                
                                //Find Master Data Finish Goods
                                FinishedGoodsRepository finishedGoodsRepository = new FinishedGoodsRepository();
                                List<WhereClause> wh = new List<WhereClause>();
                                wh.Add(new WhereClause { Property = "KodeFG", Value = KodeFG });
                                var finishgoods = finishedGoodsRepository.FindByCriteria("KodeFG=@KodeFG", wh, "ID").ToList();
                                if (finishgoods.Count > 0)
                                {
                                    //take the first row
                                    NamaFG = finishgoods[0].NamaFG;
                                    HSExport = finishgoods[0].HSExport;
                                    SatuanExport = finishgoods[0].Satuan;
                                }
                                //end for Finish Goods
                                
                                //Find WO data
                                decimal QtyOrder = 0;
                                string TglWO = "";
                                WorkOrderRepository workOrderRepository = new WorkOrderRepository();
                                List<WhereClause> wh3 = new List<WhereClause>();
                                wh3.Add(new WhereClause { Property = "NoWO", Value = NoWO });
                                var WOData = workOrderRepository.FindByCriteria("NoWO=@NoWO and Status='Approve'", wh3, "ID").ToList();
                                if (WOData.Count > 0)
                                {
                                    QtyOrder = WOData[0].QtyOrder;
                                    TglWO = WOData[0].TglWO;
                                }
                                FinishGoodsInResultRepository finishGoodsInRepository = new FinishGoodsInResultRepository();
                                TrFinishGoodsInResult data = new TrFinishGoodsInResult();
                                data.OpsiProduksi = "PPIC"; //default
                                data.NoWO = NoWO;
                                data.TglWO = TglWO;
                                data.NoResult = NoResult;
                                data.TglResult = TglResult;
                                decimal AkanDikirim = decimal.Parse(sAkanDikirim, NumberStyles.Number | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.GetCultureInfo("de-DE"));
                                data.AkanKirim = AkanDikirim;
                                data.KodeFG = KodeFG;
                                data.NamaFG = NamaFG;
                                data.HSExport = HSExport;
                                data.SatuanExport = SatuanExport;
                                data.QtyOrder = QtyOrder;
                                data.Status = "New";
                                data.DlmProses = QtyOrder - AkanDikirim;
                                var result = finishGoodsInRepository.Add(data);
                                if (result > 0)
                                {
                                    success = true;
                                    logger.Info("Successfully Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }
                                else
                                {
                                    success = false;
                                    logger.Info("Failed Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }

                            }

                            // Move the XML file to the appropriate folder based on success or failure
                            string destinationFolderPath;
                            if (success)
                            {
                                destinationFolderPath = successFolder;
                            }
                            else
                            {
                                destinationFolderPath = errorFolder;
                            }

                            string destinationFileName = Path.GetFileNameWithoutExtension(xmlFile) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(xmlFile);
                            string destinationFilePath = Path.Combine(destinationFolderPath, destinationFileName);
                            File.Move(xmlFile, destinationFilePath);
                            #endregion

                        }
                        catch (Exception ex)
                        {
                            logger.Error("An error occurred Processing " + filename + ".xml, Msg detail: " + ex.Message);

                        }
                    }
                    #endregion

                    #region DO (Delivery Order) File
                    if (filename.Contains("DO"))
                    {

                        try
                        {

                            #region Process Xml Data Material Release
                            bool success = true; // Flag to track if the data insertion was successful

                            // Load the XML document
                            XmlDocument xmlDoc = new XmlDocument();
                            xmlDoc.Load(xmlFile);

                            // Get the root element
                            XmlElement root = xmlDoc.DocumentElement;

                            // Get the Details nodes
                            XmlNodeList detailsNodes = root.SelectNodes("//Details");

                            // Iterate over each Details node
                            foreach (XmlNode detailsNode in detailsNodes)
                            {


                                // Get the attribute values XML Source
                                string NoDO = detailsNode.Attributes["NOFGKEL"].Value;
                                string TglDO = detailsNode.Attributes["TGFGKL"].Value;                                
                                string KodeFG = detailsNode.Attributes["KDPART"].Value;
                                string sQtykirim = detailsNode.Attributes["QTY"].Value;
                                //source from DB Master Finish Goods
                                string NamaFG = "";
                                string HSExport = "";
                                string SatuanExport = "";

                                //Find Master Data Finish Goods
                                FinishedGoodsRepository finishedGoodsRepository = new FinishedGoodsRepository();
                                List<WhereClause> wh = new List<WhereClause>();
                                wh.Add(new WhereClause { Property = "KodeFG", Value = KodeFG });
                                var finishgoods = finishedGoodsRepository.FindByCriteria("KodeFG=@KodeFG", wh, "ID").ToList();
                                if (finishgoods.Count > 0)
                                {
                                    //take the first row
                                    NamaFG = finishgoods[0].NamaFG;
                                    HSExport = finishgoods[0].HSExport;
                                    SatuanExport = finishgoods[0].Satuan;
                                }
                                //end for Finish Goods

                                DeliveryOrderRepository doRepository = new DeliveryOrderRepository();
                                TrDeliveryOrder data = new TrDeliveryOrder();
                                data.Valuta = "USD"; //default
                                data.NoDO = NoDO;
                                data.TglDO = TglDO;

                                bool status = true;
                                //cek If Already exist
                                List<WhereClause> wheres = new List<WhereClause>();
                                wheres.Add(new WhereClause { Property = "NoDo", Value = NoDO });
                                var rows = doRepository.Find(wheres, "ID").ToList();
                                if (rows.Count() > 0)
                                {
                                    data.ID = rows[0].ID;
                                }
                                else
                                {
                                    data.Status = "New";
                                    data.TglPEB = Convert.ToDateTime("1900-01-01");
                                    var rs = doRepository.Add(data);
                                    if (rs > 0)
                                    {
                                        data.ID = rs;
                                    }
                                    else
                                    { 
                                        success = false;
                                    }
                                }
                                
                                if (data.ID > 0) 
                                {
                                    //add detail
                                    DeliveryOrderDetailRepository deliveryOrderDetailRepository = new DeliveryOrderDetailRepository();
                                    TrDeliveryOrderDetail detail = new TrDeliveryOrderDetail();
                                    detail.DOID = data.ID;
                                    decimal AkanDikirim = decimal.Parse(sQtykirim, NumberStyles.Number | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands, CultureInfo.GetCultureInfo("de-DE"));
                                    detail.QtyKirim = AkanDikirim;
                                    detail.KodeFG = KodeFG;
                                    detail.NamaFG = NamaFG;
                                    detail.HSExport = HSExport;
                                    detail.Satuan = SatuanExport;
                                    var rsdetail = deliveryOrderDetailRepository.Add(detail);
                                }
                                
                                if (status)
                                {
                                    success = true;
                                    logger.Info("Successfully Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }
                                else
                                {
                                    success = false;
                                    logger.Info("Failed Processing Data. " + Path.GetFileNameWithoutExtension(xmlFile) + ".xml");
                                }

                            }

                            // Move the XML file to the appropriate folder based on success or failure
                            string destinationFolderPath;
                            if (success)
                            {
                                destinationFolderPath = successFolder;
                            }
                            else
                            {
                                destinationFolderPath = errorFolder;
                            }

                            string destinationFileName = Path.GetFileNameWithoutExtension(xmlFile) + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + Path.GetExtension(xmlFile);
                            string destinationFilePath = Path.Combine(destinationFolderPath, destinationFileName);
                            File.Move(xmlFile, destinationFilePath);
                            #endregion

                        }
                        catch (Exception ex)
                        {
                            logger.Error("An error occurred Processing " + filename + ".xml, Msg detail: " + ex.Message);

                        }
                    }
                    #endregion
                }

            }
            catch (Exception e)
            {

                logger.Error("An error occurred." + e.Message);
            }

            
        }

        private static void ConfigureLogger()
        {
            LogManager.Configuration = new LoggingConfiguration();
            string pathLog = ConfigurationManager.AppSettings["LogFile"].ToString();
            var fileTarget = new FileTarget
            {
                FileName = pathLog +"/logfile.txt",
                Layout = "${longdate} ${level} ${message}",
            };

            LogManager.Configuration.AddTarget("file", fileTarget);

            var fileRule = new LoggingRule("*", LogLevel.Debug, fileTarget);
            LogManager.Configuration.LoggingRules.Add(fileRule);

            LogManager.ReconfigExistingLoggers();
        }
    }
}
