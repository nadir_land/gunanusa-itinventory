﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using Lib;


namespace App.Utility
{ 
    public class ClassMenu
    {

        public ClassMenu()
        {

        }

        public string getMenu(string userName, string Connection)
        {
            Class1 lib = new Class1();
            string menu=string.Empty;

            DataTable dt = lib.Query(Connection, @"Select Distinct b.* from MsRoleMenu a inner join MsMenu b on a.MenuID=b.MenuID 
                                                where a.RoleID in 
                                                (
	                                                Select RoleID from MsUser a, MsUserRole b where a.UserID=b.UserID and
	                                                UserName like '"+userName+"')");

            if (dt.Rows.Count > 0)
            {
                //get Header
                DataView Group = new DataView(dt, "TypeMenu='G'", "MenuID", DataViewRowState.CurrentRows);
                foreach (DataRowView item in Group)
                {

                    menu += "<span class='heading'>"+item["DisplayMenu"]+"</span>";
                    menu += "<ul class='list-unstyled'>";

                    //get Child
                    //DataView child = new DataView(dt, "[Parent]=" + item["[Parent]"], "MenuID", DataViewRowState.CurrentRows);
                    DataView child = new DataView(dt);
                    child.RowFilter = "[Parent]='" + item["MenuID"].ToString() + "'";
                    
                    foreach (DataRowView items in child)
                    {

                        if (items["TypeMenu"].ToString() == "C")
                        {
                            menu +="<li><a href='#exampledropdownDropdown' aria-expanded='false' data-toggle='collapse'> <i class='icon-interface-windows'></i>" + items["DisplayMenu"] + "</a>"+
                                      "<ul id='exampledropdownDropdown' class='collapse list-unstyled '>";

                            //search Anaknya
                            DataView childnya = new DataView(dt);
                            childnya.RowFilter = "[Parent]='" + items["MenuID"].ToString() + "'";
                            foreach (DataRowView itm in childnya)
                            {
                                menu += "<li><a href='" + itm["Link"] + "'> <i class='" + itm["ClassIcon"] + "'></i>" + itm["DisplayMenu"] + "</a></li>";
                            }

                            menu += "</ul></li>";
                        }
                        else
                        {
                            menu += "<li><a href='"+items["Link"]+"'> <i class='"+items["ClassIcon"]+"'></i>"+items["DisplayMenu"]+"</a></li>";
                        }
                    }

                    menu += "</ul>";
                }
                

            }
            else
            {
                menu = "<span class='heading'>Dashboard</span>" +
                "<ul class='list-unstyled'> " +
                    "<li class='active'><a href='/Home/Index'> <i class='icon-home'></i>Inbox</a></li>" +
                "</ul>";    
            }

            return menu;
            
        }
        
    }

}

