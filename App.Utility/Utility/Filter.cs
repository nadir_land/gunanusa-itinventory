﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using Newtonsoft.Json;

namespace App.Utility
{
    public static class EncodeFilter
    {
        public static String getUrlEncode(List<WhereClause> criteria)
        {
            return HttpUtility.UrlEncode(JsonConvert.SerializeObject(criteria)).Replace("+", "%20");
        }
    }

    public class Filter
    {
        public string property { get; set; }
        public object value { get; set; }
        public string sqlOperator { get; set; }
    }

    public class Sort
    {
        public string property { get; set; }
        public string direction { get; set; }
    }

    public class Sorter
    {
        public static void Build(string sort, ref string sortStr)
        {
            if (!String.IsNullOrEmpty(sort))
            {
                var sortList = new List<string>();
                var sortData = JsonConvert.DeserializeObject<List<Sort>>(sort);
                foreach (var sortedField in sortData)
                {
                    sortList.Add(sortedField.property + " " + sortedField.direction);
                }
                sortStr = string.Join(",", sortList);
            }
        }

        public static void Build(string sort, ref string sortStr, string prefix)
        {
            if (!String.IsNullOrEmpty(sort))
            {
                var sortList = new List<string>();
                var sortData = JsonConvert.DeserializeObject<List<Sort>>(sort);
                foreach (var sortedField in sortData)
                {
                    sortList.Add(prefix + sortedField.property + " " + sortedField.direction);
                }
                sortStr = string.Join(",", sortList);
            }
        }
    }

    public class WhereClause
    {
        #region private Variables
        private string _sqlOperator;
        private string _property;
        private object _value;
        #endregion

        #region public Properties
        public string Property { get { return _property; } set { _property = value; } }
        public string SqlOperator
        {
            get
            {
                return _sqlOperator == null ? "=" : _sqlOperator;
            }
            set
            {
                _sqlOperator = value;
            }
        }
        public object Value { get { return _value; } set { _value = value; } }
        #endregion
    }


    public class CriteriaBuilder
    {
        public static void Build(string filter, ref List<WhereClause> criteria)
        {
            if (!string.IsNullOrEmpty(filter))
            {
                var filterData =  JsonConvert.DeserializeObject<List<Filter>>(filter);
                foreach (var filterAttribute in filterData)
                {
                    criteria.Add(new WhereClause { Property = filterAttribute.property, Value = filterAttribute.value, SqlOperator = filterAttribute.sqlOperator  });
                }
            }
        }
    }
}

