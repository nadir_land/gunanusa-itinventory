﻿using System;
using NLog;
using NLog.Targets;
using NLog.Config;

namespace App.Utility
{
    public class CampusLogger
    {

        public CampusLogger()
        {
            // Step 1. Create configuration object 
            var config = new LoggingConfiguration();

            // Step 2. Create targets
            var consoleTarget = new ColoredConsoleTarget("target1")
            {
                Layout = @"${date:format=HH\:mm\:ss} ${level} ${message} ${exception:format=ToString}"
            };
            config.AddTarget(consoleTarget);

            var fileTarget = new FileTarget("target2")
            {
                FileName = "${basedir}/log.txt",
                Layout = "${longdate} ${level} ${message}  ${exception:format=@}"
            };
            config.AddTarget(fileTarget);


            // Step 3. Define rules
            config.AddRuleForOneLevel(LogLevel.Error, fileTarget); // only errors to file
            config.AddRuleForAllLevels(consoleTarget); // all to console

            // Step 4. Activate the configuration
            LogManager.Configuration = config;
        }

        public static Logger GetLogger()
        {
            var campusLogger = new CampusLogger();
            return LogManager.GetLogger("CampusLogger");
        }
    }
}
