﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace App.Utility
{
    public static partial class DateTimeFormatter
    {
        public static DateTime ConvertStringToDate(string date, string separator, string culture = "id")
        {
            date = date.Replace(separator, "/");
            return Convert.ToDateTime(date, new CultureInfo(culture));
        }

        public static string ConvertDateToString(string date, string separator, string culture = "id")
        {
            date = date.Replace(separator, "/");
            return Convert.ToString(date, new CultureInfo(culture));
        }

        public static DateTime ConvertStringToDate(string DateTimeString)
        {
            if (!string.IsNullOrEmpty(DateTimeString))
                return DateTime.ParseExact(DateTimeString, "yyyy-MM-ddTHH:mm:ss", CultureInfo.InvariantCulture);
            else
                return default(DateTime);
        }
        public static string ConvertDateToString(DateTime date)
        {
            return date.ToString("dd.MM.yyyy");
        }

    }
}
