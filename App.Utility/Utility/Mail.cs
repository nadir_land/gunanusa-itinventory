﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lib;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace App.Utility.Utility
{

    public class Mail
    {
        public static string SendEmail(int isDev, string smtp, int port, string uname, string password, string from, string to, string cc, string bcc, string subject, string body)
        {
            Lib.Class1 lib = new Lib.Class1();
            string result = "";
            if (isDev==1)
            {
                result = SendMailUsingGoogleSMTP(smtp, port, from, to, cc, bcc, subject, body);
                //TestMail();
            }
            else
            {
                result = lib.SendMail(smtp, port, from, to, cc, bcc, subject, body);
                //if any credential
                //result = lib.SendMailWithAttachment(smtp, port, from, to, cc, bcc, subject, body, null, null, null, uname, password);
            }
            
            
            return result;
        }

        private static string TestMail()
        {
            try
            {
                using (MailMessage mail = new MailMessage())
                {
                    mail.From = new MailAddress("digital.iship@gmail.com");
                    mail.To.Add("nadhirasshiddiq@gmail.com");
                    mail.Subject = "Hello World";
                    mail.Body = "<h1>Hello</h1>";
                    mail.IsBodyHtml = true;
                    //mail.Attachments.Add(new Attachment("C:\\file.zip"));

                    using (SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587))
                    {
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new NetworkCredential("ndrlandapps.console@gmail.com", "P@ssw0rd12345678");
                        smtp.EnableSsl = true;
                        smtp.Send(mail);
                    }
                }
            }
            catch (Exception e)
            {
                return e.Message;
            }

            return "sukses";
            
        }

        public static string SendMailWithAttachment(string SMTP, int Port, string From, string To, string CC, string BCC, string Subject, string Body, List<byte[]> attachment, string FileName, string extention, string usernameSMTP, string passwordSMTP)
        {
            try
            {
                System.Net.Mail.MailMessage objmail = new System.Net.Mail.MailMessage();
                objmail.IsBodyHtml = true;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                string[] ToReal = To.Split(';');
                string[] CCReal = CC.Split(';');
                string[] BCCReal = BCC.Split(';');
                objmail.From = new System.Net.Mail.MailAddress(From);
                if (!string.IsNullOrEmpty(To))
                {
                    for (int i = 0; i < ToReal.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(ToReal[i].ToString()))
                        {
                            objmail.To.Add(new System.Net.Mail.MailAddress(ToReal[i].ToString()));
                        }
                    }
                }
                if (!string.IsNullOrEmpty(CC))
                {
                    for (int i = 0; i < CCReal.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(CCReal[i].ToString()))
                        {
                            objmail.CC.Add(new System.Net.Mail.MailAddress(CCReal[i].ToString()));
                        }
                    }
                }

                if (!string.IsNullOrEmpty(BCC))
                {
                    for (int i = 0; i < BCCReal.Length; i++)
                    {
                        objmail.Bcc.Add(new System.Net.Mail.MailAddress(BCCReal[i].ToString()));
                    }
                }

                //Add Attachment 
                if (attachment != null)
                {
                    int i = 1;
                    foreach (var item in attachment)
                    {
                        Attachment att = new Attachment(new MemoryStream(item), FileName + "(" + i + ")." + extention);
                        objmail.Attachments.Add(att);
                        i++;
                    }
                }

                objmail.Subject = Subject;
                objmail.Body = Body;

                smtp.Host = SMTP;
                smtp.Port = Port;

                if (!String.IsNullOrEmpty(usernameSMTP) && !string.IsNullOrEmpty(passwordSMTP))
                {
                    smtp.Credentials = new System.Net.NetworkCredential(usernameSMTP, passwordSMTP);
                    smtp.EnableSsl = false;
                    smtp.Send(objmail);
                }
                else
                {
                    smtp.Send(objmail);
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Sukses";
        }

        private static string SendMailUsingGoogleSMTP(string SMTP, int Port, string From, string To, string CC, string BCC, string Subject, string Body)
        {
            try
            {
                System.Net.Mail.MailMessage objmail = new System.Net.Mail.MailMessage();
                objmail.IsBodyHtml = true;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                string[] ToReal = To.Split(';');
                string[] CCReal = CC.Split(';');
                string[] BCCReal = BCC.Split(';');
                objmail.From = new System.Net.Mail.MailAddress(From);
                if (!string.IsNullOrEmpty(To))
                {
                    for (int i = 0; i < ToReal.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(ToReal[i].ToString()))
                        {
                            objmail.To.Add(new System.Net.Mail.MailAddress(ToReal[i].ToString()));
                        }
                    }
                }
                if (!string.IsNullOrEmpty(CC))
                {
                    for (int i = 0; i < CCReal.Length; i++)
                    {
                        if (!string.IsNullOrEmpty(CCReal[i].ToString()))
                        {
                            objmail.CC.Add(new System.Net.Mail.MailAddress(CCReal[i].ToString()));
                        }
                    }
                }

                if (!string.IsNullOrEmpty(BCC))
                {
                    for (int i = 0; i < BCCReal.Length; i++)
                    {
                        objmail.Bcc.Add(new System.Net.Mail.MailAddress(BCCReal[i].ToString()));
                    }
                }

                objmail.Subject = Subject;
                objmail.Body = Body;



                //for testing using gmail

                using (SmtpClient smtpclient = new SmtpClient("smtp.gmail.com", 587))
                {
                    smtpclient.UseDefaultCredentials = false;
                    smtpclient.Credentials = new NetworkCredential("ndrlandapps.console@gmail.com", "P@ssw0rd12345678");
                    smtpclient.EnableSsl = true;
                    smtpclient.Send(objmail);
                }
                
            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            return "Sukses";
        }
    }
    
}
