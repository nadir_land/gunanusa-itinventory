﻿ using App.Core.Interface;
using App.Model;
using App.Repository.Repository;
using App.Web.Areas.Master.Models;
using App.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Repository.Domain;
using WebApp.Controllers;

namespace App.Web.Controllers
{
    public class BillingController : Controller
    {
        private IBillingRepository _repository;
        private SelectList typeList = new SelectList(new[] { "Meeting", "Requirements", "Development", "Testing", "Documentation" });

        public BillingController() : this(new BillingRepository())
        {
        }

        public BillingController(IBillingRepository repository)
        {
            _repository = repository;
        }


        public ActionResult Index()
        {
            return View();
        }

        public JsonResult LoadData()
        {
            var data = _repository.GetBillings().rows.ToList();
            return Json(new { data = data }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddEditForm(int id)
        {
            Billing model = new Billing();
            if (id > 0)
            {
                model = _repository.GetBillingByID(id).data;                
            }

            return PartialView("~/Views/Billing/PAddEditBilling.cshtml", model);
        }


        [HttpPost]
        public ActionResult Save(Billing billing)
        {
            MessageModel<Billing> result = new MessageModel<Billing>();
            if (billing.ID > 0)
            {
                //update
                result = _repository.EditBilling(billing);
            }
            else
            {
                //Create
                result = _repository.InsertBilling(billing);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var result = _repository.DeleteBilling(id);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}