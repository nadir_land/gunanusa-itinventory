﻿using App.Core;
using App.Core.Interface;
using App.Web.Infrastructure;
using App.Web.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using App.Utility;

namespace WebApp.Controllers
{
    public class BaseController : AsyncController
    {

        protected Logger logger;
        //
        // GET: /Base/
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpSessionStateBase session = filterContext.HttpContext.Session;
            
            if (session.IsNewSession || Session["User"] == null)
            {
                //filterContext.Result = Json("Session Timeout", "text/html",JsonRequestBehavior.AllowGet);
                filterContext.Result = RedirectToAction("Login", "Account", new { area = "Master" });
                return;
            }
            else
            {
                SessionModel usr = (SessionModel)Session["User"];                
                ViewData["UserLogin"] = usr.FullName;
                ViewData["FullName"] = usr.FullName;
                if(usr.Roles.Count> 0)
                {
                    GetMenuUser(usr.Roles[0].ID.ToString());
                }
                
                //get Image Profile
                string path = Server.MapPath("~/Content/img/profile/");
                FileInfo file = new FileInfo(Path.Combine(path, usr.Username + ".png"));
                if (file.Exists)
                {
                    byte[] imageArray = System.IO.File.ReadAllBytes(Path.Combine(path, usr.Username + "-thumb.png"));
                    string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                    ViewData["fotoProfile"] = base64ImageRepresentation;
                }
            }            
        }
       

        protected void GetMenuUser(string roleid)
        {

            /*
             define Menu here...
             */


            try
            {
                IRoleMenuService menu = new RoleMenuService();
                ViewData["Menu"] = menu.GetMenuByRoleId(roleid);   //menu.GetMenuByUserId(uname);
            }
            catch(Exception e)
            {
                RedirectToAction("Login", "Account", new { area = "Master" });
                return;
            }

        }

        public string SafeSqlLiteral(string s)
        {

            return StringProtector.SafeSqlLiteral(s);

        }

    }
}
