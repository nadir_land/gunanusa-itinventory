﻿using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Configuration;

namespace App.Web.Controllers.API
{
    [AuthorizeRequest]
    public class TaskListController : ApiController
    {        
        // GET api/TaskList/status
        public IHttpActionResult GetTaskList(string status)
        {   
            List<TasklistViewModel> List = new List<TasklistViewModel>();
            for (int i = 0; i < 10; i++)
            {
                TasklistViewModel tasklist = new TasklistViewModel();
                tasklist.CustomerName = "Customer " + i;
                tasklist.Address = "Jl. Angkur No. " + i + 1;
                tasklist.Date = DateTime.Now.ToShortDateString();
                tasklist.EquipmentNumber = "00000000000012391" + i;
                tasklist.Model = "Model " + i;
                tasklist.SN = "SN12345" + i;
                tasklist.Info = "";
                tasklist.Phone = "081921219" + i;
                tasklist.PICCustomer = "";
                tasklist.Id = ""+ i + 1;

                List.Add(tasklist);
            }

            return Ok(List);
            
        }
    }
}
