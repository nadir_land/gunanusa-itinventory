﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using App.Web.Models;

namespace App.Web.Controllers.API
{
    public class TasklistViewModelsController : ApiController
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: api/TasklistViewModels
        public IQueryable<TasklistViewModel> GetTasklistViewModels()
        {
            return db.TasklistViewModels;
        }

        // GET: api/TasklistViewModels/5
        [ResponseType(typeof(TasklistViewModel))]
        public IHttpActionResult GetTasklistViewModel(string id)
        {
            TasklistViewModel tasklistViewModel = db.TasklistViewModels.Find(id);
            if (tasklistViewModel == null)
            {
                return NotFound();
            }

            return Ok(tasklistViewModel);
        }

        // PUT: api/TasklistViewModels/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTasklistViewModel(string id, TasklistViewModel tasklistViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tasklistViewModel.Id)
            {
                return BadRequest();
            }

            db.Entry(tasklistViewModel).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TasklistViewModelExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/TasklistViewModels
        [ResponseType(typeof(TasklistViewModel))]
        public IHttpActionResult PostTasklistViewModel(TasklistViewModel tasklistViewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TasklistViewModels.Add(tasklistViewModel);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (TasklistViewModelExists(tasklistViewModel.Id))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("DefaultApi", new { id = tasklistViewModel.Id }, tasklistViewModel);
        }

        // DELETE: api/TasklistViewModels/5
        [ResponseType(typeof(TasklistViewModel))]
        public IHttpActionResult DeleteTasklistViewModel(string id)
        {
            TasklistViewModel tasklistViewModel = db.TasklistViewModels.Find(id);
            if (tasklistViewModel == null)
            {
                return NotFound();
            }

            db.TasklistViewModels.Remove(tasklistViewModel);
            db.SaveChanges();

            return Ok(tasklistViewModel);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TasklistViewModelExists(string id)
        {
            return db.TasklistViewModels.Count(e => e.Id == id) > 0;
        }
    }
}