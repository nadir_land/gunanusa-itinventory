﻿using System;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Net.Http;
using System.Linq;
using System.Configuration;
using System.Net;
using System.Web.Http;

namespace App.Web.Controllers.API
{
    public class AuthorizeRequest : System.Web.Http.Filters.ActionFilterAttribute
    {
        public AuthorizeRequest()
        {

        }
            
        public override void OnActionExecuting(HttpActionContext actionContext)
        {

            #region Validation Header
                        
            var headers = actionContext.Request.Headers;

            if (headers.Contains("token"))
            {
                string token = headers.GetValues("token").First();
                string key = ConfigurationManager.AppSettings["token"].ToString();
                if (token != key)
                {

                    actionContext.Response = actionContext.Request
                    .CreateResponse("Bad Request");

                }
            }
            else
            {
                actionContext.Response = actionContext.Request
                    .CreateResponse("Bad Request");
            }

            #endregion            
        }
    }
}
