using App.Core;
using App.Core.Interface;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace App.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<IUserService, UserService>();
            container.RegisterType<IRoleMenuService, RoleMenuService>();
            container.RegisterType<IRoleService, RoleService>();
            container.RegisterType<IMenuService, MenuService>();
            container.RegisterType<IUserRoleService, UserRoleService>();
            container.RegisterType<ILogService, LogService>();
            container.RegisterType<IVendorService, VendorService>();
            container.RegisterType<ICustomerService, CustomerService>();
            container.RegisterType<IFinishedGoodsService, FinishedGoodsService>();
            container.RegisterType<IRawMaterialService, RawMaterialService>();
            container.RegisterType<IXmlWOService, XmlWOService>();
            container.RegisterType<IXmlPMRService, XmlPMRService>();
            container.RegisterType<IXmlMRService, XmlMRService>();
            container.RegisterType<IXmlDOService, XmlDOService>();
            container.RegisterType<IXmlBOMService, XmlBOMService>();
            container.RegisterType<IXmlRIService, XmlRIService>();
            container.RegisterType<IPenerimaanBarangService, PenerimaanBarangService>();
            container.RegisterType<IPenerimaanBarangItemService, PenerimaanBarangItemService>();
            container.RegisterType<IKonversiProductService, KonversiProductService>();
            container.RegisterType<IConsoleService, ConsoleService>();
            container.RegisterType<IWorkOrderService,  WorkOrderService>();
            container.RegisterType<IMaterialReleaseService, MaterialReleaseService>();
            container.RegisterType<IFinishGoodsInResultService, FinishGoodsInResultService>();
            container.RegisterType<IDeliveryOrderService, DeliveryOrderService>();
            container.RegisterType<IDeliveryOrderDetailService, DeliveryOrderDetailService>();
            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}