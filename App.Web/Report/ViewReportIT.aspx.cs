﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Lifetime;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using App.Core;
using App.Core.Interface;
using App.Model;
using App.Utility;
using App.Web.Models;
using Microsoft.Reporting.WebForms;

namespace App.Web.Report
{
    public partial class ViewReportIT : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string type = Request.QueryString["type"];
                if (type == "Vendor")
                {
                    GenerateReportVendor();
                }
                else if (type == "Customer")
                {
                    GenerateReportCustomer();
                }
                else if (type == "RawMaterial")
                {
                    GenerateReportMaterial();
                }
                else if (type == "FG")
                {
                    GenerateReportFinishGoods();
                }
                else if (type == "KonversiProduct")
                {
                    GenerateReportKonversiProduct();
                }
                else if (type == "Penerimaan")
                {
                    GenerateReportPenerimaan();
                }
                else if (type == "WO")
                {
                    GenerateReportWO();
                }
                else if (type == "MaterialRelease")
                {
                    GenerateReportMaterialRelease();
                }
                else if (type == "FinishGoodIn")
                {
                    GenerateReportFinishedGoodIn();
                }
                else if (type == "DO")
                {
                    GenerateReportDO();
                }
                else if (type == "WasteScrap")
                {
                    GenerateReportWasteScrap();
                }
            }
        }

        private void GenerateReportWasteScrap()
        {
            IDeliveryOrderDetailService service = new DeliveryOrderDetailService();
            List<DeliveryOrderDetailModel> list = new List<DeliveryOrderDetailModel>();
            List<WhereClause> whereClauses = new List<WhereClause>();
            list = service.Report(" DOID in (Select ID from TrDeliveryOrder where Status='Approve')", whereClauses, "ID").rows.ToList();
            string filename = "Report Waste Scrap";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-WasteScrap.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportDO()
        {
            IDeliveryOrderDetailService service = new DeliveryOrderDetailService();
            List<DeliveryOrderDetailModel> list = new List<DeliveryOrderDetailModel>();
            List<WhereClause> whereClauses = new List<WhereClause>();            
            list = service.Report(" DOID in (Select ID from TrDeliveryOrder where Status='Approve')", whereClauses, "ID").rows.ToList();            
            string filename = "Report DO";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-DO.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportFinishedGoodIn()
        {
            IFinishGoodsInResultService service = new FinishGoodsInResultService();
            List<FinishGoodsInResultModel> list = new List<FinishGoodsInResultModel>();
            List<WhereClause> whereClauses = new List<WhereClause>();
            whereClauses.Add(new WhereClause { Property = "Status", Value = "Approve" });
            list = service.FindByCriteria("Status=@Status", whereClauses, "TglResult desc").rows.ToList();
            string filename = "Report Finished Good In - Result";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-FinishGoodInResult.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportMaterialRelease()
        {
            IMaterialReleaseService service = new MaterialReleaseService();
            List<MaterialReleaseModel> list = new List<MaterialReleaseModel>();
            List<WhereClause> whereClauses = new List<WhereClause>();
            whereClauses.Add(new WhereClause { Property = "Status", Value = "Approve" });
            list = service.FindByCriteria("Status=@Status", whereClauses, "TglRelease desc").rows.ToList();
            string filename = "Report Material Release";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-MaterialRelease.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportWO()
        {
            IWorkOrderService service = new WorkOrderService();
            List<WorkOrderModel> list = new List<WorkOrderModel>();
            List<WhereClause> whereClauses = new List<WhereClause>();
            whereClauses.Add(new WhereClause { Property = "Status", Value = "Approve" });
            list = service.FindByCriteria("Status=@Status", whereClauses, "TglWO desc").rows.ToList();
            string filename = "Report WO";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-WorkOrder.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportPenerimaan()
        {
            IPenerimaanBarangService service = new PenerimaanBarangService();
            List<PenerimaanBarangModel> list = new List<PenerimaanBarangModel>();
            List<WhereClause> whereClauses = new List<WhereClause>();
            whereClauses.Add(new WhereClause { Property = "Status", Value = "Approve" });
            list = service.FindByCriteria("Status=@Status", whereClauses, "TanggalPIB desc").rows.ToList();
            string filename = "Report Penerimaan";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-Penerimaan.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportKonversiProduct()
        {
            IKonversiProductService service = new KonversiProductService();
            List<KonversiProductModel> list = new List<KonversiProductModel>();
            List<WhereClause> whereClauses = new List<WhereClause>();
            whereClauses.Add(new WhereClause { Property = "Status", Value = "Approve" });
            list = service.FindByCriteria("Status=@Status", whereClauses, "ID Desc").rows.ToList();
            string filename = "Report Konversi";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-Konversi.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportFinishGoods()
        {
            IFinishedGoodsService service = new FinishedGoodsService();
            List<FinishedGoodsModel> list = new List<FinishedGoodsModel>();
            list = service.FindAll().rows.ToList();
            string filename = "Report FG";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-FG.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportMaterial()
        {
            IRawMaterialService service = new RawMaterialService();
            List<RawMaterialModel> list = new List<RawMaterialModel>();
            list = service.FindAll().rows.ToList();
            string filename = "Report Raw Material Import";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-RawMaterial.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();

        }

        private void GenerateReportCustomer()
        {
            List<ReportVendorCustomer> list = new List<ReportVendorCustomer>();
            ICustomerService service = new CustomerService();
            var data = service.FindAll().rows.ToList();
            foreach (var item in data)
            {
                list.Add(new ReportVendorCustomer
                {
                    Kode = item.KodeCustomer,
                    Name = item.NamaCustomer,
                    ContactPerson = item.Contact
                });
            }

            string filename = "Report Customer";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-Customer.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }

        private void GenerateReportVendor()
        {
            List<ReportVendorCustomer> list = new List<ReportVendorCustomer>();
            IVendorService service = new VendorService();
            var data = service.FindAll().rows.ToList();
            foreach(var item in data)
            {
                list.Add(new ReportVendorCustomer
                {
                    Kode = item.KodeVendor,
                    Name = item.NamaVendor,
                    ContactPerson = item.ContactPerson
                });
            }

            string filename = "Report Vendor";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("Report-IT-Vendor.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }
    }
}