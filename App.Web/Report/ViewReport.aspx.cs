﻿using App.Core;
using App.Model;
using App.Utility;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using App.Web.Models;
using App.Core.Interface;
using System.Web.Services.Description;
using System.Data.Entity.Infrastructure;
using System.Web.Http.Results;

namespace App.Web.Report
{
    public partial class ViewReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string type = Request.QueryString["type"];
                string startDate = Request.QueryString["start"];
                string endDate = Request.QueryString["end"];

                if (type == "Pemasukan")
                {
                    GenerateReportPenerimaan(startDate, endDate);
                }
                else if (type == "Pemakaian")
                {
                    GenerateReportPemakaianBahanBaku(startDate, endDate);
                }
                else if (type == "HasilProduksi")
                {
                    GenerateReportHasilProduksiSubkon(startDate, endDate);
                }
                else if (type == "PemasukanHasilProduksi")
                {
                    GenerateReportPemasukanHasilProduksi(startDate, endDate);
                }
                else if (type == "PengeluaranHasilProduksi")
                {
                    GenerateReportPengeluaranHasilProduksi(startDate, endDate);
                }
                else if (type == "MutasiBahanBaku")
                {
                    GenerateReportMutasiBahanBaku(startDate, endDate);
                }
                else if (type == "MutasiHasilProduksi")
                {
                    GenerateReportMutasiHasilProduksi(startDate, endDate);
                }
                else if (type == "WasteScrap")
                {
                    GenerateReportWasteScrap(startDate, endDate);
                }
            }
        }        

        #region Report Penerimaan / Pemasukan
        public void GenerateReportPenerimaan(string start, string end)
        {
            string filename = "Report Pemasukan Bahan Baku";
            PenerimaanBarangService service = new PenerimaanBarangService();
            List<PenerimaanBarangModel> data = new List<PenerimaanBarangModel>();
            List<WhereClause> wh = new List<WhereClause>();
            wh.Add(new WhereClause { Property="Start", SqlOperator=">=", Value=start});
            wh.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
            data = service.FindByCriteria("FORMAT(CONVERT(Date,ReceiptDate,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,ReceiptDate,105), 'yyyy-MM-dd')<=@End and Status='Approve'", wh, "ReceiptDate Desc").rows.ToList();
            if (data.Count > 0)
            {
                data.ForEach(item => { item.start = Convert.ToDateTime(start); item.end = Convert.ToDateTime(end); });
            }
            else
            { 
                data.Add(new PenerimaanBarangModel { start = Convert.ToDateTime(start), end= Convert.ToDateTime(end), FlagNoData = 1 });
            }
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReportPenerimaan.rdlc");

            ReportViewer1.LocalReport.DataSources.Clear();            
            ReportDataSource datasource1 = new ReportDataSource("DataSetPenerimaan", data);            
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }
        #endregion

        #region Report Pemakaian Bahan Baku
        public void GenerateReportPemakaianBahanBaku(string start, string end)
        {
            string filename = "Report Pemakaian Bahan Baku";
            MaterialReleaseService service = new MaterialReleaseService();
            List<ReportPemakaianBahanBakuModel> list = new List<ReportPemakaianBahanBakuModel>();
            List<WhereClause> wh = new List<WhereClause>();
            wh.Add(new WhereClause { Property = "Start", SqlOperator = ">=", Value = start });
            wh.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
            var data = service.FindByCriteria("FORMAT(CONVERT(Date,TglRelease,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,TglRelease,105), 'yyyy-MM-dd')<=@End and Status='Approve'", wh, "TglRelease Desc").rows.ToList();
            if (data.Count == 0) //if no data found
            {
                list.Add(new ReportPemakaianBahanBakuModel
                {
                    start = Convert.ToDateTime(start),
                    end = Convert.ToDateTime(end),
                    FlagNoData = 1
                });
            }else
            {
                foreach (var item in data)
                {
                    list.Add(new ReportPemakaianBahanBakuModel
                    {
                        ID = item.ID,
                        NoRelease = item.NoRelease,
                        TglRelease = item.TglRelease,
                        KodeBarang = item.KodeBarang,
                        NamaBarang = item.NamaBarang,
                        Satuan = item.Satuan,
                        Digunakan = item.Dikeluarkan,
                        start = Convert.ToDateTime(start),
                        end = Convert.ToDateTime(end)
                    });
                }
            }
            
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReportPemakaianBahanBaku.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }
        #endregion

        #region Report Hasil Produksi dari Subkon
        public void GenerateReportHasilProduksiSubkon(string start, string end)
        {
            string filename = "Report Hasil Produksi Subkon";
            MaterialReleaseService service = new MaterialReleaseService();
            List<ReportPemakaianBahanBakuModel> list = new List<ReportPemakaianBahanBakuModel>();
            List<WhereClause> wh = new List<WhereClause>();
            wh.Add(new WhereClause { Property = "Start", SqlOperator = ">=", Value = start });
            wh.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
            var data = service.FindByCriteria("FORMAT(CONVERT(Date,TglRelease,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,TglRelease,105), 'yyyy-MM-dd')<=@End and NamaSubkon is not null ", wh, "TglRelease desc").rows.ToList();
            foreach (var item in data)
            {
                list.Add(new ReportPemakaianBahanBakuModel
                {
                    ID = item.ID,
                    NoRelease = item.NoRelease,
                    TglRelease = item.TglRelease,
                    KodeBarang = item.KodeBarang,
                    NamaBarang = item.NamaBarang,
                    Satuan = item.Satuan,
                    Digunakan = item.Dibutuhkan,
                    start = Convert.ToDateTime(start),
                    end = Convert.ToDateTime(end),
                    FlagNoData=0
                });
            }
            if (data.Count == 0) //if no data found
            {
                list.Add(new ReportPemakaianBahanBakuModel
                {
                    start = Convert.ToDateTime(start),
                    end = Convert.ToDateTime(end),
                    FlagNoData = 1
                }); ;
            }
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReportHasilProduksiSubkon.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }
        #endregion

        #region Report Pemasukan Hasil Produksi
        public void GenerateReportPemasukanHasilProduksi(string start, string end)
        {
            string filename = "Report Pemasukan Hasil Produksi";
            FinishGoodsInResultService service = new FinishGoodsInResultService();
            FinishedGoodsService finishedGoodsService = new FinishedGoodsService();
            List<ReportPemasukanHasilProduksiModel> list = new List<ReportPemasukanHasilProduksiModel>();
            List<WhereClause> wh = new List<WhereClause>();
            wh.Add(new WhereClause { Property = "Start", SqlOperator = ">=", Value = start });
            wh.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
            var data = service.FindByCriteria("FORMAT(CONVERT(Date,TglResult,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,TglResult,105), 'yyyy-MM-dd')<=@End and Status='Approve'", wh, "TglResult DESC").rows.ToList();
            if (data.Count == 0) //if no data found
            {
                list.Add(new ReportPemasukanHasilProduksiModel
                {
                    start = Convert.ToDateTime(start),
                    end = Convert.ToDateTime(end),
                    FlagNoData = 1
                });
            }
            else
            {
                foreach (var item in data)
                {
                    //get FG
                    List<WhereClause> whfg = new List<WhereClause>();

                    whfg.Add(new WhereClause { Property = "KodeFG", Value = item.KodeFG });
                    var FG = finishedGoodsService.FindByCriteria("KodeFG=@KodeFG", whfg, "ID").rows.ToList();

                    list.Add(new ReportPemasukanHasilProduksiModel
                    {
                        ID = item.ID,
                        NoResult = item.NoResult,
                        TglResult = item.TglResult,
                        KodeBarang = item.KodeFG,
                        NamaBarang = item.NamaFG,
                        Satuan = item.SatuanExport,
                        DariProduksi = item.AkanKirim,
                        DariSubkon = String.IsNullOrEmpty(item.NamaSubkon) ? 0 : item.AkanKirim,
                        Gudang = FG.Count > 0 ? FG[0].GudangFG : "",
                        start = Convert.ToDateTime(start),
                        end = Convert.ToDateTime(end),
                        FlagNoData = 0
                    });
                }
            }
            
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReportPemasukanHasilProduksi.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }
        #endregion

        #region Report Pengeluaran Hasil Produksi
        public void GenerateReportPengeluaranHasilProduksi(string start, string end)
        {
            string filename = "Report Pengeluaran Hasil Produksi";
            DeliveryOrderService service = new DeliveryOrderService();
            DeliveryOrderDetailService detailService = new DeliveryOrderDetailService();
           List<ReportPengeluaranHasilProduksiModel> list = new List<ReportPengeluaranHasilProduksiModel>();
            List<WhereClause> wh = new List<WhereClause>();
            wh.Add(new WhereClause { Property = "Start", SqlOperator = ">=", Value = start });
            wh.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
            var data = service.FindByCriteria("FORMAT(CONVERT(Date,TglDO,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,TglDO,105), 'yyyy-MM-dd')<=@End and Status='Approve'", wh, "TglDO DESC").rows.ToList();
            if (data.Count == 0) //if no data found
            {
                list.Add(new ReportPengeluaranHasilProduksiModel
                {
                    start = Convert.ToDateTime(start),
                    end = Convert.ToDateTime(end),
                    FlagNoData = 1
                });
            }
            else
            {
                foreach (var delivery in data)
                {
                    //find Customer
                    CustomerService customerService = new CustomerService();
                    List<WhereClause> whCustomer = new List<WhereClause>();
                    whCustomer.Add(new WhereClause { Property = "KodeCustomer", Value = delivery.KodeCustomer });
                    var Customer = customerService.FindByCriteria("kodeCustomer=@KodeCustomer", whCustomer, "ID").rows.ToList();
                    //find the detail
                    List<WhereClause> whdetail = new List<WhereClause>();
                    whdetail.Add(new WhereClause { Property = "DOID", Value = delivery.ID });
                    var Items = detailService.FindByCriteria("DOID=@DOID", whdetail, "ID").rows.ToList();                    
                    foreach (var item in Items)
                    {
                        list.Add(new ReportPengeluaranHasilProduksiModel
                        {
                            ID = item.ID,
                            NoPEB = delivery.NoPEB,
                            TglPEB = delivery.TglPEB.ToString("dd-MM-yyyy"),
                            NoDO = delivery.NoDO,
                            TglDO = delivery.TglDO,
                            Pembeli = delivery.KodeCustomer,
                            NegaraTujuan = Customer.Count>0 ? Customer[0].Negara : "",
                            KodeBarang = item.KodeFG,
                            NamaBarang = item.NamaFG,
                            Satuan = item.Satuan,
                            Jumlah = item.QtyKirim,
                            MataUang = delivery.Valuta,
                            NilaiBarang = item.JumlahHarga,
                            start = Convert.ToDateTime(start),
                            end = Convert.ToDateTime(end),
                            FlagNoData = 0
                        });
                    }

                }
            }

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReportPengeluaranHasilProduksi.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }
        #endregion

        #region Report Mutasi Bahan Baku
        private void GenerateReportMutasiBahanBaku(string start, string end)
        {
            string filename = "Report Mutasi Bahan Baku";
            PenerimaanBarangService service = new PenerimaanBarangService();
            RawMaterialService materialService = new RawMaterialService();
            MaterialReleaseService materialReleaseService = new MaterialReleaseService();
            FinishedGoodsService finishedGoodsService = new FinishedGoodsService();
            List<ReportMutasiModel> list = new List<ReportMutasiModel>();            
            //var data = service.FindByCriteria("FORMAT(CONVERT(Date,TglResult,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,TglResult,105), 'yyyy-MM-dd')<=@End and Status='Approve'", wh, "ID").rows.ToList();
            var data = materialService.FindAll().rows.ToList();
            if (data.Count == 0) //if no data found
            {
                list.Add(new ReportMutasiModel
                {
                    start = Convert.ToDateTime(start),
                    end = Convert.ToDateTime(end),
                    FlagNoData = 1
                });
            }
            else
            {
                foreach (var item in data)
                {
                    string gudang = "";
                    //{enerimaan Barang
                    List<WhereClause> wh = new List<WhereClause>();
                    wh.Add(new WhereClause { Property = "Start", SqlOperator = ">=", Value = start });
                    wh.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
                    wh.Add(new WhereClause { Property = "KodeBarang", Value = item.KodeMaterial });
                    var Penerimaan = service.FindByCriteria("FORMAT(CONVERT(Date,ReceiptDate,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,ReceiptDate,105), 'yyyy-MM-dd')<=@End and Status='Approve' and KodeBarang=@KodeBarang", wh, "ID").rows.ToList();

                    //get Material Release
                    List<WhereClause> whMR = new List<WhereClause>();
                    whMR.Add(new WhereClause { Property = "KodeBarang", Value = item.KodeMaterial });
                    whMR.Add(new WhereClause { Property = "Start", SqlOperator = ">=", Value = start });
                    whMR.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
                    var MaterialRelease = materialReleaseService.FindByCriteria("FORMAT(CONVERT(Date,TglRelease,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,TglRelease,105), 'yyyy-MM-dd')<=@End and Status='Approve' and KodeBarang=@KodeBarang", whMR, "ID").rows.ToList();

                    decimal pemasukan = Penerimaan.Count > 0 ? Penerimaan.Sum(p=> p.QtyRI) : 0;
                    decimal pengeluaran = MaterialRelease.Count > 0 ? MaterialRelease.Sum(p => p.Dikeluarkan) : 0;


                    list.Add(new ReportMutasiModel
                    {
                        KodeBarang = item.KodeMaterial,
                        NamaBarang = item.NamaMaterial,
                        Satuan = item.KodeSatuan,
                        Gudang = Penerimaan.Count>0 ? Penerimaan[0].Warehouse : "",
                        SaldoAwal = 0,
                        Pemasukan = pemasukan,
                        Pengeluaran = pengeluaran,
                        SaldoAkhir = pemasukan - pengeluaran,
                        start = Convert.ToDateTime(start),
                        end = Convert.ToDateTime(end),
                        FlagNoData = 0
                    });
                }
            }

            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReportMutasiBahanBaku.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();
        }
        #endregion

        #region Report Mutasi Hasil Produksi
        private void GenerateReportMutasiHasilProduksi(string start, string end)
        { 
         
            DeliveryOrderService deliveryOrderService = new DeliveryOrderService();
            DeliveryOrderDetailService deliveryOrderDetailService = new DeliveryOrderDetailService();
            FinishedGoodsService finishedGoodsService = new FinishedGoodsService();
            FinishGoodsInResultService FgInResultservice = new FinishGoodsInResultService();
            List<ReportMutasiModel> list = new List<ReportMutasiModel>();
            List<WhereClause> wh = new List<WhereClause>();
            wh.Add(new WhereClause { Property = "Start", SqlOperator = ">=", Value = start });
            wh.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
            var deliveryOrders = deliveryOrderService.FindByCriteria("FORMAT(CONVERT(Date,TglDO,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,TglDO,105), 'yyyy-MM-dd')<=@End and Status='Approve'", wh, "TglDO DESC").rows.ToList();
            if (deliveryOrders.Count > 0)
            {
                foreach(var deliveryOrder in deliveryOrders)
                {
                    //find the detail
                    List<WhereClause> whdo = new List<WhereClause>();
                    whdo.Add(new WhereClause { Property = "DOID", Value = deliveryOrder.ID });
                    var items = deliveryOrderDetailService.FindByCriteria("DOID=@DOID", whdo, "ID").rows.ToList();
                    if(items.Count > 0)
                    {
                        //distinct data
                        var result = items
                                    .GroupBy(m => new { m.KodeFG, m.NamaFG, m.Satuan })
                                    .Select(g => new DeliveryOrderDetailModel
                                    {
                                        KodeFG = g.Key.KodeFG,
                                        NamaFG = g.Key.NamaFG,
                                        Satuan = g.Key.Satuan,
                                        QtyKirim = g.Sum(m => m.QtyKirim)
                                    })
                                    .ToList();

                        foreach (var item in result)
                        {
                            //get FG (Master)
                            List<WhereClause> whfg = new List<WhereClause>();
                            whfg.Add(new WhereClause { Property = "KodeFG", Value = item.KodeFG });
                            var FG = finishedGoodsService.FindByCriteria("KodeFG=@KodeFG", whfg, "ID").rows.ToList();

                            //Get Finished Goods In Results 
                            var FgInResults = FgInResultservice.FindByCriteria("KodeFG=@KodeFG AND Status='Approve'", whfg, "ID").rows.ToList();

                            decimal _pemasukan = FgInResults.Count > 0 ? FgInResults.Sum(a => a.AkanKirim) : 0;
                            decimal _pengeluaran = item.QtyKirim;
                            decimal _saldoAkhir = _pemasukan - _pengeluaran;


                            list.Add(new ReportMutasiModel
                            {
                                KodeBarang = item.KodeFG,
                                NamaBarang = !String.IsNullOrEmpty(item.NamaFG) ? item.NamaFG : FG.Count > 0 ? FG[0].NamaFG : "",
                                Satuan = !String.IsNullOrEmpty(item.Satuan) ? item.Satuan : FG.Count > 0 ? FG[0].Satuan : "",
                                Gudang = FG.Count > 0 ? FG[0].GudangFG : "",
                                SaldoAwal = 0,
                                SaldoAkhir = _saldoAkhir,
                                Pemasukan = _pemasukan,
                                Pengeluaran = _pengeluaran,
                                start = Convert.ToDateTime(start),
                                end = Convert.ToDateTime(end),
                                FlagNoData = 0
                            });
                        }
                    }
                    
                }
            }
            else
            {
                list.Add(new ReportMutasiModel
                {
                    start = Convert.ToDateTime(start),
                    end = Convert.ToDateTime(end),
                    FlagNoData = 1
                });
            }

            string filename = "Report Mutasi Hasil Produksi";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReportMutasiHasilProduksi.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();

        }
        #endregion

        #region Report WASTE SCRAP
        private void GenerateReportWasteScrap(string start, string end)
        {

            DeliveryOrderService deliveryOrderService = new DeliveryOrderService();
            DeliveryOrderDetailService deliveryOrderDetailService = new DeliveryOrderDetailService();
            KonversiProductService konversiProductService = new KonversiProductService();
            List<ReportWasteScrapModel> list = new List<ReportWasteScrapModel>();
            List<WhereClause> wh = new List<WhereClause>();
            wh.Add(new WhereClause { Property = "Start", SqlOperator = ">=", Value = start });
            wh.Add(new WhereClause { Property = "End", SqlOperator = "<=", Value = end });
            var deliveryOrders = deliveryOrderService.FindByCriteria("FORMAT(CONVERT(Date,TglDO,105), 'yyyy-MM-dd')>=@Start and FORMAT(CONVERT(Date,TglDO,105), 'yyyy-MM-dd')<=@End and Status='Approve'", wh, "TglDO DESC").rows.ToList();
            if (deliveryOrders.Count > 0)
            {
                foreach (var deliveryOrder in deliveryOrders)
                {

                    //find the detail
                    List<WhereClause> whdo = new List<WhereClause>();
                    whdo.Add(new WhereClause { Property = "DOID", Value = deliveryOrder.ID });
                    var items = deliveryOrderDetailService.FindByCriteria("DOID=@DOID", whdo, "ID").rows.ToList();
                    if (items.Count > 0)
                    {

                        foreach (var item in items)
                        {
                            decimal nilai = 0;
                            List<WhereClause> whkonversi = new List<WhereClause>();
                            whkonversi.Add(new WhereClause { Property = "KodeFG", Value = item.KodeFG });
                            whkonversi.Add(new WhereClause { Property = "Status", Value = "Approve" });
                            var konversiDatas = konversiProductService.FindByCriteria("KodeFG=@KodeFG and Status=@Status", whkonversi, "ID").rows.ToList();
                            if (konversiDatas.Count > 0)
                            {
                                var konversiData = konversiDatas[0];
                                item.QuantityEquivalent = (item.QtyKirim / konversiData.Kandungan) * konversiData.Waste;
                                item.KodeBarang = konversiData.KodeBarang;
                                item.NamaBarang = konversiData.NamaBarang;
                                item.Satuan = konversiData.Satuan;

                                #region Waste
                                decimal tDOQtyWaste = item.QuantityEquivalent + konversiData.Waste;
                                #endregion

                                IPenerimaanBarangService penerimaanBarangService = new PenerimaanBarangService();
                                List<WhereClause> wh2 = new List<WhereClause>();
                                wh2.Add(new WhereClause { Property = "KodeBarang", Value = konversiData.KodeBarang });
                                wh2.Add(new WhereClause { Property = "Status", Value = "Approve" });
                                var penerimaanBarang = penerimaanBarangService.FindByCriteria("Status=@Status and KodeBarang=@KodeBarang", wh2, "ID").rows.ToList();
                                if (penerimaanBarang.Count > 0)
                                {
                                    //decimal hargaSatuan = penerimaanBarang[0].BmPIB / penerimaanBarang[0].QtyPIB;
                                    //nilai = item.QtyKirim * hargaSatuan;

                                    decimal SisaSaldoQty = 0;
                                    decimal bmPIB = 0;
                                    decimal qtyPIB = 0;
                                    foreach (var p in penerimaanBarang)
                                    {
                                        bmPIB = p.BmPIB;
                                        qtyPIB = p.QtyPIB;
                                        SisaSaldoQty = p.QtyPIB - tDOQtyWaste;
                                        if (SisaSaldoQty > 0)
                                        {
                                            break;
                                        }
                                    }

                                    decimal hargaSatuan = bmPIB / qtyPIB;
                                    nilai = item.QuantityEquivalent * hargaSatuan;
                                }
                            }


                            list.Add(new ReportWasteScrapModel
                            {
                                No = "BC-" + deliveryOrder.NoDO,
                                Tgl = deliveryOrder.TglDO,
                                KodeBarang = item.KodeBarang,
                                NamaBarang = item.NamaBarang,
                                Satuan = item.Satuan,
                                Jumlah = item.QuantityEquivalent,
                                Nilai = nilai,
                                start = Convert.ToDateTime(start),
                                end = Convert.ToDateTime(end),
                                FlagNoData = 0
                            });
                        }
                    }
                }
            }
            else
            {
                list.Add(new ReportWasteScrapModel
                {
                    start = Convert.ToDateTime(start),
                    end = Convert.ToDateTime(end),
                    FlagNoData = 1
                });
            }

            string filename = "Report Waste Scrap";
            ReportViewer1.LocalReport.ReportPath = Server.MapPath("ReportWasteScrap.rdlc");
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportDataSource datasource1 = new ReportDataSource("DataSet1", list);
            ReportViewer1.LocalReport.DataSources.Add(datasource1);
            ReportViewer1.LocalReport.DisplayName = filename;
            ReportViewer1.LocalReport.Refresh();

        }
        #endregion
    }
}