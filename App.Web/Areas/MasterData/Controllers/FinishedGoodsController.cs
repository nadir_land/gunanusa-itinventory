using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;

namespace App.Web.Areas.MasterData.Controllers
{
    public class FinishedGoodsController : BaseController
    {
        private string moduleName = "FinishedGoods";
		IFinishedGoodsService service;
        ILogService logService;
		/*
			GiftModel  --> from App.Models
		*/

        
		public FinishedGoodsController(IFinishedGoodsService _service, ILogService logService)
        {
            service = _service;
            this.logService = logService;
        }

        // GET: FinishedGoods
        public ActionResult Index()
        {
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
			ViewBag.PageName = "Finished Goods";
            ViewBag.RoleName = susrs.RoleName;
            return View();
        }

		#region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
			//clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }
			

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
				strCriteria = "ID like  '%"+ param.sSearch + "%' OR KodeFG like  '%"+ param.sSearch + "%' OR NamaFG like  '%"+ param.sSearch + "%' OR GudangFG like  '%"+ param.sSearch + "%' OR HSExport like  '%"+ param.sSearch + "%' OR Satuan like  '%"+ param.sSearch + "%' OR Keterangan like  '%"+ param.sSearch + "%'";
            }
            /*
            //##If You use Custom filter modified this filter
            if (!string.IsNullOrEmpty(EName))
            {
                if (strCriteria != "")
                {
					//define logic here..
					//sample
                    //strCriteria += " AND RoleID in (Select ID from MsRole where RoleName like  '%" + EName + "%')";
                }
                else
                {
					//define logic here..
					//sample
                    //strCriteria = " RoleID in (Select ID from MsRole where RoleName like  '%" + EName + "%')";
                }
            }
            */
            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<FinishedGoodsModel> Result = service.FindDataWithPaging(strCriteria,whereClauses, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditForm(int id)
        {

            FinishedGoodsModel model = new FinishedGoodsModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                if (model.Foto != null)
                {
                    string base64String = Convert.ToBase64String(model.Foto);
                    string imageSource = string.Format("data:image/jpeg;base64,{0}", base64String);
                    model.imageBase64String = imageSource;
                }

            }

            List<ComboboxModel> SatuanList = new List<ComboboxModel>();
            SatuanList.Add(new ComboboxModel()
            {
                Key = "KGM",
                Text = "KGM"
            });
            SatuanList.Add(new ComboboxModel()
            {
                Key = "RGM",
                Text = "RGM"
            });
            ViewBag.SatuanList = new SelectList(SatuanList, "Key", "Text", model.Satuan);

            return PartialView("~/Areas/MasterData/Views/FinishedGoods/PFinishedGoods.cshtml", model);
        }

        public ActionResult ViewForm(int id)
        {

            FinishedGoodsModel model = new FinishedGoodsModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                if (model.Foto != null)
                {
                    string base64String = Convert.ToBase64String(model.Foto);
                    string imageSource = string.Format("data:image/jpeg;base64,{0}", base64String);
                    model.imageBase64String = imageSource;
                }                
            }

            return PartialView("~/Areas/MasterData/Views/FinishedGoods/PViewFinishedGoods.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(FinishedGoodsModel mdl)
        {
            
            SessionModel susrs = (SessionModel)Session["User"];
            var result =  new MessageModel<FinishedGoodsModel>();
            string activity="";
            if (!String.IsNullOrEmpty(mdl.imageBase64String))
            {
                string imageData = mdl.imageBase64String.Split(',')[1];
                byte[] imageBytes = Convert.FromBase64String(imageData);
                mdl.Foto = imageBytes;
            }
            if (mdl.ID > 0)
            {
                //do update
                //mdl.UpdatedBy = usr.Username;
                result = service.Update(mdl);
                activity="Ubah Data ID = " + mdl.ID;                
            }
            else
            {
                //mdl.CreatedBy = usr.Username;
                result = service.Add(mdl);                
                activity="Tambah Data";
            }
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = activity +" di "+moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];            
            var data = service.FindByID(id).data;
            var result = service.Delete(data);
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete data Kode :" + data.KodeFG + ", ID : " + id + " di " + moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}
