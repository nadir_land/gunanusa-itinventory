using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;

namespace App.Web.Areas.MasterData.Controllers
{
    public class VendorController : BaseController
    {
        private string moduleName = "Vendor";
		IVendorService service;
        ILogService logService;
		/*
			GiftModel  --> from App.Models
		*/

        
		public VendorController(IVendorService _service, ILogService logService)
        {
            service = _service;
            this.logService = logService;
        }

        // GET: Vendor
        public ActionResult Index()
        {
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
			ViewBag.PageName = "Vendor";
            ViewBag.RoleName = susrs.RoleName;
            return View();
        }

		#region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
			//clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }
			

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
				strCriteria = "ID like  '%"+ param.sSearch + "%' OR KodeVendor like  '%"+ param.sSearch + "%' OR NamaVendor like  '%"+ param.sSearch + "%' OR AlamatVendor like  '%"+ param.sSearch + "%' OR Negara like  '%"+ param.sSearch + "%' OR Telp like  '%"+ param.sSearch + "%' OR Fax like  '%"+ param.sSearch + "%' OR ContactPerson like  '%"+ param.sSearch + "%' OR Status like  '%"+ param.sSearch + "%'";
            }
            /*
            //##If You use Custom filter modified this filter
            if (!string.IsNullOrEmpty(EName))
            {
                if (strCriteria != "")
                {
					//define logic here..
					//sample
                    //strCriteria += " AND RoleID in (Select ID from MsRole where RoleName like  '%" + EName + "%')";
                }
                else
                {
					//define logic here..
					//sample
                    //strCriteria = " RoleID in (Select ID from MsRole where RoleName like  '%" + EName + "%')";
                }
            }
            */
            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<VendorModel> Result = service.FindDataWithPaging(strCriteria,whereClauses, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditForm(int id)
        {

            VendorModel model = new VendorModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
				//define mapping Result here..
                //sampe:
				//model.MenuID = result.MenuID;
                //model.RoleID = result.RoleID;

            }

			//Example for combobox
            /*
			var Menu = menuService.FindAll();
            ViewBag.Menus = new SelectList(Menu.rows.ToList(), "ID", "DisplayMenu", model.MenuID);

            var Role = roleService.FindAll();
            ViewBag.Roles = new SelectList(Role.rows.ToList(), "ID", "RoleName", model.RoleID);
			*/

            return PartialView("~/Areas/MasterData/Views/Vendor/PVendor.cshtml", model);
        }

        public ActionResult ViewForm(int id)
        {

            VendorModel model = new VendorModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;                
            }

            return PartialView("~/Areas/MasterData/Views/Vendor/PViewVendor.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(VendorModel mdl)
        {
            SessionModel susr = (SessionModel)Session["User"];
            var result =  new MessageModel<VendorModel>();
            string activity="";
            if (mdl.ID > 0)
            {
                //do update
                //mdl.UpdatedBy = usr.Username;
                result = service.Update(mdl);
                activity="Ubah Data ID = " + mdl.ID;                
            }
            else
            {
                //mdl.CreatedBy = usr.Username;
                result = service.Add(mdl);                
                activity="Tambah Data";
            }
            logService.Add(new LogModel { Username = susr.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = activity +" di "+moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];            
            var data = service.FindByID(id).data;
            var result = service.Delete(data);
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete data Kode :" +  data.KodeVendor + ", ID : " + id + " di " + moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}
