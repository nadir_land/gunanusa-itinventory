﻿using App.Core;
using App.Core.Interface;
using App.Model;
using App.Web.Areas.Master.Models;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;

namespace App.Web.Areas.Report.Controllers
{
    public class ProcessReportController : BaseController
    {
		
        // GET: Report/ProcessReport
        public ActionResult Index(string type)
        {
            string pageName = "";
            if (type == "Pemasukan")
            {
                pageName = "Pemasukan Bahan Baku";
            }
            else if (type == "Pemakaian")
            {
                pageName = "Pemakaian Bahan Baku";
            }
            else if (type == "HasilProduksi")
            {
                pageName = "Hasil Produksi dari Subkon";
            }
            else if (type == "PemasukanHasilProduksi")
            {
                pageName = "Pemasukan Hasil Produksi";
            }
            else if (type == "PengeluaranHasilProduksi")
            {
                pageName = "Pengeluaran Hasil Produksi";
            }
            else if (type == "MutasiBahanBaku")
            {
                pageName = "Mutasi Bahan Baku";
            }
            else if (type == "MutasiHasilProduksi")
            {
                pageName = "Mutasi Hasil Produksi";
            }
            else if (type == "WasteScrap")
            {
                pageName = "WasteScrap";
            }

            SessionModel susrs = (SessionModel)Session["User"];
            ILogService logService = new LogService();
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = pageName, Activity = "View Report " + pageName });

            ViewBag.PageName = pageName;
            ViewBag.Type = type;
            return View();
        }

    }
}