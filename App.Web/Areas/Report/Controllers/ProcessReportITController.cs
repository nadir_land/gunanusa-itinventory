﻿using App.Core;
using App.Core.Interface;
using App.Model;
using App.Web.Areas.Master.Models;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;

namespace App.Web.Areas.Report.Controllers
{
    public class ProcessReportITController : BaseController
    {
		
        // GET: Report/ProcessReport
        public ActionResult Index(string type)
        {
            string pageName = "";
            if (type == "Vendor")
            {
                pageName = "Report Vendor";
            }
            else if (type == "Customer")
            {
                pageName = "Report Customer";
            }
            else if (type == "RawMaterial")
            {
                pageName = "Report Raw Material Import";
            }
            else if (type == "FG")
            {
                pageName = "Report Finish Goods";
            }
            else if (type == "KonversiProduct")
            {
                pageName = "Report Konversi Product";
            }
            else if (type == "Penerimaan")
            {
                pageName = "Report Penerimaan Barang";
            }
            else if (type == "WO")
            {
                pageName = "Report Work Order";
            }
            else if (type == "MaterialRelease")
            {
                pageName = "Report Material Release";
            }
            else if (type == "FinishGoodIn")
            {
                pageName = "Report Finish Goods In";
            }
            else if (type == "DO")
            {
                pageName = "Report Delivery Order";
            }
            else if (type == "WasteScrap")
            {
                pageName = "Report Waste/Scrap";
            }

            SessionModel susrs = (SessionModel)Session["User"];
            ILogService logService = new LogService();
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = pageName, Activity = "View Report IT " + pageName });

            ViewBag.PageName = pageName;
            ViewBag.Type = type;
            return View();
        }

    }
}