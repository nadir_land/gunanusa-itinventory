using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;
using App.Core;
using Newtonsoft.Json;
using App.Web.Areas.Transaction;
using App.Repository.Repository;
using App.Repository.Domain;

namespace App.Web.Areas.Transaction.Controllers
{
    public class DeliveryOrderController : BaseController
    {
        private string moduleName = "DeliveryOrder";
		IDeliveryOrderService service;
        IDeliveryOrderDetailService detailService;
        ILogService logService;
        ICustomerService customerService;
        IConsoleService consoleService;
        IKonversiProductService konversiProductService;
        IMaterialReleaseService materialReleaseService;
        IPenerimaanBarangService penerimaanBarangService;
        /*
			GiftModel  --> from App.Models
		*/


        public DeliveryOrderController(IDeliveryOrderService _service, ILogService logService, ICustomerService customerService, IConsoleService consoleService, IDeliveryOrderDetailService detailService, IKonversiProductService konversiProductService, IMaterialReleaseService materialReleaseService, IPenerimaanBarangService penerimaanBarangService)
        {
            service = _service;
            this.logService = logService;
            this.customerService = customerService;
            this.consoleService = consoleService;
            this.detailService = detailService;
            this.konversiProductService = konversiProductService;
            this.materialReleaseService = materialReleaseService;
            this.penerimaanBarangService = penerimaanBarangService;
        }

        // GET: DeliveryOrder
        public ActionResult Index()
        {
            consoleService.RunConsoleApp();
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
			ViewBag.PageName = "Delivery Order";
            ViewBag.RoleName = susrs.RoleName;
            return View();
        }

        public ActionResult WasteScrap()
        {
            consoleService.RunConsoleApp();
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View Waste / Scrap"});
            ViewBag.PageName = "Waste / Scrap";
            ViewBag.RoleName = susrs.RoleName;
            return View();
        }

        #region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
			//clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }
			

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
				strCriteria = "ID like  '%"+ param.sSearch + "%' OR NoDO like  '%"+ param.sSearch + "%' OR TglDO like  '%"+ param.sSearch + "%' OR Status like  '%"+ param.sSearch + "%' OR KodeCustomer like  '%"+ param.sSearch + "%' OR NamaCustomer like  '%"+ param.sSearch + "%'";
            }
            
            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<DeliveryOrderModel> Result = service.FindDataWithPaging(strCriteria,whereClauses, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDisplayDataWasteScrap(DataTablesParam param, string EName)
        {
            //clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }


            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "Status='Approve'";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
                strCriteria += " AND NoDO like  '%" + param.sSearch + "%' OR TglDO like  '%" + param.sSearch + "%'";
            }

            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<DeliveryOrderModel> Result = service.FindDataWithPaging(strCriteria, whereClauses, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditForm(int id)
        {
            ViewBag.IsExist = false;
            ViewBag.PageName = "Delivery Order";
            DeliveryOrderModel model = new DeliveryOrderModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                //find the detail
                List<WhereClause> whs = new List<WhereClause>();
                whs.Add(new WhereClause { Property = "DOID", Value = id });
                List<DeliveryOrderDetailModel> details = new List<DeliveryOrderDetailModel>();
                List<DeliveryOrderDetailModel> deliveryOrderDetails = detailService.FindByCriteria("DOID=@DOID", whs, "ID").rows.ToList();
                foreach(var detail in deliveryOrderDetails)
                {

                    if(String.IsNullOrEmpty(detail.NamaFG))
                    {
                        //Find Master Data Finish Goods
                        FinishedGoodsRepository finishedGoodsRepository = new FinishedGoodsRepository();
                        List<WhereClause> wh1 = new List<WhereClause>();
                        wh1.Add(new WhereClause { Property = "KodeFG", Value = detail.KodeFG });
                        var finishgoods = finishedGoodsRepository.FindByCriteria("KodeFG=@KodeFG", wh1, "ID").ToList();
                        if (finishgoods.Count > 0)
                        {
                            //take the first row
                            detail.NamaFG = finishgoods[0].NamaFG;
                            detail.HSExport = finishgoods[0].HSExport;
                            detail.Satuan = finishgoods[0].Satuan;
                        }
                        //end for Finish Goods
                    }
                    //assign detail to header
                    details.Add(detail);
                }

                model.Details = details;

                //cek existing data approved based on no. Transaction
                List<WhereClause> wh = new List<WhereClause>();
                wh.Add(new WhereClause { Property = "NoDO", Value = model.NoDO });
                var ExistingData = service.FindByCriteria("NoDO=@NoDO AND Status='Approve'", wh, "ID").rows.ToList();
                if (ExistingData.Count > 0)
                {
                    ViewBag.IsExist = true;
                }
            }

            var Customer = customerService.FindAll();
            ViewBag.CustomerList = Customer.rows.ToList();

            return View("PDeliveryOrder", model);
        }

        public ActionResult ViewForm(int id)
        {

            ViewBag.PageName = "Delivery Order";
            DeliveryOrderModel model = new DeliveryOrderModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                //find the detail
                List<WhereClause> whs = new List<WhereClause>();
                whs.Add(new WhereClause { Property = "DOID", Value = id });
                List<DeliveryOrderDetailModel> deliveryOrderDetails = detailService.FindByCriteria("DOID=@DOID", whs, "ID").rows.ToList();
                //assign detail to header
                model.Details = deliveryOrderDetails;
            }

            return View("PViewDeliveryOrder", model);
        }

        public ActionResult ViewWasteScrap(int id)
        {

            ViewBag.PageName = "WASTE/SCRAP";
            DeliveryOrderModel model = new DeliveryOrderModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                //find the detail
                List<DeliveryOrderDetailModel> deliveryOrderDetails = new List<DeliveryOrderDetailModel>();
                List<WhereClause> whs = new List<WhereClause>();
                whs.Add(new WhereClause { Property = "DOID", Value = id });
                var source = detailService.FindByCriteria("DOID=@DOID", whs, "ID").rows.ToList();
                foreach (var itm in source)
                {
                    DeliveryOrderDetailModel doDModel = new DeliveryOrderDetailModel();
                    doDModel.KodeFG = itm.KodeFG;
                    doDModel.NamaFG = itm.NamaFG;
                    doDModel.QtyKirim = itm.QtyKirim;
                    doDModel.HSExport = itm.HSExport;
                    doDModel.HargaSatuan = itm.HargaSatuan;
                    doDModel.JumlahHarga = itm.JumlahHarga;
                    doDModel.QuantityEquivalent = 0;
                    doDModel.Nilai = 0;                    
                    List<WhereClause> wh = new List<WhereClause>();
                    wh.Add(new WhereClause { Property = "KodeFG", Value = itm.KodeFG });
                    wh.Add(new WhereClause { Property = "Status", Value = "Approve" });
                    var konversiDatas = konversiProductService.FindByCriteria("KodeFG=@KodeFG and Status=@Status", wh, "ID").rows.ToList();
                    if (konversiDatas.Count > 0)
                    {
                        var konversiData = konversiDatas[0];
                        doDModel.QuantityEquivalent = (doDModel.QtyKirim / konversiData.Kandungan) * konversiData.Waste;
                        doDModel.KodeBarang = konversiData.KodeBarang;
                        doDModel.NamaBarang = konversiData.NamaBarang;
                        doDModel.Satuan = konversiData.Satuan;

                        #region Waste
                        decimal tDOQtyWaste = itm.QuantityEquivalent + konversiData.Waste;
                        #endregion

                        List<WhereClause> wh2 = new List<WhereClause>();
                        wh2.Add(new WhereClause { Property = "KodeBarang", Value = konversiData.KodeBarang });
                        wh2.Add(new WhereClause { Property = "Status", Value = "Approve" });
                        var penerimaanBarang = penerimaanBarangService.FindByCriteria("Status=@Status and KodeBarang=@KodeBarang", wh2, "ID").rows.ToList();
                        if (penerimaanBarang.Count > 0)
                        {
                            decimal SisaSaldoQty = 0;
                            decimal bmPIB = 0;
                            decimal qtyPIB = 0;
                            foreach (var p in penerimaanBarang)
                            {
                                bmPIB = p.BmPIB;
                                qtyPIB = p.QtyPIB;
                                SisaSaldoQty = p.QtyPIB - tDOQtyWaste;
                                if (SisaSaldoQty > 0)
                                {
                                    break;
                                }
                            }

                            decimal hargaSatuan = bmPIB / qtyPIB;
                            doDModel.Nilai = doDModel.QuantityEquivalent * hargaSatuan;
                        }
                    }
                    /*
                    var MaterialReleases = materialReleaseService.FindByCriteria("KodeFG=@KodeFG and Status=@Status", wh, "ID").rows.ToList();
                    if (MaterialReleases.Count > 0)
                    { 
                        var materialRelease = MaterialReleases[0];
                        doDModel.KodeBarang = materialRelease.KodeBarang;
                        doDModel.NamaBarang = materialRelease.NamaBarang;
                        doDModel.Satuan = materialRelease.Satuan;
                    }
                    */
                    deliveryOrderDetails.Add(doDModel);
                }               
                //assign detail to header
                model.Details = deliveryOrderDetails;
            }

            return View("PViewWasteScrap", model);
        }

        [HttpPost]
        public ActionResult Save(string DO, string Detail)
        {

            DeliveryOrderModelView mdlview = JsonConvert.DeserializeObject<DeliveryOrderModelView>(DO);
            List<DeliveryOrderDetailModelView> detailview = JsonConvert.DeserializeObject<List<DeliveryOrderDetailModelView>>(Detail);
            SessionModel susrs = (SessionModel)Session["User"];
            var result =  new MessageModel<DeliveryOrderModel>();
            string activity="";
            //remap
            DeliveryOrderModel mdl = new DeliveryOrderModel();
            List<DeliveryOrderDetailModel> detailModels = new List<DeliveryOrderDetailModel>();
            AutoMapper.Mapper.Map(mdlview, mdl);
            AutoMapper.Mapper.Map(detailview, detailModels);
            if (mdl.ID > 0)
            {
                //validate FG in details
                bool isValidFG = true;
                foreach (var detail in detailModels)
                { 
                    if(String.IsNullOrEmpty(detail.NamaFG))
                    {
                        isValidFG = false;
                        break;
                    }
                }

                if(!isValidFG)
                {
                    result.success=false;
                    result.message = "No. FG tidak ditemukan di Finish goods, Silahkan dilengkapi terlebih dahulu!";
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                    
                //do update    
                //mdl.UpdatedBy = usr.Username;                    
                result = service.Update(mdl);
                if (result.success)
                {
                    foreach (var detail in detailModels)
                    { 
                        int id = Convert.ToInt16(detail.SID);
                        var detaildata = detailService.FindByID(id).data;
                        if (detaildata != null)
                        {
                            detaildata.NamaFG = detail.NamaFG;
                            detail.HSExport = detaildata.HSExport;
                            detail.Satuan = detaildata.Satuan;
                            detaildata.HargaSatuan = detail.HargaSatuan;
                            detaildata.JumlahHarga = detail.HargaSatuan * detaildata.QtyKirim;
                            detailService.Update(detaildata);
                        }
                    }
                }
                activity="Ubah Data ID = " + mdl.ID;                
            }
            else
            {
                //mdl.CreatedBy = usr.Username;
                result = service.Add(mdl);                
                activity="Tambah Data";
            }
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = activity +" di "+moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];            
            var data = service.FindByID(id).data;
            var result = service.Delete(data);
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete data No DO :" + data.NoDO + ", ID : " + id + " di " + moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}
