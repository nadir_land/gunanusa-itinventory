using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;
using App.Repository.Repository;
using System.Web.Services.Description;

namespace App.Web.Areas.Transaction.Controllers
{
    public class WorkOrderController : BaseController
    {
        private string moduleName = "WorkOrder";
		IWorkOrderService service;
        ILogService logService;
        ICustomerService customerService;
        IConsoleService consoleService;
        /*
			GiftModel  --> from App.Models
		*/


        public WorkOrderController(IWorkOrderService _service, ILogService logService, ICustomerService customerService, IConsoleService consoleService)
        {
            service = _service;
            this.logService = logService;
            this.customerService = customerService;
            this.consoleService = consoleService;
        }

        // GET: WorkOrder
        public ActionResult Index()
        {
            consoleService.RunConsoleApp();
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
			ViewBag.PageName = "Work Order";
            ViewBag.RoleName = susrs.RoleName;
            return View();
        }

		#region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
			//clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }
			

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
				strCriteria = "ID like  '%"+ param.sSearch + "%' OR NoWO like  '%"+ param.sSearch + "%' OR TglWO like  '%"+ param.sSearch + "%' OR KodeCustomer like  '%"+ param.sSearch + "%' OR NamaCustomer like  '%"+ param.sSearch + "%' OR KodeFG like  '%"+ param.sSearch + "%' OR QtyOrder like  '%"+ param.sSearch + "%' OR Status like  '%"+ param.sSearch + "%'";
            }
            
            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<WorkOrderModel> Result = service.FindDataWithPaging(strCriteria,whereClauses, param.iDisplayLength, pageNo, "ID DESC");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditForm(int id)
        {
            ViewBag.IsExist = false;
            ViewBag.PageName = "Work Order";
            WorkOrderModel model = new WorkOrderModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                if (model != null)
                {
                    if(String.IsNullOrEmpty(model.NamaFG))
                    {
                        //Find BOM (Konversi)
                        KonversiProductRepository konversiProductRepository = new KonversiProductRepository();
                        List<WhereClause> wh2 = new List<WhereClause>();
                        wh2.Add(new WhereClause { Property = "KodeFG", Value = model.KodeFG });
                        var konversiProducts = konversiProductRepository.FindByCriteria("KodeFG=@KodeFG AND Status='Approve'", wh2, "ID").ToList();
                        if (konversiProducts.Count > 0)
                        {
                            //take the first row
                            model.NamaFG = konversiProducts[0].NamaFG;
                            model.HSExport = konversiProducts[0].HSExport;
                            model.SatuanExport = konversiProducts[0].Satuan;
                            model.Foto = konversiProducts[0].Foto;
                            model.KodeBarang = konversiProducts[0].KodeBarang;
                            model.NamaBarang = konversiProducts[0].NamaBarang;
                            model.HSImport = konversiProducts[0].HSImport;
                            model.Satuan = konversiProducts[0].Satuan;
                            model.Koefisien = konversiProducts[0].Koefisien;
                            model.Dibutuhkan = konversiProducts[0].Koefisien * model.QtyOrder;
                        }
                    }

                    if (model.Foto != null)
                    {
                        string base64String = Convert.ToBase64String(model.Foto);
                        string imageSource = string.Format("data:image/jpeg;base64,{0}", base64String);
                        model.imageBase64String = imageSource;
                    }

                    //cek existing data approved based on no. Transaction
                    List<WhereClause> wh = new List<WhereClause>();
                    wh.Add(new WhereClause { Property = "NoWO", Value = model.NoWO });
                    var ExistingData = service.FindByCriteria("NoWO=@NoWO AND Status='Approve'", wh, "ID").rows.ToList();
                    if (ExistingData.Count > 0)
                    {
                        ViewBag.IsExist = true;
                    }
                }
            }

            var Customer = customerService.FindAll();
            ViewBag.CustomerList = Customer.rows.ToList();

            return View("PWorkOrder", model);
        }

        public ActionResult ViewForm(int id)
        {
            ViewBag.PageName = "Work Order";
            WorkOrderModel model = new WorkOrderModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                if (model != null)
                {
                    if (model.Foto != null)
                    {
                        string base64String = Convert.ToBase64String(model.Foto);
                        string imageSource = string.Format("data:image/jpeg;base64,{0}", base64String);
                        model.imageBase64String = imageSource;
                    }
                }
            }

            return View("PViewWorkOrder", model);
        }

        [HttpPost]
        public ActionResult Save(WorkOrderModel mdl)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            var result =  new MessageModel<WorkOrderModel>();
            string activity="";
            if (mdl.ID > 0)
            {
                //do update
                //mdl.UpdatedBy = usr.Username;
                result = service.Update(mdl);
                activity="Approve Data ID = " + mdl.ID;                
            }
            else
            {
                //mdl.CreatedBy = usr.Username;
                result = service.Add(mdl);                
                activity="Tambah Data";
            }
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = activity +" di "+moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            var data = service.FindByID(id).data;
            var result = service.Delete(data);
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete data No WO :" + data.NoWO + ", ID : " + id + " di " + moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}
