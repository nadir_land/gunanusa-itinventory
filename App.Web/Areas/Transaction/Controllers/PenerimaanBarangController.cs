using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;
using System.Diagnostics;
using App.Repository.Domain;
using App.Repository.Repository;


namespace App.Web.Areas.Transaction.Controllers
{
    public class PenerimaanBarangController : BaseController
    {
        private string moduleName = "PenerimaanBarang";
		IPenerimaanBarangService service;
        IRawMaterialService materialService;
        ILogService logService;
        IConsoleService consoleService;
		/*
			GiftModel  --> from App.Models
		*/

        
		public PenerimaanBarangController(IPenerimaanBarangService _service, ILogService logService, IConsoleService consoleService, IRawMaterialService materialService)
        {
            service = _service;
            this.logService = logService;
            this.consoleService = consoleService;
            this.materialService = materialService;
        }

        // GET: PenerimaanBarang
        public ActionResult Index()
        {
            consoleService.RunConsoleApp();
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
			ViewBag.PageName = "Penerimaan Barang";
            ViewBag.RoleName = susrs.RoleName;
            return View();
        }

		#region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
			//clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }
			

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
                strCriteria = "ID like  '%" + param.sSearch + "%' OR ReceiptNo like  '%" + param.sSearch + "%' OR ReceiptDate like  '%" + param.sSearch + "%' OR Warehouse like  '%" + param.sSearch + "%' OR CAR like  '%" + param.sSearch + "%' OR NoPIB like  '%" + param.sSearch + "%' OR  CONVERT(VARCHAR(25), TanggalPIB, 126) like  '%" + param.sSearch + "%' OR KPBC like  '%" + param.sSearch + "%' OR AsalBarang like  '%" + param.sSearch + "%' OR Valuta like  '%" + param.sSearch + "%' OR Rate like  '%" + param.sSearch + "%' OR Status like  '%" + param.sSearch + "%' OR KodeBarang like  '%" + param.sSearch + "%' OR NamaBarang like  '%" + param.sSearch + "%' OR HSImport like  '%" + param.sSearch + "%' OR Satuan like  '%" + param.sSearch + "%' OR NoSeri like  '%" + param.sSearch + "%' OR TarifBM like  '%" + param.sSearch + "%' OR CifPIB like  '%" + param.sSearch + "%' OR QtyPIB like  '%" + param.sSearch + "%' OR BmPIB like  '%" + param.sSearch + "%' OR PpnPIB like  '%" + param.sSearch + "%' OR QtyRI like  '%" + param.sSearch + "%'";
            }            
            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<PenerimaanBarangModel> Result = service.FindDataWithPaging(strCriteria,whereClauses, param.iDisplayLength, pageNo, "ID DESC");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditForm(int id)
        {
            ViewBag.PageName = "Detail Penerimaan";
            PenerimaanBarangModel model = new PenerimaanBarangModel();
            PenerimaanBarangItemModel item = new PenerimaanBarangItemModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                //define mapping Result here..
                //sampe:
                //model.MenuID = result.MenuID;
                //model.RoleID = result.RoleID;                
            }
            else {
                //default
                model.Valuta = "USD";                
                model.Status = "New";
                model.TanggalPIB = DateTime.Now;
            }

			//Example for combobox
            /*
			var Menu = menuService.FindAll();
            ViewBag.Menus = new SelectList(Menu.rows.ToList(), "ID", "DisplayMenu", model.MenuID);

            var Role = roleService.FindAll();
            ViewBag.Roles = new SelectList(Role.rows.ToList(), "ID", "RoleName", model.RoleID);
			*/

            return View("PenerimaanBarang", model);
        }

        public ActionResult ViewForm(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View Detail " + moduleName });

            ViewBag.PageName = "Detail Penerimaan";
            PenerimaanBarangModel model = new PenerimaanBarangModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;                
            }

            return View("PViewPenerimaanBarang", model);
        }

        public ActionResult ViewApproveForm(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View Detail " + moduleName });
            ViewBag.PageName = "Detail Penerimaan";
            ViewBag.IsExist = false;
            PenerimaanBarangModel model = new PenerimaanBarangModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                //default value
                model.Valuta = "USD";
                model.AsalBarang = "US";
                //cek If Material already filled.
                if (String.IsNullOrEmpty(model.NamaBarang))
                {
                    //filled from material Master
                    List<WhereClause> wh2 = new List<WhereClause>();
                    wh2.Add(new WhereClause { Property = "KodeMaterial", Value = model.KodeBarang });
                    var rawMaterials = materialService.FindByCriteria("KodeMaterial=@KodeMaterial", wh2, "ID").rows.ToList();
                    if (rawMaterials.Count > 0)
                    {
                        //take the first row
                        model.NamaBarang = rawMaterials[0].NamaMaterial;
                        model.HSImport = rawMaterials[0].HSImport;
                        model.Satuan = rawMaterials[0].KodeSatuan;
                    }

                }


                //cek existing data approved based on no. Receipt
                List<WhereClause> wh = new List<WhereClause>();
                wh.Add(new WhereClause { Property = "ReceiptNo", Value = model.ReceiptNo });
                var ExistingData = service.FindByCriteria("ReceiptNo=@ReceiptNo AND Status='Approve'", wh, "ID").rows.ToList();
                if (ExistingData.Count > 0)
                {
                    ViewBag.IsExist = true;
                }
            }

            return View("ApprovePenerimaanBarang", model);
        }

        [HttpPost]
        public ActionResult Save(PenerimaanBarangModel mdl)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            var result =  new MessageModel<PenerimaanBarangModel>();
            string activity="";
            if (mdl.ID > 0)
            {
                //do update
                //mdl.UpdatedBy = usr.Username;
                result = service.Update(mdl);
                activity= "Approve Data ID = " + mdl.ID;                
            }
            else
            {
                //mdl.CreatedBy = usr.Username;
                result = service.Add(mdl);                
                activity="Tambah Data";
            }
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = activity +" di "+moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            var data = service.FindByID(id).data;
            var result = service.Delete(data);
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete data Receipt No :" + data.ReceiptNo + ", ID : " + id + " di " + moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}
