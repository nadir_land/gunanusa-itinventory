using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;
using App.Core;
using App.Repository.Repository;
using App.Repository.Domain;

namespace App.Web.Areas.Transaction.Controllers
{
    public class FinishGoodsInResultController : BaseController
    {
        private string moduleName = "FinishGoodsInResult";
		IFinishGoodsInResultService service;
        ILogService logService;
        IVendorService vendorService;
        IConsoleService consoleService;
        /*
			GiftModel  --> from App.Models
		*/


        public FinishGoodsInResultController(IFinishGoodsInResultService _service, ILogService logService,IVendorService vendorService, IConsoleService consoleService)
        {
            service = _service;
            this.logService = logService;
            this.vendorService = vendorService;
            this.consoleService = consoleService;
        }

        // GET: FinishGoodsInResult
        public ActionResult Index()
        {
            consoleService.RunConsoleApp();
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
			ViewBag.PageName = "Finish Goods in Result";
            ViewBag.RoleName = susrs.RoleName;
            return View();
        }

		#region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
			//clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }
			

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
				strCriteria = "ID like  '%"+ param.sSearch + "%' OR NoWO like  '%"+ param.sSearch + "%' OR TglWO like  '%"+ param.sSearch + "%' OR NoResult like  '%"+ param.sSearch + "%' OR TglResult like  '%"+ param.sSearch + "%' OR KodeFG like  '%"+ param.sSearch + "%' OR Status like  '%"+ param.sSearch + "%'";
            }
            
            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<FinishGoodsInResultModel> Result = service.FindDataWithPaging(strCriteria,whereClauses, param.iDisplayLength, pageNo, "ID DESC");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditForm(int id)
        {
            ViewBag.IsExist = false;
            ViewBag.PageName = "Finish Goods in Result";
            FinishGoodsInResultModel model = new FinishGoodsInResultModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;

                if(String.IsNullOrEmpty(model.NamaFG))
                {
                    //Find Master Data Finish Goods
                    FinishedGoodsRepository finishedGoodsRepository = new FinishedGoodsRepository();
                    List<WhereClause> wh1 = new List<WhereClause>();
                    wh1.Add(new WhereClause { Property = "KodeFG", Value = model.KodeFG });
                    var finishgoods = finishedGoodsRepository.FindByCriteria("KodeFG=@KodeFG", wh1, "ID").ToList();
                    if (finishgoods.Count > 0)
                    {
                        //take the first row
                        model.NamaFG = finishgoods[0].NamaFG;
                        model.HSExport = finishgoods[0].HSExport;
                        model.SatuanExport = finishgoods[0].Satuan;
                    }
                    //end for Finish Goods
                }

                if(model.QtyOrder==0)
                {
                    WorkOrderRepository workOrderRepository = new WorkOrderRepository();
                    List<WhereClause> wh3 = new List<WhereClause>();
                    wh3.Add(new WhereClause { Property = "NoWO", Value = model.NoWO });
                    var WOData = workOrderRepository.FindByCriteria("NoWO=@NoWO and Status='Approve'", wh3, "ID").ToList();
                    if (WOData.Count > 0)
                    {
                        model.QtyOrder = WOData[0].QtyOrder;
                        model.TglWO = WOData[0].TglWO;
                    }

                    model.DlmProses = model.QtyOrder - model.AkanKirim;
                }

                //cek existing data approved based on no. Transaction
                List<WhereClause> wh = new List<WhereClause>();
                wh.Add(new WhereClause { Property = "NoResult", Value = model.NoResult });
                var ExistingData = service.FindByCriteria("NoResult=@NoResult AND Status='Approve'", wh, "ID").rows.ToList();
                if (ExistingData.Count > 0)
                {
                    ViewBag.IsExist = true;
                }
            }
            //var VendorList = vendorService.FindAll().rows.ToList(); #### Remark source data not defined. make it blank, based on meeting 04-07-2023
            var VendorList = new List<VendorModel>();
            ViewBag.VendorList = VendorList;

            return View("PFinishGoodsInResult", model);
        }

        public ActionResult ViewForm(int id)
        {
            ViewBag.PageName = "Finish Goods in Result";
            FinishGoodsInResultModel model = new FinishGoodsInResultModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;                
            }

            return PartialView("~/Areas/Transaction/Views/FinishGoodsInResult/PViewFinishGoodsInResult.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(FinishGoodsInResultModel mdl)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            var result =  new MessageModel<FinishGoodsInResultModel>();
            string activity="";
            if (mdl.ID > 0)
            {
                //do update
                //mdl.UpdatedBy = usr.Username;
                result = service.Update(mdl);
                activity="Ubah Data ID = " + mdl.ID;                
            }
            else
            {
                //mdl.CreatedBy = usr.Username;
                result = service.Add(mdl);                
                activity="Tambah Data";
            }
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = activity +" di "+moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];            
            var data = service.FindByID(id).data;
            var result = service.Delete(data);
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete data No Result :" + data.NoResult + ", ID : " + id + " di " + moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}
