using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;
using App.Core;
using App.Repository.Repository;
using System.Web.Services.Description;

namespace App.Web.Areas.Transaction.Controllers
{
    public class KonversiProductController : BaseController
    {
        private string moduleName = "KonversiProduct";
		IKonversiProductService service;
        ILogService logService;
        IConsoleService consoleService;
        /*
			GiftModel  --> from App.Models
		*/


        public KonversiProductController(IKonversiProductService _service, ILogService logService, IConsoleService consoleService)
        {
            service = _service;
            this.logService = logService;
            this.consoleService = consoleService;
        }

        // GET: KonversiProduct
        public ActionResult Index()
        {
            consoleService.RunConsoleApp();
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
			ViewBag.PageName = "Konversi Product";
            ViewBag.RoleName = susrs.RoleName;
            return View();
        }

		#region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
			//clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }
			

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
				strCriteria = "ID like  '%"+ param.sSearch + "%' OR KodeFG like  '%"+ param.sSearch + "%' OR NamaFG like  '%"+ param.sSearch + "%' OR BOM like  '%"+ param.sSearch + "%' OR HSExport like  '%"+ param.sSearch + "%' OR SatuanExport like  '%"+ param.sSearch + "%' OR Keterangan like  '%"+ param.sSearch + "%' OR Status like  '%"+ param.sSearch + "%'";
            }
            /*
            //##If You use Custom filter modified this filter
            if (!string.IsNullOrEmpty(EName))
            {
                if (strCriteria != "")
                {
					//define logic here..
					//sample
                    //strCriteria += " AND RoleID in (Select ID from MsRole where RoleName like  '%" + EName + "%')";
                }
                else
                {
					//define logic here..
					//sample
                    //strCriteria = " RoleID in (Select ID from MsRole where RoleName like  '%" + EName + "%')";
                }
            }
            */
            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<KonversiProductModel> Result = service.FindDataWithPaging(strCriteria,whereClauses, param.iDisplayLength, pageNo, "ID DESC");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditForm(int id)
        {
            ViewBag.PageName = "Konversi Product";
            ViewBag.IsExist = false;
            KonversiProductModel model = new KonversiProductModel();
            try
            {
                if (id > 0)
                {
                    model = service.FindByID(id).data;
                    //Mapping
                    decimal qtyBarang = model.QtyBarang;
                    decimal qtyBom = model.QtyBom;
                    decimal koefisien = qtyBarang / qtyBom;
                    decimal kandungan = (qtyBom / qtyBarang) * 100;
                    decimal waste = qtyBom - kandungan;
                    //assign value
                    model.Koefisien = koefisien;
                    model.Kandungan = kandungan;
                    model.Waste = waste;
                    if (model.Foto != null)
                    {
                        string base64String = Convert.ToBase64String(model.Foto);
                        string imageSource = string.Format("data:image/jpeg;base64,{0}", base64String);
                        model.imageBase64String = imageSource;
                    }

                    //cek If Master data FG is not defined
                    if(String.IsNullOrEmpty(model.NamaFG))
                    {
                        //cek Master Data:
                        //Find Master Data Finish Goods
                        FinishedGoodsRepository finishedGoodsRepository = new FinishedGoodsRepository();
                        List<WhereClause> whfg = new List<WhereClause>();
                        whfg.Add(new WhereClause { Property = "KodeFG", Value = model.KodeFG });
                        var finishgoods = finishedGoodsRepository.FindByCriteria("KodeFG=@KodeFG", whfg, "ID").ToList();
                        if (finishgoods.Count > 0)
                        {
                            //take the first row
                            model.NamaFG = finishgoods[0].NamaFG;
                            model.HSExport = finishgoods[0].HSExport;
                            model.SatuanExport = finishgoods[0].Satuan;
                            model.Keterangan = finishgoods[0].Keterangan;
                            model.Foto = finishgoods[0].Foto;
                        }
                        //end for Finish Goods                        
                    }

                    if(String.IsNullOrEmpty(model.NamaBarang))
                    {
                        //Find Master Data Raw material
                        RawMaterialRepository rawMaterialRepository = new RawMaterialRepository();
                        List<WhereClause> wh2 = new List<WhereClause>();
                        wh2.Add(new WhereClause { Property = "KodeMaterial", Value = model.KodeBarang });
                        var rawMaterials = rawMaterialRepository.FindByCriteria("KodeMaterial=@KodeMaterial", wh2, "ID").ToList();
                        if (rawMaterials.Count > 0)
                        {
                            //take the first row
                            model.NamaBarang = rawMaterials[0].NamaMaterial;
                            model.HSImport = rawMaterials[0].HSImport;
                            model.Satuan = rawMaterials[0].KodeSatuan;
                        }
                    }


                    //cek existing data approved based on no. Transaction
                    List<WhereClause> wh = new List<WhereClause>();
                    wh.Add(new WhereClause { Property = "BOM", Value = model.BOM });
                    var ExistingData = service.FindByCriteria("BOM=@BOM AND Status='Approve'", wh, "ID").rows.ToList();
                    if (ExistingData.Count > 0)
                    {
                        ViewBag.IsExist = true;
                    }
                }                
            }
            catch
            {
                throw;
            }

            return View("PKonversiProduct", model);
        }

        public ActionResult ViewForm(int id)
        {
            ViewBag.PageName = "Konversi Product";
            KonversiProductModel model = new KonversiProductModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
                if (model.Status == "New")
                {
                    //Mapping
                    decimal qtyBarang = model.QtyBarang;
                    decimal qtyBom = model.QtyBom;
                    decimal koefisien = qtyBarang / qtyBom;
                    decimal kandungan = (qtyBom / qtyBarang) * 100;
                    decimal waste = qtyBom - kandungan;
                    //assign value
                    model.Koefisien = koefisien;
                    model.Kandungan = kandungan;
                    model.Waste = waste;
                }
                if (model.Foto != null)
                {
                    string base64String = Convert.ToBase64String(model.Foto);
                    string imageSource = string.Format("data:image/jpeg;base64,{0}", base64String);
                    model.imageBase64String = imageSource;
                }
            }

            return PartialView("~/Areas/Transaction/Views/KonversiProduct/PViewKonversiProduct.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(KonversiProductModel mdl)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            var result =  new MessageModel<KonversiProductModel>();
            string activity="";
            if (mdl.ID > 0)
            {
                //do update
                //mdl.UpdatedBy = usr.Username;
                result = service.Update(mdl);
                activity= "Approve Data ID = " + mdl.ID;                
            }
            else
            {
                //mdl.CreatedBy = usr.Username;
                result = service.Add(mdl);                
                activity="Tambah Data";
            }
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = activity +" di "+moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];            
            var data = service.FindByID(id).data;
            var result = service.Delete(data);
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete data BOM :" + data.BOM  + ", ID : " + id + " di " + moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}
