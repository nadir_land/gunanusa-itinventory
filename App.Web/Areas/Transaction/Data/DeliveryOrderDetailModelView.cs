﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace App.Web.Areas.Transaction
{
    public class DeliveryOrderDetailModelView
    {
        public int ID { get; set; }
        [DisplayName("D O I D")]
        public int DOID { get; set; }
        [DisplayName("Kode F G")]
        public string KodeFG { get; set; }
        [DisplayName("Nama F G")]
        public string NamaFG { get; set; }
        [DisplayName("H S Export")]
        public string HSExport { get; set; }
        [DisplayName("Satuan")]
        public string Satuan { get; set; }
        [DisplayName("Qty Kirim")]
        public decimal QtyKirim { get; set; }
        [DisplayName("Harga Satuan")]
        public decimal HargaSatuan { get; set; }
        [DisplayName("Jumlah Harga")]
        public decimal JumlahHarga { get; set; }

        public string SID { get; set; }
    }
}