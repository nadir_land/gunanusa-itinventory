﻿using System.Web.Mvc;

namespace App.Web.Areas.XML
{
    public class XMLAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "XML";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "XML_default",
                "XML/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}