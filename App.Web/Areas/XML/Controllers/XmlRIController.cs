using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;

namespace App.Web.Areas.XML.Controllers
{
    public class XmlRIController : BaseController
    {
        private string moduleName = "XmlRI";
		IXmlRIService service;
        ILogService logService;
		/*
			GiftModel  --> from App.Models
		*/

        
		public XmlRIController(IXmlRIService _service, ILogService logService)
        {
            service = _service;
            this.logService = logService;
        }

        // GET: XmlRI
        public ActionResult Index()
        {
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
			ViewBag.PageName = "RI Download";
            return PartialView();
        }

		#region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
			//clean Parameter
            if (!String.IsNullOrEmpty(EName))
            {
                EName = SafeSqlLiteral(EName);
            }
            if (!String.IsNullOrEmpty(param.sSearch))
            {
                param.sSearch = SafeSqlLiteral(param.sSearch);
            }
			

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                //sample
				strCriteria = "ID like  '%"+ param.sSearch + "%' OR NOBUKMSK like  '%"+ param.sSearch + "%' OR TGBUKMSK like  '%"+ param.sSearch + "%' OR KOBAR like  '%"+ param.sSearch + "%' OR QTY like  '%"+ param.sSearch + "%' OR CONVERT(VARCHAR(25), DownloadDate, 126) like  '%" + param.sSearch + "%'";
            }            
            List<WhereClause> whereClauses = new List<WhereClause>();
            MessageModel<XmlRIModel> Result = service.FindDataWithPaging(strCriteria,whereClauses, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditForm(int id)
        {

            XmlRIModel model = new XmlRIModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;
				//define mapping Result here..
                //sampe:
				//model.MenuID = result.MenuID;
                //model.RoleID = result.RoleID;

            }

			//Example for combobox
            /*
			var Menu = menuService.FindAll();
            ViewBag.Menus = new SelectList(Menu.rows.ToList(), "ID", "DisplayMenu", model.MenuID);

            var Role = roleService.FindAll();
            ViewBag.Roles = new SelectList(Role.rows.ToList(), "ID", "RoleName", model.RoleID);
			*/

            return PartialView("~/Areas/XML/Views/XmlRI/PXmlRI.cshtml", model);
        }

        public ActionResult ViewForm(int id)
        {

            XmlRIModel model = new XmlRIModel();
            if (id > 0)
            {
                model = service.FindByID(id).data;                
            }

            return PartialView("~/Areas/XML/Views/XmlRI/PViewXmlRI.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(XmlRIModel mdl)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            var result =  new MessageModel<XmlRIModel>();
            string activity="";
            if (mdl.ID > 0)
            {
                //do update
                //mdl.UpdatedBy = usr.Username;
                result = service.Update(mdl);
                activity="Ubah Data ID = " + mdl.ID;                
            }
            else
            {
                //mdl.CreatedBy = usr.Username;
                result = service.Add(mdl);                
                activity="Tambah Data";
            }
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = activity +" di "+moduleName });
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete ID " + id + " di " + moduleName });
            var data = service.FindByID(id).data;
            var result = service.Delete(data);
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}
