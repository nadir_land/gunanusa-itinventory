﻿using App.Core.Interface;
using App.Model;
using App.Utility;
using App.Web.Areas.Master.Models;
using App.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;

namespace App.Web.Areas.Master.Controllers
{
    public class UserRoleController : BaseController
    {
        IUserRoleService service;
        IUserService userService;
        IRoleService roleService;
        public UserRoleController(IUserRoleService _service, IUserService _userService, IRoleService _roleService)
        {
            service = _service;
            userService = _userService;
            roleService = _roleService;
        }

        // GET: Master/UserRole
        public ActionResult Index()
        {
            ViewBag.PageName = "User Role Setting";
            return View();
        }

        #region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                strCriteria = " UserID in (Select ID from MsUser where Username like  '%" + param.sSearch + "%') OR RoleID in (Select ID from MsRole where RoleName like  '%" + param.sSearch + "%')";
            }
            if (!string.IsNullOrEmpty(EName))
            {
                if (strCriteria != "")
                {
                    strCriteria += " AND UserID in (Select ID from MsUser where Username like  '%" + EName + "%')";
                }
                else
                {
                    strCriteria = " UserID in (Select ID from MsUser where Username like  '%" + EName + "%')";
                }
            }
            List<WhereClause> whereClause = new List<WhereClause>();
            MessageModel<UserRoleModel> Result = service.FindDataWithPaging(strCriteria, whereClause, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditUserRole(int id)
        {
            
            UserRoleModelView model = new UserRoleModelView();
            if (id > 0)
            {
                var result = service.FindByID(id).data;
                model.UserID = result.UserID;
                model.RoleID = result.RoleID;
                
            }

            var User = userService.FindAll();
            ViewBag.Users = new SelectList(User.rows.ToList(), "ID", "UserName", model.UserID);

            var Role = roleService.FindAll();
            ViewBag.Roles = new SelectList(Role.rows.ToList(), "ID", "RoleName", model.RoleID);

            return PartialView("~/Areas/Master/Views/Shared/PAddEditUserRole.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(UserRoleModelView mdl)
        {
            UserRoleModel data = new UserRoleModel();
            AutoMapper.Mapper.Map(mdl, data);
            if (mdl.ID > 0)
            {
                //do update
                var result = service.Update(data);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = service.Add(data);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var usr = service.FindByID(id).data;
            var result = service.Delete(usr);
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}