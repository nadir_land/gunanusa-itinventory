﻿using App.Core;
using App.Core.Interface;
using App.Model;
using App.Utility;
using App.Web.Areas.Master.Models;
using App.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;

namespace App.Web.Areas.Master.Controllers
{
    public class RoleController : BaseController
    {
        IRoleService roleService;
        public RoleController(RoleService _roleService)
        {
            roleService = _roleService;
        }


        // GET: Master/User

        public ActionResult Index()
        {
            ViewBag.PageName = "Role Authorization";
            return View();
        }

        #region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                strCriteria = " rolename like '%" + param.sSearch + "%'";
            }
            if (!string.IsNullOrEmpty(EName))
            {
                if (strCriteria != "")
                {
                    strCriteria += " AND rolename like  '%" + EName + "%'";
                }
                else
                {
                    strCriteria = " rolename like  '%" + EName + "%'";
                }
            }
            List<WhereClause> whereClause = new List<WhereClause>();
            MessageModel<RoleModel> Result = roleService.FindDataWithPaging(strCriteria, whereClause, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditRole(int id)
        {
            RoleModelView model = new RoleModelView();
            if (id > 0)
            {
                var result = roleService.FindByID(id).data;
                model.RoleName = result.RoleName;              
            }

            return PartialView("~/Areas/Master/Views/Shared/PAddEditRole.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(RoleModelView mdl)
        {
            RoleModel usr = new RoleModel();
            AutoMapper.Mapper.Map(mdl, usr);            
            if (mdl.ID > 0)
            {
                //do update
                var result = roleService.Update(usr);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = roleService.Add(usr);
                return Json(result, JsonRequestBehavior.AllowGet);
            }            

        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var usr = roleService.FindByID(id).data;
            var result = roleService.Delete(usr);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}