﻿using App.Core.Interface;
using App.Model;
using App.Utility;
using App.Web.Areas.Master.Models;
using App.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;

namespace App.Web.Areas.Master.Controllers
{
    public class RoleMenuController : BaseController
    {
        IRoleMenuService service;
        IMenuService menuService;
        IRoleService roleService;
        public RoleMenuController(IRoleMenuService _service, IMenuService _menuService, IRoleService _roleService)
        {
            service = _service;
            menuService = _menuService;
            roleService = _roleService;
        }

        // GET: Master/RoleMenu
        public ActionResult Index()
        {
            ViewBag.PageName = "Role Menu Setting";
            return View();
        }

        #region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                strCriteria = " RoleID in (Select ID from MsRole where RoleName like  '%" + param.sSearch + "%')";
            }
            if (!string.IsNullOrEmpty(EName))
            {
                if (strCriteria != "")
                {
                    strCriteria += " AND RoleID in (Select ID from MsRole where RoleName like  '%" + EName + "%')";
                }
                else
                {
                    strCriteria = " RoleID in (Select ID from MsRole where RoleName like  '%" + EName + "%')";
                }
            }
            List<WhereClause> whereClause = new List<WhereClause>();
            MessageModel<RoleMenuModel> Result = service.FindDataWithPaging(strCriteria, whereClause, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditRoleMenu(int id)
        {

            RoleMenuModelView model = new RoleMenuModelView();
            if (id > 0)
            {
                var result = service.FindByID(id).data;
                model.MenuID = result.MenuID;
                model.RoleID = result.RoleID;

            }

            var Menu = menuService.FindAll();
            ViewBag.Menus = new SelectList(Menu.rows.ToList(), "ID", "DisplayMenu", model.MenuID);

            var Role = roleService.FindAll();
            ViewBag.Roles = new SelectList(Role.rows.ToList(), "ID", "RoleName", model.RoleID);

            return PartialView("~/Areas/Master/Views/Shared/PAddEditRoleMenu.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(RoleMenuModelView mdl)
        {
            RoleMenuModel data = new RoleMenuModel();
            AutoMapper.Mapper.Map(mdl, data);
            if (mdl.ID > 0)
            {
                //do update
                var result = service.Update(data);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = service.Add(data);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var usr = service.FindByID(id).data;
            var result = service.Delete(usr);
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}