﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Web.Models;
using App.Core.Interface;
using App.Model;
using App.Web.Areas.Master.Models;
using App.Web.Infrastructure;
using System.Configuration;
using System.Threading.Tasks;
using OfficeOpenXml;
using System.IO;
using OfficeOpenXml.Style;
using System.Drawing;
using NLog;
using App.Utility;
using System.Web.Http.Results;
using System.Security.Cryptography;


namespace App.Web.Areas.Master.Controllers
{
    public class UserController : BaseController
    {
        private string moduleName = "User List";
        IUserService userService;
        ILogService logService;
        IRoleService roleService;
        public UserController(IUserService _userService, ILogService logService, IRoleService roleService)
        {
            userService = _userService;
            this.logService = logService;
            this.roleService=roleService;
        }


        // GET: Master/User

        public ActionResult Index()
        {
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View "+moduleName });
            ViewBag.PageName="User List";
            return View();
        }

        #region Profile

        public UserModelView getProfile()
        {            
            //bind model            
            SessionModel susrs = (SessionModel)Session["User"];
            string uname = susrs.Username;
            UserModelView model = new UserModelView();
            var whereClause = new List<WhereClause>();
            whereClause.Add(new WhereClause() { Property = "Username", SqlOperator = "=", Value = uname });            
            if (!String.IsNullOrEmpty(uname))
            {
                string criteria = "Username=@Username";
                var result = userService.FindByCriteria(criteria, whereClause, "ID").rows.ToList();
                if (result != null)
                {
                    model.Username = result[0].Username;
                    model.FullName = result[0].FullName;
                    model.Department = result[0].Department;
                    model.Email = result[0].Email;
                    model.NPK = result[0].NPK;
                }

                //get Image Profile
                string path = Server.MapPath("~/Content/img/profile/");
                FileInfo file = new FileInfo(Path.Combine(path, uname + ".png"));
                if (file.Exists)
                {                    
                    byte[] imageArray = System.IO.File.ReadAllBytes(Path.Combine(path, uname + ".png"));
                    string base64ImageRepresentation = Convert.ToBase64String(imageArray);

                    model.FotoProfile = base64ImageRepresentation;

                }

            }
            return model;
        }
        public ActionResult Profiles()
        {
            ViewBag.PageName = "User Profile";            
            return View(getProfile());
        }

        [HttpPost]
        public ActionResult Profiles(HttpPostedFileBase postedFile)
        {
            ViewBag.PageName = "User Profile";
            SessionModel susrs = (SessionModel)Session["User"];
            string uname = susrs.Username;

            if (postedFile != null)
            {
                string path = Server.MapPath("~/Content/img/profile/");
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                string pathnya = path + uname + ".png";
                postedFile.SaveAs(pathnya);

                //save for thumbnail profile
                resizeImageAndSave(pathnya);

                ViewBag.Message = "File uploaded successfully.";
            }


            
            return View(getProfile());           
        }

        private string resizeImageAndSave(string imagePath)
        {

            System.Drawing.Image fullSizeImg
                 = System.Drawing.Image.FromFile(imagePath);
            var thumbnailImg = new Bitmap(160, 160);
            var thumbGraph = Graphics.FromImage(thumbnailImg);
            thumbGraph.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
            thumbGraph.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            thumbGraph.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
            var imageRectangle = new Rectangle(0, 0, 160, 160);
            thumbGraph.DrawImage(fullSizeImg, imageRectangle);
            string targetPath = imagePath.Replace(Path.GetFileNameWithoutExtension(imagePath), Path.GetFileNameWithoutExtension(imagePath) + "-thumb");
            thumbnailImg.Save(targetPath, System.Drawing.Imaging.ImageFormat.Jpeg); //(A generic error occurred in GDI+) Error occur here !
            thumbnailImg.Dispose();
            return targetPath;
        }

        #endregion

        #region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {
            
            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                strCriteria = " UserName like '%"+param.sSearch+ "%' OR FullName like '%" + param.sSearch + "%' OR UserRole like '%" + param.sSearch + "%' OR Status like '%" + param.sSearch + "%'";
            }
            if (!string.IsNullOrEmpty(EName))
            {
                if (strCriteria != "")
                {
                    strCriteria += " AND FullName like  '%" + EName + "%'";
                }
                else
                {
                    strCriteria = " FullName like  '%" + EName + "%'";
                }                
            }
            List<WhereClause> whereClause = new List<WhereClause>();
            MessageModel<UserModel> Result = userService.FindDataWithPaging(strCriteria, whereClause, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditUser(int id)
        {            
            UserModelView model = new UserModelView();
            if (id > 0)
            {
                var result = userService.FindByID(id).data;
                model.Username = result.Username;
                model.FullName = result.FullName;
                model.Department = result.Department;
                model.Email = result.Email;
                model.NPK = result.NPK;
                model.Status = result.Status;
                model.UserRole = result.UserRole;
            }

            List<ComboboxModel> StatusList = new List<ComboboxModel>();
            StatusList.Add(new ComboboxModel()
            {
                Key = "Active",
                Text = "Active"
            });
            StatusList.Add(new ComboboxModel()
            {
                Key = "Inactive",
                Text = "Inactive"
            });
            ViewBag.StatusList = new SelectList(StatusList, "Key", "Text", model.Status);

            var Role = roleService.FindAll();
            ViewBag.Roles = new SelectList(Role.rows.ToList(), "RoleName", "RoleName", model.UserRole);

            return PartialView("~/Areas/Master/Views/Shared/PAddEdituser.cshtml", model);            
        }

        public ActionResult EditUserPassword(int id)
        {
            UserModel model = new UserModel();
            if (id > 0)
            {
                model = userService.FindByID(id).data;               
            }


            return PartialView("~/Areas/Master/Views/Shared/PEdituserPassword.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(UserModelView mdl)
        {
            //cek if Username Exist or not
            List<WhereClause> whereClause = new List<WhereClause>();
            var result = userService.FindByCriteria(" Username='" + mdl.Username + "'", whereClause, "ID");
            if (result.rows.ToList().Count > 0 && mdl.ID==0)
            {
                result.message = "Username Already Exist";
                result.success = false;
                return Json(result, JsonRequestBehavior.AllowGet);                
            }


            UserModel usr = new UserModel();
            AutoMapper.Mapper.Map(mdl, usr);

            SessionModel susr = (SessionModel)Session["User"];
            string username = susr.Username;
            usr.SessionUser = username;
            string mode = "Tambah Data"; //default
            if (mdl.ID > 0)
            {
                //do update
                result = userService.Update(usr);
                mode = "Edit Data ID = " + mdl.ID;
            }
            else
            {
                //set default password for user
                usr.Password = ConfigurationManager.AppSettings["defaultPass"].ToString();
                result = userService.Add(usr);                
            }
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = mode + " di " + moduleName });

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            SessionModel susrs = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Delete Data ID " + id + " di " + moduleName });

            var usr = userService.FindByID(id).data;
            var result = userService.Delete(usr);
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        public ActionResult ChangePassword()
        {
            ViewBag.PageName = "Ubah Password Login";
            return View();
        }

        public ActionResult ChangePasswordUser()
        {
            SessionModel susr = (SessionModel)Session["User"];
            logService.Add(new LogModel { Username = susr.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View Ubah Password " + moduleName });
            ViewBag.PageName = "Ubah Password User";
            return View();
        }

        [HttpPost]
        public ActionResult ChangePasswordUser(UserModel mdl)
        {
            SessionModel susr = (SessionModel)Session["User"];            
            logService.Add(new LogModel { Username = susr.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "Ubah Password user " + mdl.FullName + " di " + moduleName });
            MessageModel<UserModel> result = new MessageModel<UserModel>();
            string username = susr.Username;
            var user = userService.FindByID(mdl.ID).data;
            if (user != null)
            {
                user.Password = mdl.Password;
                result = userService.ChangePassword(user);

                return Json(result, JsonRequestBehavior.AllowGet);
            }

            result.success = false;
            result.message = "not valid user or password";
            return Json(result, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ChangePassword(string oldPassword, string newPassword)
        {
            SessionModel susr = (SessionModel)Session["User"];
            string username = susr.Username;
            MessageModel<UserModel> result = new MessageModel<UserModel>();
            List<WhereClause> whereClause = new List<WhereClause>();
            bool isValid = userService.ValidUser(username, oldPassword);
            if (isValid)
            {
                var user = userService.FindByCriteria(" Username like '" + username + "'", whereClause, "ID").rows.ToList();
                if (user != null)
                {
                    UserModel usr = user[0];

                    usr.Password = newPassword;

                    result = userService.ChangePassword(usr);

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }

            //not valid
            result.success = false;
            result.message = "not valid user or password";            
            return Json(result, JsonRequestBehavior.AllowGet);


        }

        #endregion

        #region Export To Excel

        [HttpGet]
        public ActionResult ExportToExcel()
        {
            ReportModel report = new ReportModel();
            try
            {
                //define Path & Name File
                string sFileName = @"UserReport.xlsx";
                string uploadDir = Server.MapPath("~/Download");
                if (!Directory.Exists(uploadDir))
                {
                    Directory.CreateDirectory(uploadDir);
                }
                FileInfo file = new FileInfo(Path.Combine(uploadDir, sFileName));
                string filePath = Path.Combine(uploadDir, sFileName);
                if (file.Exists)
                {
                    file.Delete();
                    file = new FileInfo(Path.Combine(uploadDir, sFileName));
                }


                //Source data
                var result = userService.FindAll().rows.ToList();

                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage package = new ExcelPackage(file))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("User List");

                    //First add the headers
                    worksheet.Cells[1, 1].Value = "NPK";
                    worksheet.Cells[1, 2].Value = "UserName";
                    worksheet.Cells[1, 3].Value = "FullName";
                    worksheet.Cells[1, 4].Value = "Email";

                    //fill rows data              
                    int row = 2;
                    foreach (var data in result)
                    {
                        worksheet.Cells[row, 1].Value = data.NPK;
                        worksheet.Cells[row, 2].Value = data.Username;
                        worksheet.Cells[row, 3].Value = data.FullName;
                        worksheet.Cells[row, 4].Value = data.Email;
                        row++;
                    }

                    //design header style
                    using (var range = worksheet.Cells[1, 1, 1, 4])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                        range.Style.Fill.BackgroundColor.SetColor(Color.DarkBlue);
                        range.Style.Font.Color.SetColor(Color.White);
                    }

                    //set Width header
                    worksheet.Cells["A1:D1"].AutoFitColumns();
                    worksheet.Column(1).Width = 20;
                    worksheet.Column(2).Width = 30;
                    worksheet.Column(3).Width = 40;
                    worksheet.Column(4).Width = 40;
                    package.Save();
                }

                byte[] b = System.IO.File.ReadAllBytes(filePath);

                var base64 = "data:application/octet-stream;base64," + Convert.ToBase64String(b);

                
                report.message = "OK";
                report.data = base64;
                report.success = true;
                
            }
            catch (Exception e)
            {
                
                report.message = "Error Generate File."+e.Message;
                report.success = false;
            }


            return Json(report, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}