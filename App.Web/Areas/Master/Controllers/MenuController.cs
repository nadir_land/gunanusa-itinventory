﻿using App.Core.Interface;
using App.Model;
using App.Utility;
using App.Web.Areas.Master.Models;
using App.Web.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;

namespace App.Web.Areas.Master.Controllers
{
    public class MenuController : BaseController
    {

        IMenuService service;
        public MenuController(IMenuService _service)
        {
            service = _service;
        }

        // GET: Master/Menu
        public ActionResult Index()
        {
            ViewBag.PageName = "Menu Setting";
            return View();
        }

        #region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {

            int pageNo = 1;

            if (param.iDisplayStart >= param.iDisplayLength)
            {
                pageNo = (param.iDisplayStart / param.iDisplayLength) + 1;

            }

            string strCriteria = "";

            //Search by columne name..
            if (param.sSearch != null)
            {
                //define colom filter here..
                strCriteria = " DisplayMenu like '%" + param.sSearch + "%'";
            }
            if (!string.IsNullOrEmpty(EName))
            {
                if (strCriteria != "")
                {
                    strCriteria += " AND DisplayMenu like  '%" + EName + "%'";
                }
                else
                {
                    strCriteria = " DisplayMenu like  '%" + EName + "%'";
                }
            }

            List<WhereClause> whereClause = new List<WhereClause>();
            MessageModel<MenuModel> Result = service.FindDataWithPaging(strCriteria,whereClause, param.iDisplayLength, pageNo, "ID");

            return Json(new
            {
                aaData = Result.rows.ToList(),
                sEcho = param.sEcho,
                iTotalDisplayRecords = Result.total,
                iTotalRecords = Result.total

            }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Action CUD

        public ActionResult AddEditMenu(int id)
        {
            
            MenuModelView model = new MenuModelView();
            if (id > 0)
            {
                var result = service.FindByID(id).data;
                model.DisplayMenu = result.DisplayMenu;
                model.Link = result.Link;
                model.ClassIcon = result.ClassIcon;
                model.Parent = result.Parent;
                model.TypeMenu = result.TypeMenu;                
            }
            List<WhereClause> whereClause = new List<WhereClause>();
            ViewBag.ParentList = new SelectList(service.FindByCriteria(" TypeMenu='G'", whereClause, "ID").rows.ToList(), "ID", "DisplayMenu", model.ID);
            List<string> lstype = new List<string>() { "G", "C", "H", "L" };            
            ViewBag.Type = new SelectList(lstype, model.TypeMenu);

            return PartialView("~/Areas/Master/Views/Shared/PAddEditMenu.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(MenuModelView mdl)
        {
            MenuModel data = new MenuModel();
            AutoMapper.Mapper.Map(mdl, data);
            if (mdl.ID > 0)
            {
                //do update
                var result = service.Update(data);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = service.Add(data);
                return Json(result, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            var usr = service.FindByID(id).data;
            var result = service.Delete(usr);
            return Json(result, JsonRequestBehavior.AllowGet);
        }




        #endregion
    }
}