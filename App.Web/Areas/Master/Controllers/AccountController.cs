﻿using App.Core;
using App.Core.Interface;
using App.Utility;
using App.Web.Areas.Master.Models;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace App.Web.Areas.Master.Controllers
{
    public class AccountController : Controller
    {
        // GET: Master/Account
        public ActionResult Login()
        {
            Session.RemoveAll();
            ViewBag.Message = null;
            return View();
        }

        [HttpPost]
        public ActionResult Login(string Username, string Password)
        {
            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                ViewBag.Message = "Username/Password must be filled!";
                return View();
            }
            
            UserService service = new UserService();
            bool valid = false;

            //get user
            List<WhereClause> whereClause = new List<WhereClause>();
            whereClause.Add(new WhereClause { Property = "Username", SqlOperator = "=", Value = Username });
            whereClause.Add(new WhereClause { Property = "Status", SqlOperator = "=", Value = "Active" });
            string strcriteria = "Username=@Username and Status=@Status";
            
            
            var user = service.FindByCriteria(strcriteria, whereClause, "ID").rows.ToList();
            if (user != null)
            {
                valid = service.ValidUser(Username, Password);
            }
            
            if (valid)
            {                
                SessionModel usr = new SessionModel();
                usr.FullName = user[0].FullName;
                usr.Username = user[0].Username;
                usr.UserID = user[0].ID;
                usr.RoleName = user[0].UserRole;

                ILogService logService = new LogService();
                logService.Add(new Model.LogModel
                {
                    Username = user[0].FullName,
                    LogTime = DateTime.Now,
                    ModulName = "Login",
                    Activity = "Login"
                });

                List<RoleModelView> roleview = new List<RoleModelView>();
                UserRoleService urole = new UserRoleService();
                RoleService roleService = new RoleService();
                List<WhereClause> whereClause2 = new List<WhereClause>();
                whereClause2.Add(new WhereClause { Property = "RoleName", Value = user[0].UserRole });
                var roles = roleService.FindDataWithPaging(" RoleName=@RoleName", whereClause2, 1, 1, "ID").rows.ToList();
                if (roles.Count > 0)
                {
                    roleview.Add(new RoleModelView
                    {
                        ID = roles[0].ID,
                        RoleName = roles[0].RoleName,
                    });
                }

                //set to Model
                usr.Roles = roleview;
                //commit session
                Session["User"] = usr;
                                
                //welcome abroad..
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else
            {
                ViewBag.Message = "Invalid Username/Password";
                return View();
            }

            
        }

        
        public ActionResult RabbitHole()
        {

            if (Session["User"] == null)
            {
                return RedirectToAction("Login", "Account", new { area = "Master" });
            }

            bool isValid = false;
            SessionModel user = (SessionModel)Session["User"];
            foreach(var role in user.Roles)
            {
                if(role.RoleName.ToLower().Equals("admin") || role.RoleName.ToLower().Equals("developer"))
                {
                    isValid = true;
                    break;
                }
            }

            if(isValid)
            {
                return View("RabbitHole");                

            }

            return RedirectToAction("Login", "Account", new { area = "Master" });

        }

        [HttpPost]
        public ActionResult RabbitHole(string Username)
        {
            UserService service = new UserService();
            bool valid = false;
            List<WhereClause> whereClause = new List<WhereClause>();
            //get user
            var user = service.FindByCriteria(" Username='" + Username + "'", whereClause, "ID").rows.ToList();
            if (user != null)
            {
                valid = true;
            }

            if (valid)
            {
                SessionModel usr = new SessionModel();
                usr.FullName = user[0].FullName;
                usr.Username = user[0].Username;
                usr.UserID = user[0].ID;

                List<RoleModelView> roleview = new List<RoleModelView>();
                UserRoleService urole = new UserRoleService();
                List<WhereClause> whereClause2 = new List<WhereClause>();
                var roles = urole.FindByCriteria(" UserID='" + user[0].ID + "'", whereClause2, "ID").rows.ToList();
                //mapping
                AutoMapper.Mapper.Map(roles, roleview);

                //set to Model
                usr.Roles = roleview;
                //commit session
                Session["User"] = usr;

                //welcome abroad..
                return RedirectToAction("Index", "Home", new { area = "" });
            }
            else
            {
                ViewBag.Message = "Invalid Username/Password";
                return View();
            }
        }
    }
}