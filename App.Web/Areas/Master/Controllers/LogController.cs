using App.Core.Interface;
using App.Model;
using App.Web.Infrastructure;
using App.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Controllers;
using App.Utility;
using App.Core;

namespace App.Web.Areas.Master.Controllers
{
    public class LogController : BaseController
    {
        private string moduleName = "Log Aplikasi";
        ILogService service;

		/*
			GiftModel  --> from App.Models
		*/

        
		public LogController(ILogService _service)
        {
            service = _service;
        }

        // GET: Log
        public ActionResult Index()
        {
            SessionModel susrs = (SessionModel)Session["User"];
            service.Add(new LogModel { Username = susrs.FullName, LogTime = DateTime.Now, ModulName = moduleName, Activity = "View " + moduleName });
            ViewBag.PageName = "Log Aplikasi";
            ViewBag.DisplayCode = "0"; //Not Authorized
            if (susrs.Roles.Count > 0)
            {
                if (susrs.Roles[0].ID == 1) //Super Admin
                {
                    ViewBag.DisplayCode = "1"; //show All
                }
                else if (susrs.Roles[0].ID == 5) //Bea Cukai
                {
                    ViewBag.Mode = "2"; //Hide Activity
                }
            }            
            return View();
        }

		#region Display data

        public JsonResult GetDisplayData(DataTablesParam param, string EName)
        {

            MessageModel<LogModel> Result = new MessageModel<LogModel>();
            SessionModel susrs = (SessionModel)Session["User"];
            if (susrs.Roles.Count > 0)
            {
                if (susrs.Roles[0].ID == 1) //Super Admin
                {
                    ViewBag.DisplayCode = "1"; //show All
                    Result = service.FindAll();
                }
                else if (susrs.Roles[0].ID == 5) //Bea Cukai
                {
                    ViewBag.Mode = "2"; //Hide Activity
                    List<WhereClause> whereClauses = new List<WhereClause>();
                    Result = service.FindByCriteria("ModulName='Login'", whereClauses, "ID");
                }
            }

            return Json(new
            {
                data = Result.rows.ToList(),               
            }, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}
