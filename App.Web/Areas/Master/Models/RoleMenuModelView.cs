﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Areas.Master.Models
{
    public class RoleMenuModelView
    {
        public int ID { get; set; }
        public int? RoleID { get; set; }
        public int? MenuID { get; set; }

        //Custom
        public string RoleName { get; set; }

        public string DisplayMenu { get; set; }

    }
}