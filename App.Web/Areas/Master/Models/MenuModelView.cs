﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Areas.Master.Models
{
    public class MenuModelView
    {
        public int ID { get; set; }
        public string Link { get; set; }
        public string DisplayMenu { get; set; }
        public string ClassIcon { get; set; }
        public int? Parent { get; set; }
        public string TypeMenu { get; set; }
    }
}