﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.Web.Areas.Master.Models
{
    public class UserModelView
    {
        [Key]
        public int ID { get; set; }

        [Required(ErrorMessage = "Enter Username")]
        public string Username { get; set; }
        
        public string Password { get; set; }
        public string NPK { get; set; }
        [Required(ErrorMessage = "Enter Full Name")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Enter Department Name")]
        public string Department { get; set; }

        [Required(ErrorMessage = "Enter Email")]
        public string Email { get; set; }

        public string Status { get; set; }
        public string UserRole { get; set; }


        public string FotoProfile { get; set; }
    }
}