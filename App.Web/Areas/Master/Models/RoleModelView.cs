﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace App.Web.Areas.Master.Models
{
    public class RoleModelView
    {
        [Key]
        public int ID { get; set; }
        public string RoleName { get; set; }
    }
}