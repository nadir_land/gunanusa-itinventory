﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Areas.Master.Models
{
    public class UserRoleModelView
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int RoleID { get; set; }
        public string UserName { get; set; }
        public string RoleName { get; set; }
    }
}