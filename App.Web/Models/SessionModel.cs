﻿using App.Model;
using App.Web.Areas.Master.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Models
{
    public class SessionModel
    {
        public int UserID { get; set; }
        public string Username { get; set; }
        public string FullName { get; set; }
        public string RoleName { get; set; }
        public List<RoleModelView> Roles { get; set; }
    }
}