﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace App.Web.Models
{
    public class ReportPemakaianBahanBakuModel
    {
        public int ID { get; set; }
        public string NoRelease { get; set; }
        public string TglRelease { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public decimal Digunakan { get; set; }
        public decimal Disubkon { get; set; }
        public string PenerimaSubkon { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int FlagNoData { get; set; }
    }

    public class ReportPemasukanHasilProduksiModel
    {
        public int ID { get; set; }
        public string NoResult { get; set; }
        public string TglResult { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public decimal DariProduksi { get; set; }
        public decimal DariSubkon { get; set; }
        public string Gudang { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int FlagNoData { get; set; }
    }

    public class ReportPengeluaranHasilProduksiModel
    {
        public int ID { get; set; }
        public string NoPEB { get; set; }
        public string TglPEB { get; set; }
        public string NoDO { get; set; }
        public string TglDO { get; set; }
        public string Pembeli { get; set; }
        public string NegaraTujuan { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public decimal Jumlah { get; set; }
        public decimal NilaiBarang { get; set; }
        public string MataUang { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int FlagNoData { get; set; }
    }

    public class ReportMutasiModel
    {
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public decimal SaldoAwal { get; set; }
        public decimal Pemasukan { get; set; }
        public decimal Pengeluaran { get; set; }
        public decimal SaldoAkhir { get; set; }
        public string Gudang { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int FlagNoData { get; set; }
    }

    public class ReportWasteScrapModel
    {
        public string No { get; set; }
        public string Tgl { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public string Satuan { get; set; }
        public decimal Jumlah { get; set; }
        public decimal Nilai { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public int FlagNoData { get; set; }
    }

    public class ReportVendorCustomer
    {
        public string Kode { get; set; }
        public string Name { get; set; }
        public string ContactPerson { get; set; }
    }
}