﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Models
{
    public class ComboboxModel
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Text { get; set; }

    }
}