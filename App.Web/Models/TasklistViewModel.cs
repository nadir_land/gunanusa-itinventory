﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Models
{
    public class TasklistViewModel
    {
        public string Id { get; set; }
        public string CustomerName { get; set; }
        public string Address { get; set; }
        public string Date { get; set; }
        public string EquipmentNumber { get; set; }
        public string Model { get; set; }
        public string SN { get; set; }
        public string PICCustomer { get; set; }
        public string Phone { get; set; }
        public string Info { get; set; }
    }
}