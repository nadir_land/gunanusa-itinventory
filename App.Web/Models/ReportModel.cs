﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Models
{
    public class ReportModel
    {
        public string message { get; set; }
        public string data { get; set; }
        public bool success { get; set; }
    }
}