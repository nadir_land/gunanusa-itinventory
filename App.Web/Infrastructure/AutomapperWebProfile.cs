﻿using App.Model;
using App.Web.Areas.Master.Models;
using App.Web.Areas.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace App.Web.Infrastructure
{
    public class AutomapperWebProfile : AutoMapper.Profile
    {
        public static void run()
        {
            AutoMapper.Mapper.Initialize(a => {
                a.AddProfile<AutomapperWebProfile>();
            });
        }
        public AutomapperWebProfile()
        {
            CreateMap<UserModelView, UserModel>();
            CreateMap<RoleModelView, RoleModel>();
            CreateMap<MenuModelView, MenuModel>();
            CreateMap<UserRoleModelView, UserRoleModel>();
            CreateMap<RoleMenuModelView, RoleMenuModel>();
            CreateMap<DeliveryOrderModelView, DeliveryOrderModel>();
            CreateMap<DeliveryOrderDetailModelView, DeliveryOrderDetailModel>();
            //reverse
            CreateMap<UserModel, UserModelView>();
            CreateMap<RoleModel, RoleModelView>();
            CreateMap<MenuModel, MenuModelView>();
            CreateMap<UserRoleModel, UserRoleModelView>();
            CreateMap<RoleMenuModel, RoleMenuModelView>();
            CreateMap<DeliveryOrderModel, DeliveryOrderModelView>();
            CreateMap<DeliveryOrderDetailModel, DeliveryOrderDetailModelView>();
        }

        

        

    }
}