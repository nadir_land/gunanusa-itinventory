/*
=======================================
Progammer   : <Programmer NickName>
Date        : <Created / Updated Date>
LineNumber  : <Partial / Range line number to applied code
Purpose     : <The purpose of code>
=======================================
*/
#region .NET Base Class Namespace Imports
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Runtime.Serialization;
#endregion


namespace App.Model
{
	[Serializable]
    [DataContract]
    public class ModelBase
    {
        protected Dictionary<string, IEnumerable<string>> _errors = new Dictionary<string, IEnumerable<string>>();
       

        public Dictionary<string, IEnumerable<string>> GetErrors()
        {
            return _errors;
        }

        public string SessionUser { get; set; }
        public string SessionRole { get; set; }
    }
}