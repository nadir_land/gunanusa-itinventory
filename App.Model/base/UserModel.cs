using System;
using System.Runtime.Serialization;

namespace App.Model
{
    [Serializable]
    [DataContract]
    public partial class UserModel : ModelBase
    {
        [DataMember]
        public int ID { get; set; }
        [DataMember]
        public string Username { get; set; }
        [DataMember]
        public string Password { get; set; }
        [DataMember]
        public string NPK { get; set; }
        [DataMember]
        public string FullName { get; set; }
        [DataMember]
        public string Department { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public bool? IsLocked { get; set; }
        [DataMember]
        public bool? IsOnline { get; set; }
        [DataMember]
        public DateTime? CreatedOn { get; set; }
        [DataMember]
        public string CreatedBy { get; set; }
        [DataMember]
        public DateTime? UpdateOn { get; set; }
        [DataMember]
        public string UpdateBy { get; set; }
        [DataMember]
        public string Status { get; set; }
        public string UserRole { get; set; }
        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}
