using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace App.Model
{
    public partial class DeliveryOrderModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("No D O")]
		public string NoDO { get; set; } 
		[DisplayName("Tgl D O")]
		public string TglDO { get; set; } 
		[DisplayName("No P E B")]
		public string NoPEB { get; set; } 
		[DisplayName("Tgl P E B")]
		public DateTime TglPEB { get; set; } 
		[DisplayName("Valuta")]
		public string Valuta { get; set; } 
		[DisplayName("Kode Customer")]
		public string KodeCustomer { get; set; } 
		[DisplayName("Nama Customer")]
		public string NamaCustomer { get; set; } 
		[DisplayName("Alamat")]
		public string Alamat { get; set; } 
		[DisplayName("Keterangan")]
		public string Keterangan { get; set; }

        public string Status { get; set; }

		public List<DeliveryOrderDetailModel> Details { get; set; }
        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

