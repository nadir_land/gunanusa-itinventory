using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class VendorModel : ModelBase
    {
        
        public int ID { get; set; } 
		public string KodeVendor { get; set; } 
		public string NamaVendor { get; set; } 
		public string AlamatVendor { get; set; } 
		public string Negara { get; set; } 
		public string Telp { get; set; } 
		public string Fax { get; set; } 
		public string ContactPerson { get; set; } 
		public string Status { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

