using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class XmlRIModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("NOBUKMSK")]
		public string NOBUKMSK { get; set; } 
		[DisplayName("TGBUKMSK")]
		public string TGBUKMSK { get; set; } 
		[DisplayName("KOBAR")]
		public string KOBAR { get; set; } 
		[DisplayName("QTY")]
		public string QTY { get; set; } 
		[DisplayName("Tgl Download")]
		public DateTime DownloadDate { get; set; } 

		public RawMaterialModel RawMaterialModel { get;set; } 

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

