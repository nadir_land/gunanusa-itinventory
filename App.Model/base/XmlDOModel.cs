using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class XmlDOModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("N O F G K E L")]
		public string NOFGKEL { get; set; } 
		[DisplayName("T G F G K L")]
		public string TGFGKL { get; set; } 
		[DisplayName("K D P A R T")]
		public string KDPART { get; set; } 
		[DisplayName("Q T Y")]
		public string QTY { get; set; } 
		[DisplayName("Download Date")]
		public DateTime DownloadDate { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

