using System;
using System.ComponentModel;
using System.Security.Principal;

namespace App.Model
{
    public partial class WorkOrderModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("No W O")]
		public string NoWO { get; set; } 
		[DisplayName("Tgl W O")]
		public string TglWO { get; set; } 
		[DisplayName("Kode Customer")]
		public string KodeCustomer { get; set; } 
		[DisplayName("Nama Customer")]
		public string NamaCustomer { get; set; } 
		[DisplayName("Alamat")]
		public string Alamat { get; set; } 
		[DisplayName("Negara Tujuan")]
		public string NegaraTujuan { get; set; } 
		[DisplayName("Kode F G")]
		public string KodeFG { get; set; } 
		[DisplayName("Nama F G")]
		public string NamaFG { get; set; } 
		[DisplayName("H S Export")]
		public string HSExport { get; set; } 
		[DisplayName("Satuan Export")]
		public string SatuanExport { get; set; } 
		[DisplayName("Foto")]
		public byte[] Foto { get; set; } 
		[DisplayName("Qty Order")]
		public decimal QtyOrder { get; set; } 
		[DisplayName("Kode Barang")]
		public string KodeBarang { get; set; } 
		[DisplayName("Nama Barang")]
		public string NamaBarang { get; set; } 
		[DisplayName("H S Import")]
		public string HSImport { get; set; } 
		[DisplayName("Satuan")]
		public string Satuan { get; set; } 
		[DisplayName("Koefisien")]
		public decimal Koefisien { get; set; } 
		[DisplayName("Dibutuhkan")]
		public decimal Dibutuhkan { get; set; } 
		[DisplayName("Status")]
		public string Status { get; set; }

		//for display
        public  string imageBase64String { get; set; }
        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

