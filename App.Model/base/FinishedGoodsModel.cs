using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class FinishedGoodsModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("Kode FG")]
		public string KodeFG { get; set; } 
		[DisplayName("Nama FG")]
		public string NamaFG { get; set; } 
		[DisplayName("Gudang FG")]
		public string GudangFG { get; set; } 
		[DisplayName("HS Export")]
		public string HSExport { get; set; } 
		[DisplayName("Satuan")]
		public string Satuan { get; set; } 
		[DisplayName("Keterangan")]
		public string Keterangan { get; set; } 
		[DisplayName("Foto")]
		public byte[] Foto { get; set; }

        //for display only
        public string imageBase64String { get; set; }

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

