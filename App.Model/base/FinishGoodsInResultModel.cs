using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class FinishGoodsInResultModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("Opsi Produksi")]
		public string OpsiProduksi { get; set; } 
		[DisplayName("No W O")]
		public string NoWO { get; set; } 
		[DisplayName("Tgl W O")]
		public string TglWO { get; set; } 
		[DisplayName("No Result")]
		public string NoResult { get; set; } 
		[DisplayName("Tgl Result")]
		public string TglResult { get; set; } 
		[DisplayName("Kode Subkon")]
		public string KodeSubkon { get; set; } 
		[DisplayName("Nama Subkon")]
		public string NamaSubkon { get; set; } 
		[DisplayName("Alamat")]
		public string Alamat { get; set; } 
		[DisplayName("Keterangan")]
		public string Keterangan { get; set; } 
		[DisplayName("Kode F G")]
		public string KodeFG { get; set; } 
		[DisplayName("Nama F G")]
		public string NamaFG { get; set; } 
		[DisplayName("H S Export")]
		public string HSExport { get; set; } 
		[DisplayName("Satuan Export")]
		public string SatuanExport { get; set; } 
		[DisplayName("Qty Order")]
		public decimal QtyOrder { get; set; } 
		[DisplayName("Sudah Kirim")]
		public decimal SudahKirim { get; set; } 
		[DisplayName("Akan Kirim")]
		public decimal AkanKirim { get; set; } 
		[DisplayName("Dlm Proses")]
		public decimal DlmProses { get; set; }

        public string Status { get; set; }
        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

