using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class PenerimaanBarangModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("Receipt No")]
		public string ReceiptNo { get; set; } 
		[DisplayName("Receipt Date")]
		public string ReceiptDate { get; set; } 
		[DisplayName("Warehouse")]
		public string Warehouse { get; set; } 
		[DisplayName("C A R")]
		public string CAR { get; set; } 
		[DisplayName("No P I B")]
		public string NoPIB { get; set; } 
		[DisplayName("Tanggal P I B")]
		public DateTime TanggalPIB { get; set; } 
		[DisplayName("K P B C")]
		public string KPBC { get; set; } 
		[DisplayName("Asal Barang")]
		public string AsalBarang { get; set; } 
		[DisplayName("Valuta")]
		public string Valuta { get; set; } 
		[DisplayName("Rate")]
		public decimal Rate { get; set; } 
		[DisplayName("Status")]
		public string Status { get; set; } 
		[DisplayName("Kode Barang")]
		public string KodeBarang { get; set; } 
		[DisplayName("Nama Barang")]
		public string NamaBarang { get; set; } 
		[DisplayName("H S Import")]
		public string HSImport { get; set; } 
		[DisplayName("Satuan")]
		public string Satuan { get; set; } 
		[DisplayName("No Seri")]
		public string NoSeri { get; set; } 
		[DisplayName("Tarif B M")]
		public decimal TarifBM { get; set; } 
		[DisplayName("Cif P I B")]
		public decimal CifPIB { get; set; } 
		[DisplayName("Qty P I B")]
		public decimal QtyPIB { get; set; } 
		[DisplayName("Bm P I B")]
		public decimal BmPIB { get; set; } 
		[DisplayName("Ppn P I B")]
		public decimal PpnPIB { get; set; } 
		[DisplayName("Qty R I")]
		public decimal QtyRI { get; set; } 

		//for Report
		public DateTime start { get;set; }
        public DateTime end { get; set; }
        public int FlagNoData { get; set; }
        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

