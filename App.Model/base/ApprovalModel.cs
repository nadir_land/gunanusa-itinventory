
using System;
using System.Collections.Generic;

namespace App.Model
{
    public partial class ApprovalModel : ModelBase
    {
        public int ID { get; set; }
        public int TransactionID { get; set; }
        public string TypeTransaction { get; set; }
        public int MatrixID { get; set; }
        public string DestinationUser { get; set; }
        public DateTime? CreatedOn { get; set; }
        public int SequentialDetail { get; set; }
        public string TransactionNumber { get; set; }

        public int StatusID { get; set; }
        public string StatusDesc { get; set; }

        public string CreatedBy { get; set; }

        public DateTime SubmitToSAPDate { get; set; }

        public IEnumerable<ApprovalDetailModel> ApprovalDetail { get; set; }

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}
