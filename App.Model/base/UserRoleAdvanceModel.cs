﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model
{
    public class UserRoleAdvanceModel
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int RoleID { get; set; }
        
        public string UserName { get; set; }
        public string RoleName { get; set; }

        public string DisplayMenu { get; set; }
    }
}
