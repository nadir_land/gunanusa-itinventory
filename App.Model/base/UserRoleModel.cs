﻿using System;
using System.Runtime.Serialization;

namespace App.Model
{
    [Serializable]
    [DataContract]
    public partial class UserRoleModel : ModelBase
    {
        public int ID { get; set; }
        public int UserID { get; set; }
        public int RoleID { get; set; }

        public string UserName { get; set; }
        public string RoleName { get; set; }

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }

    }
}
