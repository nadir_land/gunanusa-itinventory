using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class LogModel : ModelBase
    {
        
        public int ID { get; set; } 
		public string Username { get; set; } 
		public DateTime LogTime { get; set; } 
		public string ModulName { get; set; } 
		public string Activity { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

