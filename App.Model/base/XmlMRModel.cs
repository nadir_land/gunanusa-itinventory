using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class XmlMRModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("N O O R D P")]
		public string NOORDP { get; set; } 
		[DisplayName("N O S L I P R M")]
		public string NOSLIPRM { get; set; } 
		[DisplayName("T G S L I P R M")]
		public string TGSLIPRM { get; set; } 
		[DisplayName("K D P A R T")]
		public string KDPART { get; set; } 
		[DisplayName("K O B A R")]
		public string KOBAR { get; set; } 
		[DisplayName("Q T Y")]
		public string QTY { get; set; } 
		[DisplayName("Download Date")]
		public DateTime DownloadDate { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

