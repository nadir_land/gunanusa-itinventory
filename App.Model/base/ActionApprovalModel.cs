﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Model
{
    public class ActionApprovalModel : ModelBase
    {
        public int ApprovalID { get; set; }
        public int SequentialDetail { get; set; }
        public string Type { get; set; }
        public string Status { get; set; }
        public string Notes { get; set; }

        public int UserID { get; set; }
        public string Username { get; set; }
    }
}
