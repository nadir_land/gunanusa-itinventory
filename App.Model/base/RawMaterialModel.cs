using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class RawMaterialModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("Kode Material")]
		public string KodeMaterial { get; set; } 
		[DisplayName("Kode RM")]
		public string KodeRM { get; set; } 
		[DisplayName("Nama Material")]
		public string NamaMaterial { get; set; } 
		[DisplayName("HS Import")]
		public string HSImport { get; set; } 
		[DisplayName("Qty Equivalent")]
		public decimal QtyEquivalent { get; set; } 
		[DisplayName("Kode Satuan")]
		public string KodeSatuan { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

