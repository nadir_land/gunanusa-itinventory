using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class CustomerModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("Kode Customer")]
		public string KodeCustomer { get; set; } 
		[DisplayName("Nama Customer")]
		public string NamaCustomer { get; set; } 
		[DisplayName("Alamat Customer")]
		public string AlamatCustomer { get; set; } 
		[DisplayName("Negara")]
		public string Negara { get; set; } 
		[DisplayName("Telp")]
		public string Telp { get; set; } 
		[DisplayName("Fax")]
		public string Fax { get; set; } 
		[DisplayName("Contact")]
		public string Contact { get; set; } 
		[DisplayName("Status")]
		public string Status { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

