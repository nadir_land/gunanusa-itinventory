namespace App.Model
{
    public partial class RoleMenuModel : ModelBase
    {
        public int ID { get; set; }
        public int? RoleID { get; set; }
        public int? MenuID { get; set; }

        //Custom
        public string RoleName {get; set;}

        public string DisplayMenu { get; set; }
        public string Link { get; set; }
        public string Icon { get; set; }
        public int? Parent { get; set; }
        public string TypeMenu { get; set; }

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}
