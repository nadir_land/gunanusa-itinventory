using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class MaterialReleaseModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("Opsi Produksi")]
		public string OpsiProduksi { get; set; } 
		[DisplayName("No W O")]
		public string NoWO { get; set; } 
		[DisplayName("Tgl W O")]
		public string TglWO { get; set; } 
		[DisplayName("No Release")]
		public string NoRelease { get; set; }
        [DisplayName("Tgl Release")]
        public string TglRelease { get; set; }
        [DisplayName("Kode Subkon")]
		public string KodeSubkon { get; set; } 
		[DisplayName("Nama Subkon")]
		public string NamaSubkon { get; set; } 
		[DisplayName("Alamat")]
		public string Alamat { get; set; } 
		[DisplayName("Keterangan")]
		public string Keterangan { get; set; } 
		[DisplayName("Kode F G")]
		public string KodeFG { get; set; } 
		[DisplayName("Nama F G")]
		public string NamaFG { get; set; } 
		[DisplayName("H S Export")]
		public string HSExport { get; set; } 
		[DisplayName("Satuan Export")]
		public string SatuanExport { get; set; } 
		[DisplayName("Qty Order")]
		public decimal QtyOrder { get; set; } 
		[DisplayName("Kode Barang")]
		public string KodeBarang { get; set; } 
		[DisplayName("Nama Barang")]
		public string NamaBarang { get; set; } 
		[DisplayName("Satuan")]
		public string Satuan { get; set; } 
		[DisplayName("H S Import")]
		public string HSImport { get; set; } 
		[DisplayName("Dibutuhkan")]
		public decimal Dibutuhkan { get; set; } 
		[DisplayName("Terpakai")]
		public decimal Terpakai { get; set; } 
		[DisplayName("Dikeluarkan")]
		public decimal Dikeluarkan { get; set; } 
		[DisplayName("Belum Kirim")]
		public decimal BelumKirim { get; set; } 
		[DisplayName("Status")]
		public string Status { get; set; }

		//for Report
        public DateTime start { get; set; }
        public DateTime end { get; set; }
        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

