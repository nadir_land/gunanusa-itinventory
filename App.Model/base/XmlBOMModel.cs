using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class XmlBOMModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("B O M N O")]
		public string BOMNO { get; set; } 
		[DisplayName("K D P A R T")]
		public string KDPART { get; set; } 
		[DisplayName("B O M Q T Y")]
		public string BOMQTY { get; set; } 
		[DisplayName("K O B A R")]
		public string KOBAR { get; set; } 
		[DisplayName("Q T Y")]
		public string QTY { get; set; } 
		[DisplayName("Download Date")]
		public DateTime DownloadDate { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

