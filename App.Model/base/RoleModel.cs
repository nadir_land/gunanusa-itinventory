using System.Collections.Generic;

namespace App.Model
{
    public partial class RoleModel : ModelBase
    {
       
        public int ID { get; set; }
        public string RoleName { get; set; }

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}
