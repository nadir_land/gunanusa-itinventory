using System.Collections.Generic;

namespace App.Model
{
    public partial class ApprovalMatrixModel : ModelBase
    {
        public int ID { get; set; }
        public string PRType { get; set; }
        public string PRCategory { get; set; }
        public string PRJType { get; set; }
        public string Nominal { get; set; }
        public double? Min { get; set; }
        public double? Max { get; set; }

        public IEnumerable<ApprovalMatrixDetailModel> MatrixDetails { get; set; }

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}
