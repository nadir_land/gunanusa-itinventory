using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class PenerimaanBarangItemModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("Penerimaan Barang I D")]
		public int PenerimaanBarangID { get; set; }
        [DisplayName("Kode Barang")]
        public string KodeBarang { get; set; }
        
		[DisplayName("Nama Barang")]
		public string NamaBarang { get; set; } 
		[DisplayName("HS Import")]
		public string HSImport { get; set; } 
		[DisplayName("Satuan")]
		public string Satuan { get; set; } 
		[DisplayName("No Seri")]
		public string NoSeri { get; set; } 
		[DisplayName("Tarif BM")]
		public string TarifBM { get; set; } 
		[DisplayName("Cif PIB")]
		public string CifPIB { get; set; } 
		[DisplayName("Qty PIB")]
		public string QtyPIB { get; set; } 
		[DisplayName("Bm PIB")]
		public string BmPIB { get; set; } 
		[DisplayName("Ppn PIB")]
		public string PpnPIB { get; set; } 
		[DisplayName("Qty RI")]
		public string QtyRI { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

