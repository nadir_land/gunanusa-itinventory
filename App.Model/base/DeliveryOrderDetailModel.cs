using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class DeliveryOrderDetailModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("D O I D")]
		public int DOID { get; set; } 
		[DisplayName("Kode F G")]
		public string KodeFG { get; set; } 
		[DisplayName("Nama F G")]
		public string NamaFG { get; set; } 
		[DisplayName("H S Export")]
		public string HSExport { get; set; } 
		[DisplayName("Satuan")]
		public string Satuan { get; set; } 
		[DisplayName("Qty Kirim")]
		public decimal QtyKirim { get; set; } 
		[DisplayName("Harga Satuan")]
		public decimal HargaSatuan { get; set; } 
		[DisplayName("Jumlah Harga")]
		public decimal JumlahHarga { get; set; }

        //for display only
        public string dNoDO { get; set; }        
        public string dTglDO { get; set; }
        public string SID { get; set; }
        public string KodeBarang { get; set; }
        public string NamaBarang { get; set; }
        public string SatuanBarang { get; set; }
        public decimal QuantityEquivalent { get; set; }
        public decimal Nilai { get; set; }
        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

