using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class XmlPMRModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("N O O R D P R")]
		public string NOORDPR { get; set; } 
		[DisplayName("N O F G M S K")]
		public string NOFGMSK { get; set; } 
		[DisplayName("T G F G M S K")]
		public string TGFGMSK { get; set; } 
		[DisplayName("K D P A R T")]
		public string KDPART { get; set; } 
		[DisplayName("Q T Y")]
		public string QTY { get; set; } 
		[DisplayName("Download Date")]
		public DateTime DownloadDate { get; set; } 


        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

