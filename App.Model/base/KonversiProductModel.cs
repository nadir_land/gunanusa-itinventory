using System;
using System.ComponentModel;

namespace App.Model
{
    public partial class KonversiProductModel : ModelBase
    {
        
        public int ID { get; set; } 
		[DisplayName("Kode F G")]
		public string KodeFG { get; set; } 
		[DisplayName("Nama F G")]
		public string NamaFG { get; set; } 
		[DisplayName("B O M")]
		public string BOM { get; set; } 
		[DisplayName("H S Export")]
		public string HSExport { get; set; } 
		[DisplayName("Satuan Export")]
		public string SatuanExport { get; set; } 
		[DisplayName("Keterangan")]
		public string Keterangan { get; set; } 
		[DisplayName("Foto")]
		public byte[] Foto { get; set; } 
		[DisplayName("Kode Barang")]
		public string KodeBarang { get; set; } 
		[DisplayName("Nama Barang")]
		public string NamaBarang { get; set; } 
		[DisplayName("H S Import")]
		public string HSImport { get; set; } 
		[DisplayName("Satuan")]
		public string Satuan { get; set; } 
		[DisplayName("Koefisien")]
		public decimal Koefisien { get; set; } 
		[DisplayName("Kandungan")]
		public decimal Kandungan { get; set; } 
		[DisplayName("Waste")]
		public decimal Waste { get; set; } 
		[DisplayName("Status")]
		public string Status { get; set; } 
		[DisplayName("Qty Bom")]
		public decimal QtyBom { get; set; } 
		[DisplayName("Qty Barang")]
		public decimal QtyBarang { get; set; }

        //for display only
        public string imageBase64String { get; set; }

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }
}

