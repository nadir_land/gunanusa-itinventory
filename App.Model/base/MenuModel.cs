using System.Collections.Generic;

namespace App.Model
{
    public partial class MenuModel : ModelBase
    {
       
        public int ID { get; set; }
        public string Link { get; set; }
        public string DisplayMenu { get; set; }
        public string ClassIcon { get; set; }
        public int? Parent { get; set; }
        public string TypeMenu { get; set; }

        public List<ItemMenu> childMenu { get; set; }

        public bool IsValid()
        {
            return _errors.Count > 0 ? false : true;
        }
    }

    public partial class ItemMenu
    {
        public string Link { get; set; }
        public string DisplayMenu { get; set; }
        public string ClassIcon { get; set; }                
    }
}
