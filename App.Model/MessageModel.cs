﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace App.Model
{
    [Serializable]
    [DataContract]
    public class MessageModel<T>
    {
        [DataMember]
        public string message { get; set; }
        [DataMember]
        public bool success { get; set; }
        [DataMember]
        public T data { get; set; }
        [DataMember]
        public IEnumerable<T> rows { get; set; }
        [DataMember]
        public int total { get; set; }
        [DataMember]
        public Dictionary<string, IEnumerable<string>> errors { get; set; }

        public MessageModel()
        {
            this.rows = new List<T>();
        }
    }
}
