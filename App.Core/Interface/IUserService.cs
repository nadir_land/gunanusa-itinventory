﻿using App.Model;
using App.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Interface
{
    public interface IUserService
    {
        MessageModel<UserModel> Add(UserModel model);
        MessageModel<UserModel> Update(UserModel model);
        MessageModel<UserModel> Delete(UserModel model);
        MessageModel<UserModel> FindAll();
        MessageModel<UserModel> FindByID(int id);
        MessageModel<UserModel> FindByCriteria(string criteria,List<WhereClause> whereClause, string orderBy);
        //For Grid and paging
        MessageModel<UserModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);

        MessageModel<UserModel> ChangePassword(UserModel model);

        bool ValidUser(string userName, string Password);


    }
}
