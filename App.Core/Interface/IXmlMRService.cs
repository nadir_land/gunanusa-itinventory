using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IXmlMRService
    {
        MessageModel<XmlMRModel> Add(XmlMRModel model);
        MessageModel<XmlMRModel> Update(XmlMRModel model);
        MessageModel<XmlMRModel> Delete(XmlMRModel model);
        //For Grid and paging
        MessageModel<XmlMRModel> FindAll();
        MessageModel<XmlMRModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<XmlMRModel> FindByID(int id);
        MessageModel<XmlMRModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

