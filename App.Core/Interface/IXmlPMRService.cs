using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IXmlPMRService
    {
        MessageModel<XmlPMRModel> Add(XmlPMRModel model);
        MessageModel<XmlPMRModel> Update(XmlPMRModel model);
        MessageModel<XmlPMRModel> Delete(XmlPMRModel model);
        //For Grid and paging
        MessageModel<XmlPMRModel> FindAll();
        MessageModel<XmlPMRModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<XmlPMRModel> FindByID(int id);
        MessageModel<XmlPMRModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

