﻿using App.Model;
using App.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Interface
{
    public interface IUserRoleService
    {
        MessageModel<UserRoleModel> Add(UserRoleModel model);
        MessageModel<UserRoleModel> Update(UserRoleModel model);
        MessageModel<UserRoleModel> Delete(UserRoleModel model);
        //For Grid and paging
        MessageModel<UserRoleModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<UserRoleModel> FindByID(int id);

        MessageModel<UserRoleModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string orderby);
    }
}
