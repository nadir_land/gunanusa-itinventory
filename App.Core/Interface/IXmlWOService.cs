using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IXmlWOService
    {
        MessageModel<XmlWOModel> Add(XmlWOModel model);
        MessageModel<XmlWOModel> Update(XmlWOModel model);
        MessageModel<XmlWOModel> Delete(XmlWOModel model);
        //For Grid and paging
        MessageModel<XmlWOModel> FindAll();
        MessageModel<XmlWOModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<XmlWOModel> FindByID(int id);
        MessageModel<XmlWOModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

