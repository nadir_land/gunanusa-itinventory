using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IDeliveryOrderService
    {
        MessageModel<DeliveryOrderModel> Add(DeliveryOrderModel model);
        MessageModel<DeliveryOrderModel> Update(DeliveryOrderModel model);
        MessageModel<DeliveryOrderModel> Delete(DeliveryOrderModel model);
        //For Grid and paging
        MessageModel<DeliveryOrderModel> FindAll();
        MessageModel<DeliveryOrderModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<DeliveryOrderModel> FindByID(int id);
        MessageModel<DeliveryOrderModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

