﻿using App.Model;
using App.Utility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Interface
{
    public interface IRoleMenuService
    {
        List<RoleMenuModel> GetMenu();

        List<MenuModel> GetMenuByUserId(string uname);

        List<MenuModel> GetMenuByRoleId(string roleid);
        MessageModel<RoleMenuModel> Add(RoleMenuModel model);
        MessageModel<RoleMenuModel> Update(RoleMenuModel model);
        MessageModel<RoleMenuModel> Delete(RoleMenuModel model);
        //For Grid and paging
        MessageModel<RoleMenuModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<RoleMenuModel> FindByID(int id);

    }
}
