using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IXmlBOMService
    {
        MessageModel<XmlBOMModel> Add(XmlBOMModel model);
        MessageModel<XmlBOMModel> Update(XmlBOMModel model);
        MessageModel<XmlBOMModel> Delete(XmlBOMModel model);
        //For Grid and paging
        MessageModel<XmlBOMModel> FindAll();
        MessageModel<XmlBOMModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<XmlBOMModel> FindByID(int id);
        MessageModel<XmlBOMModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

