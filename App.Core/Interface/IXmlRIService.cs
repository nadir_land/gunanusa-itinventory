using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IXmlRIService
    {
        MessageModel<XmlRIModel> Add(XmlRIModel model);
        MessageModel<XmlRIModel> Update(XmlRIModel model);
        MessageModel<XmlRIModel> Delete(XmlRIModel model);
        //For Grid and paging
        MessageModel<XmlRIModel> FindAll();
        MessageModel<XmlRIModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<XmlRIModel> FindByID(int id);
        MessageModel<XmlRIModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

