﻿using App.Model;
using App.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Interface
{
    public interface IRoleService
    {
        MessageModel<RoleModel> Add(RoleModel model);
        MessageModel<RoleModel> Update(RoleModel model);
        MessageModel<RoleModel> Delete(RoleModel model);
        //For Grid and paging
        MessageModel<RoleModel> FindAll();
        MessageModel<RoleModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<RoleModel> FindByID(int id);
    }
}
