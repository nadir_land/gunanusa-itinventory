using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IXmlDOService
    {
        MessageModel<XmlDOModel> Add(XmlDOModel model);
        MessageModel<XmlDOModel> Update(XmlDOModel model);
        MessageModel<XmlDOModel> Delete(XmlDOModel model);
        //For Grid and paging
        MessageModel<XmlDOModel> FindAll();
        MessageModel<XmlDOModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<XmlDOModel> FindByID(int id);
        MessageModel<XmlDOModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

