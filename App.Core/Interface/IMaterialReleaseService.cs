using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IMaterialReleaseService
    {
        MessageModel<MaterialReleaseModel> Add(MaterialReleaseModel model);
        MessageModel<MaterialReleaseModel> Update(MaterialReleaseModel model);
        MessageModel<MaterialReleaseModel> Delete(MaterialReleaseModel model);
        //For Grid and paging
        MessageModel<MaterialReleaseModel> FindAll();
        MessageModel<MaterialReleaseModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<MaterialReleaseModel> FindByID(int id);
        MessageModel<MaterialReleaseModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

