using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IFinishGoodsInResultService
    {
        MessageModel<FinishGoodsInResultModel> Add(FinishGoodsInResultModel model);
        MessageModel<FinishGoodsInResultModel> Update(FinishGoodsInResultModel model);
        MessageModel<FinishGoodsInResultModel> Delete(FinishGoodsInResultModel model);
        //For Grid and paging
        MessageModel<FinishGoodsInResultModel> FindAll();
        MessageModel<FinishGoodsInResultModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<FinishGoodsInResultModel> FindByID(int id);
        MessageModel<FinishGoodsInResultModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

