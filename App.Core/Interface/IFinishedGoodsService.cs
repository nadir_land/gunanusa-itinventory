using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IFinishedGoodsService
    {
        MessageModel<FinishedGoodsModel> Add(FinishedGoodsModel model);
        MessageModel<FinishedGoodsModel> Update(FinishedGoodsModel model);
        MessageModel<FinishedGoodsModel> Delete(FinishedGoodsModel model);
        //For Grid and paging
        MessageModel<FinishedGoodsModel> FindAll();
        MessageModel<FinishedGoodsModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<FinishedGoodsModel> FindByID(int id);
        MessageModel<FinishedGoodsModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

