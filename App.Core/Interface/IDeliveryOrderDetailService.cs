using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IDeliveryOrderDetailService
    {
        MessageModel<DeliveryOrderDetailModel> Add(DeliveryOrderDetailModel model);
        MessageModel<DeliveryOrderDetailModel> Update(DeliveryOrderDetailModel model);
        MessageModel<DeliveryOrderDetailModel> Delete(DeliveryOrderDetailModel model);
        //For Grid and paging
        MessageModel<DeliveryOrderDetailModel> FindAll();
        MessageModel<DeliveryOrderDetailModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<DeliveryOrderDetailModel> FindByID(int id);
        MessageModel<DeliveryOrderDetailModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
        MessageModel<DeliveryOrderDetailModel> Report(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

