using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface ICustomerService
    {
        MessageModel<CustomerModel> Add(CustomerModel model);
        MessageModel<CustomerModel> Update(CustomerModel model);
        MessageModel<CustomerModel> Delete(CustomerModel model);
        //For Grid and paging
        MessageModel<CustomerModel> FindAll();
        MessageModel<CustomerModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<CustomerModel> FindByID(int id);
        MessageModel<CustomerModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

