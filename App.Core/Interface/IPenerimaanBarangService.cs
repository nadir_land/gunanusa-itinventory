using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IPenerimaanBarangService
    {
        MessageModel<PenerimaanBarangModel> Add(PenerimaanBarangModel model);
        MessageModel<PenerimaanBarangModel> Update(PenerimaanBarangModel model);
        MessageModel<PenerimaanBarangModel> Delete(PenerimaanBarangModel model);
        //For Grid and paging
        MessageModel<PenerimaanBarangModel> FindAll();
        MessageModel<PenerimaanBarangModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<PenerimaanBarangModel> FindByID(int id);
        MessageModel<PenerimaanBarangModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

