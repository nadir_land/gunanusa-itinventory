using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IVendorService
    {
        MessageModel<VendorModel> Add(VendorModel model);
        MessageModel<VendorModel> Update(VendorModel model);
        MessageModel<VendorModel> Delete(VendorModel model);
        //For Grid and paging
        MessageModel<VendorModel> FindAll();
        MessageModel<VendorModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<VendorModel> FindByID(int id);
        MessageModel<VendorModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

