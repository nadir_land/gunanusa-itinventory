﻿using App.Model;
using App.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Interface
{
    public interface IMenuService
    {
        MessageModel<MenuModel> Add(MenuModel model);
        MessageModel<MenuModel> Update(MenuModel model);
        MessageModel<MenuModel> Delete(MenuModel model);
        //For Grid and paging
        MessageModel<MenuModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);

        MessageModel<MenuModel> FindByID(int id);
        MessageModel<MenuModel> FindAll();

        MessageModel<MenuModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}
