using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IWorkOrderService
    {
        MessageModel<WorkOrderModel> Add(WorkOrderModel model);
        MessageModel<WorkOrderModel> Update(WorkOrderModel model);
        MessageModel<WorkOrderModel> Delete(WorkOrderModel model);
        //For Grid and paging
        MessageModel<WorkOrderModel> FindAll();
        MessageModel<WorkOrderModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<WorkOrderModel> FindByID(int id);
        MessageModel<WorkOrderModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

