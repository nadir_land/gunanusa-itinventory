using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IKonversiProductService
    {
        MessageModel<KonversiProductModel> Add(KonversiProductModel model);
        MessageModel<KonversiProductModel> Update(KonversiProductModel model);
        MessageModel<KonversiProductModel> Delete(KonversiProductModel model);
        //For Grid and paging
        MessageModel<KonversiProductModel> FindAll();
        MessageModel<KonversiProductModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<KonversiProductModel> FindByID(int id);
        MessageModel<KonversiProductModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

