using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IPenerimaanBarangItemService
    {
        MessageModel<PenerimaanBarangItemModel> Add(PenerimaanBarangItemModel model);
        MessageModel<PenerimaanBarangItemModel> Update(PenerimaanBarangItemModel model);
        MessageModel<PenerimaanBarangItemModel> Delete(PenerimaanBarangItemModel model);
        //For Grid and paging
        MessageModel<PenerimaanBarangItemModel> FindAll();
        MessageModel<PenerimaanBarangItemModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<PenerimaanBarangItemModel> FindByID(int id);
        MessageModel<PenerimaanBarangItemModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

