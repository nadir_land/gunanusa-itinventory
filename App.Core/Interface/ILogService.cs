using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface ILogService
    {
        MessageModel<LogModel> Add(LogModel model);
        MessageModel<LogModel> Update(LogModel model);
        MessageModel<LogModel> Delete(LogModel model);
        //For Grid and paging
        MessageModel<LogModel> FindAll();
        MessageModel<LogModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<LogModel> FindByID(int id);
        MessageModel<LogModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

