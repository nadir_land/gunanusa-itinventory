using App.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Utility;

namespace App.Core.Interface
{
    public interface IRawMaterialService
    {
        MessageModel<RawMaterialModel> Add(RawMaterialModel model);
        MessageModel<RawMaterialModel> Update(RawMaterialModel model);
        MessageModel<RawMaterialModel> Delete(RawMaterialModel model);
        //For Grid and paging
        MessageModel<RawMaterialModel> FindAll();
        MessageModel<RawMaterialModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy);
        MessageModel<RawMaterialModel> FindByID(int id);
        MessageModel<RawMaterialModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby);
    }
}

