using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class FinishGoodsInResultService : IFinishGoodsInResultService
    {
        #region private	
        private List<FinishGoodsInResultModel> listModel;
        FinishGoodsInResultRepository _FinishGoodsInResultRepository;
        protected Logger logger;
        #endregion

        public FinishGoodsInResultService()
        {
            _FinishGoodsInResultRepository = new FinishGoodsInResultRepository();
            listModel = new List<FinishGoodsInResultModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<FinishGoodsInResultModel> Add(FinishGoodsInResultModel model)
        {
            var jsonObj = new MessageModel<FinishGoodsInResultModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrFinishGoodsInResult()
                    {
                        ID = model.ID,
						OpsiProduksi = model.OpsiProduksi,
						NoWO = model.NoWO,
						TglWO = model.TglWO,
						NoResult = model.NoResult,
						TglResult = model.TglResult,
						KodeSubkon = model.KodeSubkon,
						NamaSubkon = model.NamaSubkon,
						Alamat = model.Alamat,
						Keterangan = model.Keterangan,
						KodeFG = model.KodeFG,
						NamaFG = model.NamaFG,
						HSExport = model.HSExport,
						SatuanExport = model.SatuanExport,
						QtyOrder = model.QtyOrder,
						SudahKirim = model.SudahKirim,
						AkanKirim = model.AkanKirim,
						DlmProses = model.DlmProses,
                        Status = model.Status
                    };
                    int ID = (_FinishGoodsInResultRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<FinishGoodsInResultModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<FinishGoodsInResultModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<FinishGoodsInResultModel> Update(FinishGoodsInResultModel model)
        {
            var jsonObj = new MessageModel<FinishGoodsInResultModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _FinishGoodsInResultRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.OpsiProduksi = model.OpsiProduksi;
						data.NoWO = model.NoWO;
						data.TglWO = model.TglWO;
						data.NoResult = model.NoResult;
						data.TglResult = model.TglResult;
						data.KodeSubkon = model.KodeSubkon;
						data.NamaSubkon = model.NamaSubkon;
						data.Alamat = model.Alamat;
						data.Keterangan = model.Keterangan;
						data.KodeFG = model.KodeFG;
						data.NamaFG = model.NamaFG;
						data.HSExport = model.HSExport;
						data.SatuanExport = model.SatuanExport;
						data.QtyOrder = model.QtyOrder;
						data.SudahKirim = model.SudahKirim;
						data.AkanKirim = model.AkanKirim;
						data.DlmProses = model.DlmProses;
                        data.Status = model.Status;
                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_FinishGoodsInResultRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<FinishGoodsInResultModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<FinishGoodsInResultModel> Delete(FinishGoodsInResultModel model)
        {
            var jsonObj = new MessageModel<FinishGoodsInResultModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _FinishGoodsInResultRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_FinishGoodsInResultRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<FinishGoodsInResultModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<FinishGoodsInResultModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<FinishGoodsInResultModel>();
            var message = new MessageModel<FinishGoodsInResultModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _FinishGoodsInResultRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new FinishGoodsInResultModel()
                    {
                        ID = data.ID,
						OpsiProduksi = data.OpsiProduksi,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						NoResult = data.NoResult,
						TglResult = data.TglResult,
						KodeSubkon = data.KodeSubkon,
						NamaSubkon = data.NamaSubkon,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						QtyOrder = data.QtyOrder,
						SudahKirim = data.SudahKirim,
						AkanKirim = data.AkanKirim,
						DlmProses = data.DlmProses,
                        Status = data.Status,
                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<FinishGoodsInResultModel> FindByID(int id)
        {
            var message = new MessageModel<FinishGoodsInResultModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new FinishGoodsInResultModel();

                var data = _FinishGoodsInResultRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.OpsiProduksi = data.OpsiProduksi;
						result.NoWO = data.NoWO;
						result.TglWO = data.TglWO;
						result.NoResult = data.NoResult;
						result.TglResult = data.TglResult;
						result.KodeSubkon = data.KodeSubkon;
						result.NamaSubkon = data.NamaSubkon;
						result.Alamat = data.Alamat;
						result.Keterangan = data.Keterangan;
						result.KodeFG = data.KodeFG;
						result.NamaFG = data.NamaFG;
						result.HSExport = data.HSExport;
						result.SatuanExport = data.SatuanExport;
						result.QtyOrder = data.QtyOrder;
						result.SudahKirim = data.SudahKirim;
						result.AkanKirim = data.AkanKirim;
						result.DlmProses = data.DlmProses;
                    result.Status = data.Status;
                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _FinishGoodsInResultRepository.Count(criteria, whereClause);
        }

        public MessageModel<FinishGoodsInResultModel> FindAll()
        {
            listModel = new List<FinishGoodsInResultModel>();
            var message = new MessageModel<FinishGoodsInResultModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _FinishGoodsInResultRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new FinishGoodsInResultModel()
                    {
                        ID = data.ID,
						OpsiProduksi = data.OpsiProduksi,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						NoResult = data.NoResult,
						TglResult = data.TglResult,
						KodeSubkon = data.KodeSubkon,
						NamaSubkon = data.NamaSubkon,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						QtyOrder = data.QtyOrder,
						SudahKirim = data.SudahKirim,
						AkanKirim = data.AkanKirim,
						DlmProses = data.DlmProses,
                        Status = data.Status,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<FinishGoodsInResultModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<FinishGoodsInResultModel>();
            var message = new MessageModel<FinishGoodsInResultModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _FinishGoodsInResultRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new FinishGoodsInResultModel()
                    {
                        ID = data.ID,
						OpsiProduksi = data.OpsiProduksi,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						NoResult = data.NoResult,
						TglResult = data.TglResult,
						KodeSubkon = data.KodeSubkon,
						NamaSubkon = data.NamaSubkon,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						QtyOrder = data.QtyOrder,
						SudahKirim = data.SudahKirim,
						AkanKirim = data.AkanKirim,
						DlmProses = data.DlmProses,
                        Status = data.Status,
                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


