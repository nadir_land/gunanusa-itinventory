﻿using App.Core.Interface;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using NLog;

namespace App.Core
{
    public class ConsoleService : IConsoleService
    {
        protected Logger logger;

        public ConsoleService() {
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public void RunConsoleApp()
        {
            try
            {
                string consoleAppPath = ConfigurationManager.AppSettings["ConsoleApp"].ToString();
                //string consoleAppArgs = "arg1 arg2";

                Process process = new Process();
                process.StartInfo.FileName = consoleAppPath;
                //process.StartInfo.Arguments = consoleAppArgs;
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.RedirectStandardOutput = true;
                process.Start();
                string output = process.StandardOutput.ReadToEnd();
                process.WaitForExit();
            }
            catch (Exception e)
            {
                logger.Error(e, "Got exception.");
            }            
        }
    }
}
