using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class RawMaterialService : IRawMaterialService
    {
        #region private	
        private List<RawMaterialModel> listModel;
        RawMaterialRepository _RawMaterialRepository;
        protected Logger logger;
        #endregion

        public RawMaterialService()
        {
            _RawMaterialRepository = new RawMaterialRepository();
            listModel = new List<RawMaterialModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<RawMaterialModel> Add(RawMaterialModel model)
        {
            var jsonObj = new MessageModel<RawMaterialModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsRawMaterial()
                    {
                        ID = model.ID,
						KodeMaterial = model.KodeMaterial,
						KodeRM = model.KodeRM,
						NamaMaterial = model.NamaMaterial,
						HSImport = model.HSImport,
						QtyEquivalent = model.QtyEquivalent,
						KodeSatuan = model.KodeSatuan,

                    };
                    int ID = (_RawMaterialRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<RawMaterialModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<RawMaterialModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<RawMaterialModel> Update(RawMaterialModel model)
        {
            var jsonObj = new MessageModel<RawMaterialModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _RawMaterialRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.KodeMaterial = model.KodeMaterial;
						data.KodeRM = model.KodeRM;
						data.NamaMaterial = model.NamaMaterial;
						data.HSImport = model.HSImport;
						data.QtyEquivalent = model.QtyEquivalent;
						data.KodeSatuan = model.KodeSatuan;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_RawMaterialRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<RawMaterialModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<RawMaterialModel> Delete(RawMaterialModel model)
        {
            var jsonObj = new MessageModel<RawMaterialModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _RawMaterialRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_RawMaterialRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<RawMaterialModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<RawMaterialModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<RawMaterialModel>();
            var message = new MessageModel<RawMaterialModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _RawMaterialRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new RawMaterialModel()
                    {
                        ID = data.ID,
						KodeMaterial = data.KodeMaterial,
						KodeRM = data.KodeRM,
						NamaMaterial = data.NamaMaterial,
						HSImport = data.HSImport,
						QtyEquivalent = data.QtyEquivalent,
						KodeSatuan = data.KodeSatuan,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<RawMaterialModel> FindByID(int id)
        {
            var message = new MessageModel<RawMaterialModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new RawMaterialModel();

                var data = _RawMaterialRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.KodeMaterial = data.KodeMaterial;
						result.KodeRM = data.KodeRM;
						result.NamaMaterial = data.NamaMaterial;
						result.HSImport = data.HSImport;
						result.QtyEquivalent = data.QtyEquivalent;
						result.KodeSatuan = data.KodeSatuan;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _RawMaterialRepository.Count(criteria, whereClause);
        }

        public MessageModel<RawMaterialModel> FindAll()
        {
            listModel = new List<RawMaterialModel>();
            var message = new MessageModel<RawMaterialModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _RawMaterialRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new RawMaterialModel()
                    {
                        ID = data.ID,
						KodeMaterial = data.KodeMaterial,
						KodeRM = data.KodeRM,
						NamaMaterial = data.NamaMaterial,
						HSImport = data.HSImport,
						QtyEquivalent = data.QtyEquivalent,
						KodeSatuan = data.KodeSatuan,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<RawMaterialModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<RawMaterialModel>();
            var message = new MessageModel<RawMaterialModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _RawMaterialRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new RawMaterialModel()
                    {
                        ID = data.ID,
						KodeMaterial = data.KodeMaterial,
						KodeRM = data.KodeRM,
						NamaMaterial = data.NamaMaterial,
						HSImport = data.HSImport,
						QtyEquivalent = data.QtyEquivalent,
						KodeSatuan = data.KodeSatuan,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


