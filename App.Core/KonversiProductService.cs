using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class KonversiProductService : IKonversiProductService
    {
        #region private	
        private List<KonversiProductModel> listModel;
        KonversiProductRepository _KonversiProductRepository;
        protected Logger logger;
        #endregion

        public KonversiProductService()
        {
            _KonversiProductRepository = new KonversiProductRepository();
            listModel = new List<KonversiProductModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<KonversiProductModel> Add(KonversiProductModel model)
        {
            var jsonObj = new MessageModel<KonversiProductModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrKonversiProduct()
                    {
                        ID = model.ID,
						KodeFG = model.KodeFG,
						NamaFG = model.NamaFG,
						BOM = model.BOM,
						HSExport = model.HSExport,
						SatuanExport = model.SatuanExport,
						Keterangan = model.Keterangan,
						Foto = model.Foto,
						KodeBarang = model.KodeBarang,
						NamaBarang = model.NamaBarang,
						HSImport = model.HSImport,
						Satuan = model.Satuan,
						Koefisien = model.Koefisien,
						Kandungan = model.Kandungan,
						Waste = model.Waste,
						Status = model.Status,
						QtyBom = model.QtyBom,
						QtyBarang = model.QtyBarang,

                    };
                    int ID = (_KonversiProductRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<KonversiProductModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<KonversiProductModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<KonversiProductModel> Update(KonversiProductModel model)
        {
            var jsonObj = new MessageModel<KonversiProductModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _KonversiProductRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.KodeFG = model.KodeFG;
						data.NamaFG = model.NamaFG;
						data.BOM = model.BOM;
						data.HSExport = model.HSExport;
						data.SatuanExport = model.SatuanExport;
						data.Keterangan = model.Keterangan;
						data.Foto = model.Foto;
						data.KodeBarang = model.KodeBarang;
						data.NamaBarang = model.NamaBarang;
						data.HSImport = model.HSImport;
						data.Satuan = model.Satuan;
						data.Koefisien = model.Koefisien;
						data.Kandungan = model.Kandungan;
						data.Waste = model.Waste;
						data.Status = model.Status;
						data.QtyBom = model.QtyBom;
						data.QtyBarang = model.QtyBarang;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_KonversiProductRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<KonversiProductModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<KonversiProductModel> Delete(KonversiProductModel model)
        {
            var jsonObj = new MessageModel<KonversiProductModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _KonversiProductRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_KonversiProductRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<KonversiProductModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<KonversiProductModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<KonversiProductModel>();
            var message = new MessageModel<KonversiProductModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _KonversiProductRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new KonversiProductModel()
                    {
                        ID = data.ID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						BOM = data.BOM,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						Keterangan = data.Keterangan,
						Foto = data.Foto,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						Koefisien = data.Koefisien,
						Kandungan = data.Kandungan,
						Waste = data.Waste,
						Status = data.Status,
						QtyBom = data.QtyBom,
						QtyBarang = data.QtyBarang,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<KonversiProductModel> FindByID(int id)
        {
            var message = new MessageModel<KonversiProductModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new KonversiProductModel();

                var data = _KonversiProductRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.KodeFG = data.KodeFG;
						result.NamaFG = data.NamaFG;
						result.BOM = data.BOM;
						result.HSExport = data.HSExport;
						result.SatuanExport = data.SatuanExport;
						result.Keterangan = data.Keterangan;
						result.Foto = data.Foto;
						result.KodeBarang = data.KodeBarang;
						result.NamaBarang = data.NamaBarang;
						result.HSImport = data.HSImport;
						result.Satuan = data.Satuan;
						result.Koefisien = data.Koefisien;
						result.Kandungan = data.Kandungan;
						result.Waste = data.Waste;
						result.Status = data.Status;
						result.QtyBom = data.QtyBom;
						result.QtyBarang = data.QtyBarang;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _KonversiProductRepository.Count(criteria, whereClause);
        }

        public MessageModel<KonversiProductModel> FindAll()
        {
            listModel = new List<KonversiProductModel>();
            var message = new MessageModel<KonversiProductModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _KonversiProductRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new KonversiProductModel()
                    {
                        ID = data.ID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						BOM = data.BOM,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						Keterangan = data.Keterangan,
						Foto = data.Foto,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						Koefisien = data.Koefisien,
						Kandungan = data.Kandungan,
						Waste = data.Waste,
						Status = data.Status,
						QtyBom = data.QtyBom,
						QtyBarang = data.QtyBarang,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<KonversiProductModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<KonversiProductModel>();
            var message = new MessageModel<KonversiProductModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _KonversiProductRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new KonversiProductModel()
                    {
                        ID = data.ID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						BOM = data.BOM,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						Keterangan = data.Keterangan,
						Foto = data.Foto,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						Koefisien = data.Koefisien,
						Kandungan = data.Kandungan,
						Waste = data.Waste,
						Status = data.Status,
						QtyBom = data.QtyBom,
						QtyBarang = data.QtyBarang,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


