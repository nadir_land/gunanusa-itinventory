using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class DeliveryOrderService : IDeliveryOrderService
    {
        #region private	
        private List<DeliveryOrderModel> listModel;
        DeliveryOrderRepository _DeliveryOrderRepository;
        protected Logger logger;
        #endregion

        public DeliveryOrderService()
        {
            _DeliveryOrderRepository = new DeliveryOrderRepository();
            listModel = new List<DeliveryOrderModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<DeliveryOrderModel> Add(DeliveryOrderModel model)
        {
            var jsonObj = new MessageModel<DeliveryOrderModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrDeliveryOrder()
                    {
                        ID = model.ID,
						NoDO = model.NoDO,
						TglDO = model.TglDO,
						NoPEB = model.NoPEB,
						TglPEB = model.TglPEB,
						Valuta = model.Valuta,
						KodeCustomer = model.KodeCustomer,
						NamaCustomer = model.NamaCustomer,
						Alamat = model.Alamat,
						Keterangan = model.Keterangan,
                        Status = model.Status
                    };
                    int ID = (_DeliveryOrderRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<DeliveryOrderModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<DeliveryOrderModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<DeliveryOrderModel> Update(DeliveryOrderModel model)
        {
            var jsonObj = new MessageModel<DeliveryOrderModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _DeliveryOrderRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.NoDO = model.NoDO;
						data.TglDO = model.TglDO;
						data.NoPEB = model.NoPEB;
						data.TglPEB = model.TglPEB;
						data.Valuta = model.Valuta;
						data.KodeCustomer = model.KodeCustomer;
						data.NamaCustomer = model.NamaCustomer;
						data.Alamat = model.Alamat;
						data.Keterangan = model.Keterangan;
                        data.Status = model.Status;
                        //data.ProjectID = model.ProjectID;

                    }
                    if (_DeliveryOrderRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<DeliveryOrderModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<DeliveryOrderModel> Delete(DeliveryOrderModel model)
        {
            var jsonObj = new MessageModel<DeliveryOrderModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _DeliveryOrderRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_DeliveryOrderRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<DeliveryOrderModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<DeliveryOrderModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<DeliveryOrderModel>();
            var message = new MessageModel<DeliveryOrderModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _DeliveryOrderRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new DeliveryOrderModel()
                    {
                        ID = data.ID,
						NoDO = data.NoDO,
						TglDO = data.TglDO,
						NoPEB = data.NoPEB,
						TglPEB = data.TglPEB,
						Valuta = data.Valuta,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
                        Status = data.Status
                        //sample
                        //ID = data.ID,

                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<DeliveryOrderModel> FindByID(int id)
        {
            var message = new MessageModel<DeliveryOrderModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new DeliveryOrderModel();

                var data = _DeliveryOrderRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.NoDO = data.NoDO;
						result.TglDO = data.TglDO;
						result.NoPEB = data.NoPEB;
						result.TglPEB = data.TglPEB;
						result.Valuta = data.Valuta;
						result.KodeCustomer = data.KodeCustomer;
						result.NamaCustomer = data.NamaCustomer;
						result.Alamat = data.Alamat;
						result.Keterangan = data.Keterangan;
                    result.Status = data.Status;
                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _DeliveryOrderRepository.Count(criteria, whereClause);
        }

        public MessageModel<DeliveryOrderModel> FindAll()
        {
            listModel = new List<DeliveryOrderModel>();
            var message = new MessageModel<DeliveryOrderModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _DeliveryOrderRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new DeliveryOrderModel()
                    {
                        ID = data.ID,
						NoDO = data.NoDO,
						TglDO = data.TglDO,
						NoPEB = data.NoPEB,
						TglPEB = data.TglPEB,
						Valuta = data.Valuta,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
                        Status = data.Status

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<DeliveryOrderModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<DeliveryOrderModel>();
            var message = new MessageModel<DeliveryOrderModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _DeliveryOrderRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new DeliveryOrderModel()
                    {
                        ID = data.ID,
						NoDO = data.NoDO,
						TglDO = data.TglDO,
						NoPEB = data.NoPEB,
						TglPEB = data.TglPEB,
						Valuta = data.Valuta,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
                        Status = data.Status
                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


