using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class WorkOrderService : IWorkOrderService
    {
        #region private	
        private List<WorkOrderModel> listModel;
        WorkOrderRepository _WorkOrderRepository;
        protected Logger logger;
        #endregion

        public WorkOrderService()
        {
            _WorkOrderRepository = new WorkOrderRepository();
            listModel = new List<WorkOrderModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<WorkOrderModel> Add(WorkOrderModel model)
        {
            var jsonObj = new MessageModel<WorkOrderModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrWorkOrder()
                    {
                        ID = model.ID,
						NoWO = model.NoWO,
						TglWO = model.TglWO,
						KodeCustomer = model.KodeCustomer,
						NamaCustomer = model.NamaCustomer,
						Alamat = model.Alamat,
						NegaraTujuan = model.NegaraTujuan,
						KodeFG = model.KodeFG,
						NamaFG = model.NamaFG,
						HSExport = model.HSExport,
						SatuanExport = model.SatuanExport,
						Foto = model.Foto,
						QtyOrder = model.QtyOrder,
						KodeBarang = model.KodeBarang,
						NamaBarang = model.NamaBarang,
						HSImport = model.HSImport,
						Satuan = model.Satuan,
						Koefisien = model.Koefisien,
						Dibutuhkan = model.Dibutuhkan,
						Status = model.Status,

                    };
                    int ID = (_WorkOrderRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<WorkOrderModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<WorkOrderModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<WorkOrderModel> Update(WorkOrderModel model)
        {
            var jsonObj = new MessageModel<WorkOrderModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _WorkOrderRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.NoWO = model.NoWO;
						data.TglWO = model.TglWO;
						data.KodeCustomer = model.KodeCustomer;
						data.NamaCustomer = model.NamaCustomer;
						data.Alamat = model.Alamat;
						data.NegaraTujuan = model.NegaraTujuan;
						data.KodeFG = model.KodeFG;
						data.NamaFG = model.NamaFG;
						data.HSExport = model.HSExport;
						data.SatuanExport = model.SatuanExport;
						data.Foto = model.Foto;
						data.QtyOrder = model.QtyOrder;
						data.KodeBarang = model.KodeBarang;
						data.NamaBarang = model.NamaBarang;
						data.HSImport = model.HSImport;
						data.Satuan = model.Satuan;
						data.Koefisien = model.Koefisien;
						data.Dibutuhkan = model.Dibutuhkan;
						data.Status = model.Status;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_WorkOrderRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<WorkOrderModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<WorkOrderModel> Delete(WorkOrderModel model)
        {
            var jsonObj = new MessageModel<WorkOrderModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _WorkOrderRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_WorkOrderRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<WorkOrderModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<WorkOrderModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<WorkOrderModel>();
            var message = new MessageModel<WorkOrderModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _WorkOrderRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new WorkOrderModel()
                    {
                        ID = data.ID,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						Alamat = data.Alamat,
						NegaraTujuan = data.NegaraTujuan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						Foto = data.Foto,
						QtyOrder = data.QtyOrder,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						Koefisien = data.Koefisien,
						Dibutuhkan = data.Dibutuhkan,
						Status = data.Status,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<WorkOrderModel> FindByID(int id)
        {
            var message = new MessageModel<WorkOrderModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new WorkOrderModel();

                var data = _WorkOrderRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.NoWO = data.NoWO;
						result.TglWO = data.TglWO;
						result.KodeCustomer = data.KodeCustomer;
						result.NamaCustomer = data.NamaCustomer;
						result.Alamat = data.Alamat;
						result.NegaraTujuan = data.NegaraTujuan;
						result.KodeFG = data.KodeFG;
						result.NamaFG = data.NamaFG;
						result.HSExport = data.HSExport;
						result.SatuanExport = data.SatuanExport;
						result.Foto = data.Foto;
						result.QtyOrder = data.QtyOrder;
						result.KodeBarang = data.KodeBarang;
						result.NamaBarang = data.NamaBarang;
						result.HSImport = data.HSImport;
						result.Satuan = data.Satuan;
						result.Koefisien = data.Koefisien;
						result.Dibutuhkan = data.Dibutuhkan;
						result.Status = data.Status;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _WorkOrderRepository.Count(criteria, whereClause);
        }

        public MessageModel<WorkOrderModel> FindAll()
        {
            listModel = new List<WorkOrderModel>();
            var message = new MessageModel<WorkOrderModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _WorkOrderRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new WorkOrderModel()
                    {
                        ID = data.ID,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						Alamat = data.Alamat,
						NegaraTujuan = data.NegaraTujuan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						Foto = data.Foto,
						QtyOrder = data.QtyOrder,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						Koefisien = data.Koefisien,
						Dibutuhkan = data.Dibutuhkan,
						Status = data.Status,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<WorkOrderModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<WorkOrderModel>();
            var message = new MessageModel<WorkOrderModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _WorkOrderRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new WorkOrderModel()
                    {
                        ID = data.ID,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						Alamat = data.Alamat,
						NegaraTujuan = data.NegaraTujuan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						Foto = data.Foto,
						QtyOrder = data.QtyOrder,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						Koefisien = data.Koefisien,
						Dibutuhkan = data.Dibutuhkan,
						Status = data.Status,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


