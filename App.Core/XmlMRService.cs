using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class XmlMRService : IXmlMRService
    {
        #region private	
        private List<XmlMRModel> listModel;
        XmlMRRepository _XmlMRRepository;
        protected Logger logger;
        #endregion

        public XmlMRService()
        {
            _XmlMRRepository = new XmlMRRepository();
            listModel = new List<XmlMRModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<XmlMRModel> Add(XmlMRModel model)
        {
            var jsonObj = new MessageModel<XmlMRModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new XmlMR()
                    {
                        ID = model.ID,
						NOORDP = model.NOORDP,
						NOSLIPRM = model.NOSLIPRM,
						TGSLIPRM = model.TGSLIPRM,
						KDPART = model.KDPART,
						KOBAR = model.KOBAR,
						QTY = model.QTY,
						DownloadDate = model.DownloadDate,

                    };
                    int ID = (_XmlMRRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<XmlMRModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<XmlMRModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlMRModel> Update(XmlMRModel model)
        {
            var jsonObj = new MessageModel<XmlMRModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _XmlMRRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.NOORDP = model.NOORDP;
						data.NOSLIPRM = model.NOSLIPRM;
						data.TGSLIPRM = model.TGSLIPRM;
						data.KDPART = model.KDPART;
						data.KOBAR = model.KOBAR;
						data.QTY = model.QTY;
						data.DownloadDate = model.DownloadDate;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_XmlMRRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<XmlMRModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlMRModel> Delete(XmlMRModel model)
        {
            var jsonObj = new MessageModel<XmlMRModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _XmlMRRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_XmlMRRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<XmlMRModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlMRModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<XmlMRModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _XmlMRRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new XmlMRModel()
                    {
                        ID = data.ID,
						NOORDP = data.NOORDP,
						NOSLIPRM = data.NOSLIPRM,
						TGSLIPRM = data.TGSLIPRM,
						KDPART = data.KDPART,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlMRModel> FindByID(int id)
        {
            var message = new MessageModel<XmlMRModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new XmlMRModel();

                var data = _XmlMRRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.NOORDP = data.NOORDP;
						result.NOSLIPRM = data.NOSLIPRM;
						result.TGSLIPRM = data.TGSLIPRM;
						result.KDPART = data.KDPART;
						result.KOBAR = data.KOBAR;
						result.QTY = data.QTY;
						result.DownloadDate = data.DownloadDate;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _XmlMRRepository.Count(criteria, whereClause);
        }

        public MessageModel<XmlMRModel> FindAll()
        {
            var message = new MessageModel<XmlMRModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlMRRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new XmlMRModel()
                    {
                        ID = data.ID,
						NOORDP = data.NOORDP,
						NOSLIPRM = data.NOSLIPRM,
						TGSLIPRM = data.TGSLIPRM,
						KDPART = data.KDPART,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlMRModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            var message = new MessageModel<XmlMRModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlMRRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new XmlMRModel()
                    {
                        ID = data.ID,
						NOORDP = data.NOORDP,
						NOSLIPRM = data.NOSLIPRM,
						TGSLIPRM = data.TGSLIPRM,
						KDPART = data.KDPART,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


