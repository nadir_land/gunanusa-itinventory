using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class XmlPMRService : IXmlPMRService
    {
        #region private	
        private List<XmlPMRModel> listModel;
        XmlPMRRepository _XmlPMRRepository;
        protected Logger logger;
        #endregion

        public XmlPMRService()
        {
            _XmlPMRRepository = new XmlPMRRepository();
            listModel = new List<XmlPMRModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<XmlPMRModel> Add(XmlPMRModel model)
        {
            var jsonObj = new MessageModel<XmlPMRModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new XmlPMR()
                    {
                        ID = model.ID,
						NOORDPR = model.NOORDPR,
						NOFGMSK = model.NOFGMSK,
						TGFGMSK = model.TGFGMSK,
						KDPART = model.KDPART,
						QTY = model.QTY,
						DownloadDate = model.DownloadDate,

                    };
                    int ID = (_XmlPMRRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<XmlPMRModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<XmlPMRModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlPMRModel> Update(XmlPMRModel model)
        {
            var jsonObj = new MessageModel<XmlPMRModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _XmlPMRRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.NOORDPR = model.NOORDPR;
						data.NOFGMSK = model.NOFGMSK;
						data.TGFGMSK = model.TGFGMSK;
						data.KDPART = model.KDPART;
						data.QTY = model.QTY;
						data.DownloadDate = model.DownloadDate;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_XmlPMRRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<XmlPMRModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlPMRModel> Delete(XmlPMRModel model)
        {
            var jsonObj = new MessageModel<XmlPMRModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _XmlPMRRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_XmlPMRRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<XmlPMRModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlPMRModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<XmlPMRModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _XmlPMRRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new XmlPMRModel()
                    {
                        ID = data.ID,
						NOORDPR = data.NOORDPR,
						NOFGMSK = data.NOFGMSK,
						TGFGMSK = data.TGFGMSK,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlPMRModel> FindByID(int id)
        {
            var message = new MessageModel<XmlPMRModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new XmlPMRModel();

                var data = _XmlPMRRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.NOORDPR = data.NOORDPR;
						result.NOFGMSK = data.NOFGMSK;
						result.TGFGMSK = data.TGFGMSK;
						result.KDPART = data.KDPART;
						result.QTY = data.QTY;
						result.DownloadDate = data.DownloadDate;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _XmlPMRRepository.Count(criteria, whereClause);
        }

        public MessageModel<XmlPMRModel> FindAll()
        {
            var message = new MessageModel<XmlPMRModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlPMRRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new XmlPMRModel()
                    {
                        ID = data.ID,
						NOORDPR = data.NOORDPR,
						NOFGMSK = data.NOFGMSK,
						TGFGMSK = data.TGFGMSK,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlPMRModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            var message = new MessageModel<XmlPMRModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlPMRRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new XmlPMRModel()
                    {
                        ID = data.ID,
						NOORDPR = data.NOORDPR,
						NOFGMSK = data.NOFGMSK,
						TGFGMSK = data.TGFGMSK,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


