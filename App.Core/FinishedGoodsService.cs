using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class FinishedGoodsService : IFinishedGoodsService
    {
        #region private	
        private List<FinishedGoodsModel> listModel;
        FinishedGoodsRepository _FinishedGoodsRepository;
        protected Logger logger;
        #endregion

        public FinishedGoodsService()
        {
            _FinishedGoodsRepository = new FinishedGoodsRepository();
            listModel = new List<FinishedGoodsModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<FinishedGoodsModel> Add(FinishedGoodsModel model)
        {
            var jsonObj = new MessageModel<FinishedGoodsModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsFinishedGoods()
                    {
                        ID = model.ID,
						KodeFG = model.KodeFG,
						NamaFG = model.NamaFG,
						GudangFG = model.GudangFG,
						HSExport = model.HSExport,
						Satuan = model.Satuan,
						Keterangan = model.Keterangan,
						Foto = model.Foto,

                    };
                    int ID = (_FinishedGoodsRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<FinishedGoodsModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<FinishedGoodsModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<FinishedGoodsModel> Update(FinishedGoodsModel model)
        {
            var jsonObj = new MessageModel<FinishedGoodsModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _FinishedGoodsRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.KodeFG = model.KodeFG;
						data.NamaFG = model.NamaFG;
						data.GudangFG = model.GudangFG;
						data.HSExport = model.HSExport;
						data.Satuan = model.Satuan;
						data.Keterangan = model.Keterangan;
						data.Foto = model.Foto;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_FinishedGoodsRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<FinishedGoodsModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<FinishedGoodsModel> Delete(FinishedGoodsModel model)
        {
            var jsonObj = new MessageModel<FinishedGoodsModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _FinishedGoodsRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_FinishedGoodsRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<FinishedGoodsModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<FinishedGoodsModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<FinishedGoodsModel>();
            var message = new MessageModel<FinishedGoodsModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _FinishedGoodsRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new FinishedGoodsModel()
                    {
                        ID = data.ID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						GudangFG = data.GudangFG,
						HSExport = data.HSExport,
						Satuan = data.Satuan,
						Keterangan = data.Keterangan,
						Foto = data.Foto,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<FinishedGoodsModel> FindByID(int id)
        {
            var message = new MessageModel<FinishedGoodsModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new FinishedGoodsModel();

                var data = _FinishedGoodsRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.KodeFG = data.KodeFG;
						result.NamaFG = data.NamaFG;
						result.GudangFG = data.GudangFG;
						result.HSExport = data.HSExport;
						result.Satuan = data.Satuan;
						result.Keterangan = data.Keterangan;
						result.Foto = data.Foto;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _FinishedGoodsRepository.Count(criteria, whereClause);
        }

        public MessageModel<FinishedGoodsModel> FindAll()
        {
            listModel = new List<FinishedGoodsModel>();
            var message = new MessageModel<FinishedGoodsModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _FinishedGoodsRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new FinishedGoodsModel()
                    {
                        ID = data.ID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						GudangFG = data.GudangFG,
						HSExport = data.HSExport,
						Satuan = data.Satuan,
						Keterangan = data.Keterangan,
						Foto = data.Foto,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<FinishedGoodsModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<FinishedGoodsModel>();
            var message = new MessageModel<FinishedGoodsModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _FinishedGoodsRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new FinishedGoodsModel()
                    {
                        ID = data.ID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						GudangFG = data.GudangFG,
						HSExport = data.HSExport,
						Satuan = data.Satuan,
						Keterangan = data.Keterangan,
						Foto = data.Foto,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


