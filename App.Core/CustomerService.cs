using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class CustomerService : ICustomerService
    {
        #region private	
        private List<CustomerModel> listModel;
        CustomerRepository _CustomerRepository;
        protected Logger logger;
        #endregion

        public CustomerService()
        {
            _CustomerRepository = new CustomerRepository();
            listModel = new List<CustomerModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<CustomerModel> Add(CustomerModel model)
        {
            var jsonObj = new MessageModel<CustomerModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsCustomer()
                    {
                        ID = model.ID,
						KodeCustomer = model.KodeCustomer,
						NamaCustomer = model.NamaCustomer,
						AlamatCustomer = model.AlamatCustomer,
						Negara = model.Negara,
						Telp = model.Telp,
						Fax = model.Fax,
						Contact = model.Contact,
						Status = model.Status,

                    };
                    int ID = (_CustomerRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<CustomerModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<CustomerModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<CustomerModel> Update(CustomerModel model)
        {
            var jsonObj = new MessageModel<CustomerModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _CustomerRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.KodeCustomer = model.KodeCustomer;
						data.NamaCustomer = model.NamaCustomer;
						data.AlamatCustomer = model.AlamatCustomer;
						data.Negara = model.Negara;
						data.Telp = model.Telp;
						data.Fax = model.Fax;
						data.Contact = model.Contact;
						data.Status = model.Status;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_CustomerRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<CustomerModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<CustomerModel> Delete(CustomerModel model)
        {
            var jsonObj = new MessageModel<CustomerModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _CustomerRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_CustomerRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<CustomerModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<CustomerModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<CustomerModel>();
            var message = new MessageModel<CustomerModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _CustomerRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new CustomerModel()
                    {
                        ID = data.ID,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						AlamatCustomer = data.AlamatCustomer,
						Negara = data.Negara,
						Telp = data.Telp,
						Fax = data.Fax,
						Contact = data.Contact,
						Status = data.Status,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<CustomerModel> FindByID(int id)
        {
            var message = new MessageModel<CustomerModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new CustomerModel();

                var data = _CustomerRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.KodeCustomer = data.KodeCustomer;
						result.NamaCustomer = data.NamaCustomer;
						result.AlamatCustomer = data.AlamatCustomer;
						result.Negara = data.Negara;
						result.Telp = data.Telp;
						result.Fax = data.Fax;
						result.Contact = data.Contact;
						result.Status = data.Status;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _CustomerRepository.Count(criteria, whereClause);
        }

        public MessageModel<CustomerModel> FindAll()
        {
            listModel = new List<CustomerModel>();
            var message = new MessageModel<CustomerModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _CustomerRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new CustomerModel()
                    {
                        ID = data.ID,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						AlamatCustomer = data.AlamatCustomer,
						Negara = data.Negara,
						Telp = data.Telp,
						Fax = data.Fax,
						Contact = data.Contact,
						Status = data.Status,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<CustomerModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<CustomerModel>();
            var message = new MessageModel<CustomerModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _CustomerRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new CustomerModel()
                    {
                        ID = data.ID,
						KodeCustomer = data.KodeCustomer,
						NamaCustomer = data.NamaCustomer,
						AlamatCustomer = data.AlamatCustomer,
						Negara = data.Negara,
						Telp = data.Telp,
						Fax = data.Fax,
						Contact = data.Contact,
						Status = data.Status,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


