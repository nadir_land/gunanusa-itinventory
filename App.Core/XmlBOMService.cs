using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class XmlBOMService : IXmlBOMService
    {
        #region private	
        private List<XmlBOMModel> listModel;
        XmlBOMRepository _XmlBOMRepository;
        protected Logger logger;
        #endregion

        public XmlBOMService()
        {
            _XmlBOMRepository = new XmlBOMRepository();
            listModel = new List<XmlBOMModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<XmlBOMModel> Add(XmlBOMModel model)
        {
            var jsonObj = new MessageModel<XmlBOMModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new XmlBOM()
                    {
                        ID = model.ID,
						BOMNO = model.BOMNO,
						KDPART = model.KDPART,
						BOMQTY = model.BOMQTY,
						KOBAR = model.KOBAR,
						QTY = model.QTY,
						DownloadDate = model.DownloadDate,

                    };
                    int ID = (_XmlBOMRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<XmlBOMModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<XmlBOMModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlBOMModel> Update(XmlBOMModel model)
        {
            var jsonObj = new MessageModel<XmlBOMModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _XmlBOMRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.BOMNO = model.BOMNO;
						data.KDPART = model.KDPART;
						data.BOMQTY = model.BOMQTY;
						data.KOBAR = model.KOBAR;
						data.QTY = model.QTY;
						data.DownloadDate = model.DownloadDate;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_XmlBOMRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<XmlBOMModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlBOMModel> Delete(XmlBOMModel model)
        {
            var jsonObj = new MessageModel<XmlBOMModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _XmlBOMRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_XmlBOMRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<XmlBOMModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlBOMModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<XmlBOMModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _XmlBOMRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new XmlBOMModel()
                    {
                        ID = data.ID,
						BOMNO = data.BOMNO,
						KDPART = data.KDPART,
						BOMQTY = data.BOMQTY,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlBOMModel> FindByID(int id)
        {
            var message = new MessageModel<XmlBOMModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new XmlBOMModel();

                var data = _XmlBOMRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.BOMNO = data.BOMNO;
						result.KDPART = data.KDPART;
						result.BOMQTY = data.BOMQTY;
						result.KOBAR = data.KOBAR;
						result.QTY = data.QTY;
						result.DownloadDate = data.DownloadDate;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _XmlBOMRepository.Count(criteria, whereClause);
        }

        public MessageModel<XmlBOMModel> FindAll()
        {
            var message = new MessageModel<XmlBOMModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlBOMRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new XmlBOMModel()
                    {
                        ID = data.ID,
						BOMNO = data.BOMNO,
						KDPART = data.KDPART,
						BOMQTY = data.BOMQTY,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlBOMModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            var message = new MessageModel<XmlBOMModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlBOMRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new XmlBOMModel()
                    {
                        ID = data.ID,
						BOMNO = data.BOMNO,
						KDPART = data.KDPART,
						BOMQTY = data.BOMQTY,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


