﻿using App.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Model;
using App.Repository.Repository;
using NLog;
using App.Repository.Domain;
using App.Utility;

namespace App.Core
{
    public class MenuService : IMenuService
    {
        #region private
        private List<MenuModel> listModel;
        MenuRepository _menuRepository;
        protected Logger logger;
        #endregion

        public MenuService()
        {
            _menuRepository = new MenuRepository();
            listModel = new List<MenuModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<MenuModel> Add(MenuModel model)
        {
            var jsonObj = new MessageModel<MenuModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsMenus()
                    {
                        Link = model.Link,
                        DisplayMenu = model.DisplayMenu,
                        ClassIcon = model.ClassIcon,
                        Parent = model.Parent,
                        TypeMenu = model.TypeMenu
                    };
                    if (_menuRepository.Add(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<MenuModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<MenuModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }


        public MessageModel<MenuModel> Delete(MenuModel model)
        {
            var jsonObj = new MessageModel<MenuModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _menuRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_menuRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<MenuModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<MenuModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<MenuModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _menuRepository.FindWithPaging(criteria, whereClause, pageSize, page, "ID");
                foreach (var data in List)
                {
                    listModel.Add(new MenuModel()
                    {
                        ID = data.ID,
                        Link = data.Link,
                        DisplayMenu = data.DisplayMenu,
                        ClassIcon = data.ClassIcon,
                        Parent = data.Parent,
                        TypeMenu = data.TypeMenu
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<MenuModel> FindByID(int id)
        {
            var message = new MessageModel<MenuModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new MenuModel();

                var data = _menuRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
                    result.Link = data.Link;
                    result.DisplayMenu = data.DisplayMenu;
                    result.ClassIcon = data.ClassIcon;
                    result.Parent = data.Parent;
                    result.TypeMenu = data.TypeMenu;

                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }



        public MessageModel<MenuModel> Update(MenuModel model)
        {
            var jsonObj = new MessageModel<MenuModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _menuRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.Link = model.Link;
                        data.DisplayMenu = model.DisplayMenu;
                        data.ClassIcon = model.ClassIcon;
                        data.Parent = model.Parent;
                        data.TypeMenu = model.TypeMenu;

                    }
                    if (_menuRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<MenuModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _menuRepository.Count(criteria, whereClause);
        }

        public MessageModel<MenuModel> FindAll()
        {
            var message = new MessageModel<MenuModel>();
            try
            {
                message.message = "Ok";
                message.success = true;                
                var List = _menuRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new MenuModel()
                    {
                        ID = data.ID,
                        Link = data.Link,
                        DisplayMenu = data.DisplayMenu,
                        ClassIcon = data.ClassIcon,
                        Parent = data.Parent,
                        TypeMenu = data.TypeMenu
                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<MenuModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            var message = new MessageModel<MenuModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _menuRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new MenuModel()
                    {
                        ID = data.ID,
                        Link = data.Link,
                        DisplayMenu = data.DisplayMenu,
                        ClassIcon = data.ClassIcon,
                        Parent = data.Parent,
                        TypeMenu = data.TypeMenu
                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}
