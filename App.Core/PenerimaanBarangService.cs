using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class PenerimaanBarangService : IPenerimaanBarangService
    {
        #region private	
        private List<PenerimaanBarangModel> listModel;
        PenerimaanBarangRepository _PenerimaanBarangRepository;
        protected Logger logger;
        #endregion

        public PenerimaanBarangService()
        {
            _PenerimaanBarangRepository = new PenerimaanBarangRepository();
            listModel = new List<PenerimaanBarangModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<PenerimaanBarangModel> Add(PenerimaanBarangModel model)
        {
            var jsonObj = new MessageModel<PenerimaanBarangModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrPenerimaanBarang()
                    {
                        ID = model.ID,
						ReceiptNo = model.ReceiptNo,
						ReceiptDate = model.ReceiptDate,
						Warehouse = model.Warehouse,
						CAR = model.CAR,
						NoPIB = model.NoPIB,
						TanggalPIB = model.TanggalPIB,
						KPBC = model.KPBC,
						AsalBarang = model.AsalBarang,
						Valuta = model.Valuta,
						Rate = model.Rate,
						Status = model.Status,
						KodeBarang = model.KodeBarang,
						NamaBarang = model.NamaBarang,
						HSImport = model.HSImport,
						Satuan = model.Satuan,
						NoSeri = model.NoSeri,
						TarifBM = model.TarifBM,
						CifPIB = model.CifPIB,
						QtyPIB = model.QtyPIB,
						BmPIB = model.BmPIB,
						PpnPIB = model.PpnPIB,
						QtyRI = model.QtyRI,

                    };
                    int ID = (_PenerimaanBarangRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<PenerimaanBarangModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<PenerimaanBarangModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<PenerimaanBarangModel> Update(PenerimaanBarangModel model)
        {
            var jsonObj = new MessageModel<PenerimaanBarangModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _PenerimaanBarangRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.ReceiptNo = model.ReceiptNo;
						data.ReceiptDate = model.ReceiptDate;
						data.Warehouse = model.Warehouse;
						data.CAR = model.CAR;
						data.NoPIB = model.NoPIB;
						data.TanggalPIB = model.TanggalPIB;
						data.KPBC = model.KPBC;
						data.AsalBarang = model.AsalBarang;
						data.Valuta = model.Valuta;
						data.Rate = model.Rate;
						data.Status = model.Status;
						data.KodeBarang = model.KodeBarang;
						data.NamaBarang = model.NamaBarang;
						data.HSImport = model.HSImport;
						data.Satuan = model.Satuan;
						data.NoSeri = model.NoSeri;
						data.TarifBM = model.TarifBM;
						data.CifPIB = model.CifPIB;
						data.QtyPIB = model.QtyPIB;
						data.BmPIB = model.BmPIB;
						data.PpnPIB = model.PpnPIB;
						data.QtyRI = model.QtyRI;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_PenerimaanBarangRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<PenerimaanBarangModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<PenerimaanBarangModel> Delete(PenerimaanBarangModel model)
        {
            var jsonObj = new MessageModel<PenerimaanBarangModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _PenerimaanBarangRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_PenerimaanBarangRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<PenerimaanBarangModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<PenerimaanBarangModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<PenerimaanBarangModel>();
            var message = new MessageModel<PenerimaanBarangModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _PenerimaanBarangRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new PenerimaanBarangModel()
                    {
                        ID = data.ID,
						ReceiptNo = data.ReceiptNo,
						ReceiptDate = data.ReceiptDate,
						Warehouse = data.Warehouse,
						CAR = data.CAR,
						NoPIB = data.NoPIB,
						TanggalPIB = data.TanggalPIB,
						KPBC = data.KPBC,
						AsalBarang = data.AsalBarang,
						Valuta = data.Valuta,
						Rate = data.Rate,
						Status = data.Status,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						NoSeri = data.NoSeri,
						TarifBM = data.TarifBM,
						CifPIB = data.CifPIB,
						QtyPIB = data.QtyPIB,
						BmPIB = data.BmPIB,
						PpnPIB = data.PpnPIB,
						QtyRI = data.QtyRI,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<PenerimaanBarangModel> FindByID(int id)
        {
            var message = new MessageModel<PenerimaanBarangModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new PenerimaanBarangModel();

                var data = _PenerimaanBarangRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.ReceiptNo = data.ReceiptNo;
						result.ReceiptDate = data.ReceiptDate;
						result.Warehouse = data.Warehouse;
						result.CAR = data.CAR;
						result.NoPIB = data.NoPIB;
						result.TanggalPIB = data.TanggalPIB;
						result.KPBC = data.KPBC;
						result.AsalBarang = data.AsalBarang;
						result.Valuta = data.Valuta;
						result.Rate = data.Rate;
						result.Status = data.Status;
						result.KodeBarang = data.KodeBarang;
						result.NamaBarang = data.NamaBarang;
						result.HSImport = data.HSImport;
						result.Satuan = data.Satuan;
						result.NoSeri = data.NoSeri;
						result.TarifBM = data.TarifBM;
						result.CifPIB = data.CifPIB;
						result.QtyPIB = data.QtyPIB;
						result.BmPIB = data.BmPIB;
						result.PpnPIB = data.PpnPIB;
						result.QtyRI = data.QtyRI;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _PenerimaanBarangRepository.Count(criteria, whereClause);
        }

        public MessageModel<PenerimaanBarangModel> FindAll()
        {
            listModel = new List<PenerimaanBarangModel>();
            var message = new MessageModel<PenerimaanBarangModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _PenerimaanBarangRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new PenerimaanBarangModel()
                    {
                        ID = data.ID,
						ReceiptNo = data.ReceiptNo,
						ReceiptDate = data.ReceiptDate,
						Warehouse = data.Warehouse,
						CAR = data.CAR,
						NoPIB = data.NoPIB,
						TanggalPIB = data.TanggalPIB,
						KPBC = data.KPBC,
						AsalBarang = data.AsalBarang,
						Valuta = data.Valuta,
						Rate = data.Rate,
						Status = data.Status,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						NoSeri = data.NoSeri,
						TarifBM = data.TarifBM,
						CifPIB = data.CifPIB,
						QtyPIB = data.QtyPIB,
						BmPIB = data.BmPIB,
						PpnPIB = data.PpnPIB,
						QtyRI = data.QtyRI,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<PenerimaanBarangModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<PenerimaanBarangModel>();
            var message = new MessageModel<PenerimaanBarangModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _PenerimaanBarangRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new PenerimaanBarangModel()
                    {
                        ID = data.ID,
						ReceiptNo = data.ReceiptNo,
						ReceiptDate = data.ReceiptDate,
						Warehouse = data.Warehouse,
						CAR = data.CAR,
						NoPIB = data.NoPIB,
						TanggalPIB = data.TanggalPIB,
						KPBC = data.KPBC,
						AsalBarang = data.AsalBarang,
						Valuta = data.Valuta,
						Rate = data.Rate,
						Status = data.Status,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						NoSeri = data.NoSeri,
						TarifBM = data.TarifBM,
						CifPIB = data.CifPIB,
						QtyPIB = data.QtyPIB,
						BmPIB = data.BmPIB,
						PpnPIB = data.PpnPIB,
						QtyRI = data.QtyRI,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


