﻿using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core
{
    public class UserRoleService : IUserRoleService
    {
        #region private
        private List<UserRoleModel> listModel;
        UserRoleRepository _repository;
        protected Logger logger;
        #endregion

        public UserRoleService()
        {
            _repository = new UserRoleRepository();
            listModel = new List<UserRoleModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<UserRoleModel> Add(UserRoleModel model)
        {
            var jsonObj = new MessageModel<UserRoleModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsUserRole()
                    {
                        UserID = model.UserID,
                        RoleID = model.RoleID,
                        CreatedBy = model.SessionUser,
                        CreatedOn = DateTime.Now                        
                    };
                    if (_repository.Add(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<UserRoleModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<UserRoleModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<UserRoleModel> Update(UserRoleModel model)
        {
            var jsonObj = new MessageModel<UserRoleModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _repository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.UserID = model.UserID;
                        data.RoleID = model.RoleID;
                        data.UpdateOn = DateTime.Now;
                        data.UpdateBy = model.SessionUser;                        

                    }
                    if (_repository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<UserRoleModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<UserRoleModel> Delete(UserRoleModel model)
        {
            var jsonObj = new MessageModel<UserRoleModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _repository.FindByID(model.ID);

                if (data != null)
                {
                    if (_repository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<UserRoleModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<UserRoleModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            
            var message = new MessageModel<UserRoleModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _repository.FindWithPaging(criteria, whereClause, pageSize, page, "ID");
                foreach (var data in List)
                {
                    listModel.Add(new UserRoleModel()
                    {
                        ID = data.ID,
                        RoleName = data.Role.RoleName,
                        UserName = data.User.Username                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<UserRoleModel> FindByID(int id)
        {
            var message = new MessageModel<UserRoleModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new UserRoleModel();

                var data = _repository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
                    result.UserID = Convert.ToInt16(data.UserID);
                    result.RoleID = Convert.ToInt16(data.RoleID);
                    result.RoleName = data.Role.RoleName;
                    result.UserName = data.User.Username; 

                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _repository.Count(criteria, whereClause);
        }

        public MessageModel<UserRoleModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string orderby)
        {
            var message = new MessageModel<UserRoleModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _repository.FindByCriteria(criteria, whereClause, orderby);
                foreach (var data in List)
                {
                    listModel.Add(new UserRoleModel()
                    {
                        ID = data.ID,
                        RoleName = data.Role.RoleName,
                        UserName = data.User.Username
                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}
