﻿using App.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Repository.Domain;
using App.Core.Interface;
using App.Model;
using NLog;
using System.Security;
using Microsoft.IdentityModel.Tokens;
using System.Configuration;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using App.Utility;

namespace App.Core
{
    public class UserService : IUserService
    {
        #region private
        private List<UserModel> userListModel;
        UserRepository _userRepository;
        protected Logger logger;
        Lib.Class1 lib;
        private static string Key = "Covid-19";
        #endregion

        public UserService()
        {
            _userRepository = new UserRepository();
            userListModel = new List<UserModel>();
            logger = App.Utility.CampusLogger.GetLogger();
            lib = new Lib.Class1();
        }


        public MessageModel<UserModel> Add(UserModel model)
        {           
            var jsonObj = new MessageModel<UserModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsUser()
                    {
                        Username = model.Username,
                        Department = model.Department,
                        Email = model.Email,
                        NPK = model.NPK,
                        FullName = model.FullName,
                        Password = lib.Encrypt(model.Password, true, Key),
                        CreatedOn = DateTime.Now,
                        CreatedBy = model.SessionUser,
                        Status = model.Status,
                        UserRole = model.UserRole
                    };
                    int result = _userRepository.Add(data);
                    if (result <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    }                    
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<UserModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<UserModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
            
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _userRepository.Count(criteria, whereClause);
        }

        public MessageModel<UserModel> Delete(UserModel model)
        {
            var jsonObj = new MessageModel<UserModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _userRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_userRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }
                
                jsonObj = new MessageModel<UserModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<UserModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string orderBy)
        {
            userListModel = new List<UserModel>();
            var message = new MessageModel<UserModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var userList = _userRepository.FindByCriteria(criteria, whereClause, orderBy);
                foreach (var user in userList)
                {
                    userListModel.Add(new UserModel()
                    {
                        ID = user.ID,
                        Username = user.Username,
                        Department = user.Department,
                        Email = user.Email,
                        NPK = user.NPK,
                        FullName = user.FullName,
                        Status = user.Status,
                        UserRole = user.UserRole
                    });
                }
                message.total = userList.Count();
                message.rows = userListModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<UserModel> FindByID(int id)
        {
            var message = new MessageModel<UserModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new UserModel();

                var data = _userRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
                    result.Username = data.Username;
                    result.Department = data.Department;
                    result.Email = data.Email;
                    result.NPK = data.NPK;
                    result.FullName = data.FullName;
                    result.Status = data.Status;
                    result.UserRole = data.UserRole;
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<UserModel> FindAll()
        {
            userListModel = new List<UserModel>();
            var message = new MessageModel<UserModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var userList = _userRepository.FindAll();
                foreach (var user in userList)
                {
                    userListModel.Add(new UserModel()
                    {
                        ID = user.ID,
                        Username = user.Username,
                        Department = user.Department,
                        Email = user.Email,
                        NPK = user.NPK,
                        FullName = user.FullName,
                        Status = user.Status,
                        UserRole = user.UserRole
                    });
                }
                message.total = userList.Count();
                message.rows = userListModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<UserModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            userListModel = new List<UserModel>();
            var message = new MessageModel<UserModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var userList = _userRepository.FindWithPaging(criteria, whereClause, pageSize, page, "ID");
                foreach (var user in userList)
                {
                    userListModel.Add(new UserModel()
                    {
                        ID = user.ID,
                        Username = user.Username,
                        Department = user.Department,
                        Email = user.Email,
                        NPK = user.NPK,
                        FullName = user.FullName,
                        Status = user.Status,
                        UserRole = user.UserRole
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = userListModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
        
        public MessageModel<UserModel> Update(UserModel model)
        {
            var jsonObj = new MessageModel<UserModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _userRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.Username = model.Username;
                        data.Department = model.Department;
                        data.Email = model.Email;
                        data.NPK = model.NPK;
                        data.FullName = model.FullName;
                        data.UpdateBy = model.SessionUser;
                        data.UpdateOn = DateTime.Now;
                        data.Status = model.Status;
                        data.UserRole = model.UserRole;
                    }
                    if (_userRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<UserModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<UserModel> ChangePassword(UserModel model)
        {
            var jsonObj = new MessageModel<UserModel>();
            try
            {
                var message = "Password Successfully Updated";
                var success = true;

                if (model.IsValid())
                {
                    var data = _userRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.Password = lib.Encrypt(model.Password, true, Key);                        
                    }
                    if (_userRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<UserModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public bool ValidUser(string userName, string Password)
        {
            bool isValid = false;
            List<WhereClause> whereClause = new List<WhereClause>();
            whereClause.Add(new WhereClause() { Property = "Username", SqlOperator = "=", Value = userName });
            string criteria = "Username=@Username";
            var user = _userRepository.FindByCriteria(criteria, whereClause, "ID").ToList();
            if (user.Count()> 0)
            {
                string pass = lib.Decrypt(user[0].Password, true, Key);

                if (pass.Equals(Password))
                {
                    isValid = true;
                }

            }

            return isValid;
        }

        private String generateToken(UserModel user)
        {
            var roles = new List<String>();
            //roles.Add(user.Role);

            var JwtExpireDays = ConfigurationManager.AppSettings["JwtExpireDays"].ToString();
            var JwtKey = ConfigurationManager.AppSettings["JwtKey"].ToString();
            var JwtIssuer = ConfigurationManager.AppSettings["JwtIssuer"].ToString();

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(JwtKey));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(JwtExpireDays));
            var issuer = JwtIssuer;

            var claims = new Claim[] {
                new Claim (ClaimTypes.Name, user.FullName),
                new Claim (ClaimTypes.NameIdentifier, user.NPK)
            };

            ClaimsIdentity claimsIdentity = new ClaimsIdentity(claims, "Token");
            foreach (var role in roles)
            {
                claimsIdentity.AddClaim(new Claim(ClaimsIdentity.DefaultRoleClaimType, role));
            }

            var token = new JwtSecurityToken(
                issuer,
                issuer,
                claimsIdentity.Claims,
                expires: expires,
                signingCredentials: creds
            );

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            return tokenHandler.WriteToken(token);
        }
    }
}
