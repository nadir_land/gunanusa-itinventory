using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class VendorService : IVendorService
    {
        #region private	
        private List<VendorModel> listModel;
        VendorRepository _VendorRepository;
        protected Logger logger;
        #endregion

        public VendorService()
        {
            _VendorRepository = new VendorRepository();
            listModel = new List<VendorModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<VendorModel> Add(VendorModel model)
        {
            var jsonObj = new MessageModel<VendorModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsVendor()
                    {
                        ID = model.ID,
						KodeVendor = model.KodeVendor,
						NamaVendor = model.NamaVendor,
						AlamatVendor = model.AlamatVendor,
						Negara = model.Negara,
						Telp = model.Telp,
						Fax = model.Fax,
						ContactPerson = model.ContactPerson,
						Status = model.Status,

                    };
                    int ID = (_VendorRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<VendorModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<VendorModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<VendorModel> Update(VendorModel model)
        {
            var jsonObj = new MessageModel<VendorModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _VendorRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.KodeVendor = model.KodeVendor;
						data.NamaVendor = model.NamaVendor;
						data.AlamatVendor = model.AlamatVendor;
						data.Negara = model.Negara;
						data.Telp = model.Telp;
						data.Fax = model.Fax;
						data.ContactPerson = model.ContactPerson;
						data.Status = model.Status;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_VendorRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<VendorModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<VendorModel> Delete(VendorModel model)
        {
            var jsonObj = new MessageModel<VendorModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _VendorRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_VendorRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<VendorModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<VendorModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<VendorModel>();
            var message = new MessageModel<VendorModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _VendorRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new VendorModel()
                    {
                        ID = data.ID,
						KodeVendor = data.KodeVendor,
						NamaVendor = data.NamaVendor,
						AlamatVendor = data.AlamatVendor,
						Negara = data.Negara,
						Telp = data.Telp,
						Fax = data.Fax,
						ContactPerson = data.ContactPerson,
						Status = data.Status,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<VendorModel> FindByID(int id)
        {
            var message = new MessageModel<VendorModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new VendorModel();

                var data = _VendorRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.KodeVendor = data.KodeVendor;
						result.NamaVendor = data.NamaVendor;
						result.AlamatVendor = data.AlamatVendor;
						result.Negara = data.Negara;
						result.Telp = data.Telp;
						result.Fax = data.Fax;
						result.ContactPerson = data.ContactPerson;
						result.Status = data.Status;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _VendorRepository.Count(criteria, whereClause);
        }

        public MessageModel<VendorModel> FindAll()
        {
            listModel = new List<VendorModel>();
            var message = new MessageModel<VendorModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _VendorRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new VendorModel()
                    {
                        ID = data.ID,
						KodeVendor = data.KodeVendor,
						NamaVendor = data.NamaVendor,
						AlamatVendor = data.AlamatVendor,
						Negara = data.Negara,
						Telp = data.Telp,
						Fax = data.Fax,
						ContactPerson = data.ContactPerson,
						Status = data.Status,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<VendorModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<VendorModel>();
            var message = new MessageModel<VendorModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _VendorRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new VendorModel()
                    {
                        ID = data.ID,
						KodeVendor = data.KodeVendor,
						NamaVendor = data.NamaVendor,
						AlamatVendor = data.AlamatVendor,
						Negara = data.Negara,
						Telp = data.Telp,
						Fax = data.Fax,
						ContactPerson = data.ContactPerson,
						Status = data.Status,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


