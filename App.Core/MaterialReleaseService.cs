using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class MaterialReleaseService : IMaterialReleaseService
    {
        #region private	
        private List<MaterialReleaseModel> listModel;
        MaterialReleaseRepository _MaterialReleaseRepository;
        protected Logger logger;
        #endregion

        public MaterialReleaseService()
        {
            _MaterialReleaseRepository = new MaterialReleaseRepository();
            listModel = new List<MaterialReleaseModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<MaterialReleaseModel> Add(MaterialReleaseModel model)
        {
            var jsonObj = new MessageModel<MaterialReleaseModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrMaterialRelease()
                    {
                        ID = model.ID,
						OpsiProduksi = model.OpsiProduksi,
						NoWO = model.NoWO,
						TglWO = model.TglWO,
						NoRelease = model.NoRelease,
                        TglRelease = model.TglRelease,
						KodeSubkon = model.KodeSubkon,
						NamaSubkon = model.NamaSubkon,
						Alamat = model.Alamat,
						Keterangan = model.Keterangan,
						KodeFG = model.KodeFG,
						NamaFG = model.NamaFG,
						HSExport = model.HSExport,
						SatuanExport = model.SatuanExport,
						QtyOrder = model.QtyOrder,
						KodeBarang = model.KodeBarang,
						NamaBarang = model.NamaBarang,
						Satuan = model.Satuan,
						HSImport = model.HSImport,
						Dibutuhkan = model.Dibutuhkan,
						Terpakai = model.Terpakai,
						Dikeluarkan = model.Dikeluarkan,
						BelumKirim = model.BelumKirim,
						Status = model.Status,

                    };
                    int ID = (_MaterialReleaseRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<MaterialReleaseModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<MaterialReleaseModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<MaterialReleaseModel> Update(MaterialReleaseModel model)
        {
            var jsonObj = new MessageModel<MaterialReleaseModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _MaterialReleaseRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.OpsiProduksi = model.OpsiProduksi;
						data.NoWO = model.NoWO;
						data.TglWO = model.TglWO;
						data.NoRelease = model.NoRelease;
                        data.TglRelease = model.TglRelease;
						data.KodeSubkon = model.KodeSubkon;
						data.NamaSubkon = model.NamaSubkon;
						data.Alamat = model.Alamat;
						data.Keterangan = model.Keterangan;
						data.KodeFG = model.KodeFG;
						data.NamaFG = model.NamaFG;
						data.HSExport = model.HSExport;
						data.SatuanExport = model.SatuanExport;
						data.QtyOrder = model.QtyOrder;
						data.KodeBarang = model.KodeBarang;
						data.NamaBarang = model.NamaBarang;
						data.Satuan = model.Satuan;
						data.HSImport = model.HSImport;
						data.Dibutuhkan = model.Dibutuhkan;
						data.Terpakai = model.Terpakai;
						data.Dikeluarkan = model.Dikeluarkan;
						data.BelumKirim = model.BelumKirim;
						data.Status = model.Status;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_MaterialReleaseRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<MaterialReleaseModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<MaterialReleaseModel> Delete(MaterialReleaseModel model)
        {
            var jsonObj = new MessageModel<MaterialReleaseModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _MaterialReleaseRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_MaterialReleaseRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<MaterialReleaseModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<MaterialReleaseModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<MaterialReleaseModel>();
            listModel = new List<MaterialReleaseModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _MaterialReleaseRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new MaterialReleaseModel()
                    {
                        ID = data.ID,
						OpsiProduksi = data.OpsiProduksi,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						NoRelease = data.NoRelease,
                        TglRelease = data.TglRelease,
                        KodeSubkon = data.KodeSubkon,
						NamaSubkon = data.NamaSubkon,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						QtyOrder = data.QtyOrder,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						Satuan = data.Satuan,
						HSImport = data.HSImport,
						Dibutuhkan = data.Dibutuhkan,
						Terpakai = data.Terpakai,
						Dikeluarkan = data.Dikeluarkan,
						BelumKirim = data.BelumKirim,
						Status = data.Status,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<MaterialReleaseModel> FindByID(int id)
        {
            var message = new MessageModel<MaterialReleaseModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new MaterialReleaseModel();

                var data = _MaterialReleaseRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.OpsiProduksi = data.OpsiProduksi;
						result.NoWO = data.NoWO;
						result.TglWO = data.TglWO;
						result.NoRelease = data.NoRelease;
                    result.TglRelease = data.TglRelease;
						result.KodeSubkon = data.KodeSubkon;
						result.NamaSubkon = data.NamaSubkon;
						result.Alamat = data.Alamat;
						result.Keterangan = data.Keterangan;
						result.KodeFG = data.KodeFG;
						result.NamaFG = data.NamaFG;
						result.HSExport = data.HSExport;
						result.SatuanExport = data.SatuanExport;
						result.QtyOrder = data.QtyOrder;
						result.KodeBarang = data.KodeBarang;
						result.NamaBarang = data.NamaBarang;
						result.Satuan = data.Satuan;
						result.HSImport = data.HSImport;
						result.Dibutuhkan = data.Dibutuhkan;
						result.Terpakai = data.Terpakai;
						result.Dikeluarkan = data.Dikeluarkan;
						result.BelumKirim = data.BelumKirim;
						result.Status = data.Status;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _MaterialReleaseRepository.Count(criteria, whereClause);
        }

        public MessageModel<MaterialReleaseModel> FindAll()
        {
            listModel = new List<MaterialReleaseModel>();
            var message = new MessageModel<MaterialReleaseModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _MaterialReleaseRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new MaterialReleaseModel()
                    {
                        ID = data.ID,
						OpsiProduksi = data.OpsiProduksi,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						NoRelease = data.NoRelease,
                        TglRelease = data.TglRelease,
                        KodeSubkon = data.KodeSubkon,
						NamaSubkon = data.NamaSubkon,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						QtyOrder = data.QtyOrder,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						Satuan = data.Satuan,
						HSImport = data.HSImport,
						Dibutuhkan = data.Dibutuhkan,
						Terpakai = data.Terpakai,
						Dikeluarkan = data.Dikeluarkan,
						BelumKirim = data.BelumKirim,
						Status = data.Status,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<MaterialReleaseModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<MaterialReleaseModel>();
            var message = new MessageModel<MaterialReleaseModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _MaterialReleaseRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new MaterialReleaseModel()
                    {
                        ID = data.ID,
						OpsiProduksi = data.OpsiProduksi,
						NoWO = data.NoWO,
						TglWO = data.TglWO,
						NoRelease = data.NoRelease,
                        TglRelease = data.TglRelease,
                        KodeSubkon = data.KodeSubkon,
						NamaSubkon = data.NamaSubkon,
						Alamat = data.Alamat,
						Keterangan = data.Keterangan,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						SatuanExport = data.SatuanExport,
						QtyOrder = data.QtyOrder,
						KodeBarang = data.KodeBarang,
						NamaBarang = data.NamaBarang,
						Satuan = data.Satuan,
						HSImport = data.HSImport,
						Dibutuhkan = data.Dibutuhkan,
						Terpakai = data.Terpakai,
						Dikeluarkan = data.Dikeluarkan,
						BelumKirim = data.BelumKirim,
						Status = data.Status,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


