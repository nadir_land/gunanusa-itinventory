using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class XmlDOService : IXmlDOService
    {
        #region private	
        private List<XmlDOModel> listModel;
        XmlDORepository _XmlDORepository;
        protected Logger logger;
        #endregion

        public XmlDOService()
        {
            _XmlDORepository = new XmlDORepository();
            listModel = new List<XmlDOModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<XmlDOModel> Add(XmlDOModel model)
        {
            var jsonObj = new MessageModel<XmlDOModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new XmlDO()
                    {
                        ID = model.ID,
						NOFGKEL = model.NOFGKEL,
						TGFGKL = model.TGFGKL,
						KDPART = model.KDPART,
						QTY = model.QTY,
						DownloadDate = model.DownloadDate,

                    };
                    int ID = (_XmlDORepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<XmlDOModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<XmlDOModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlDOModel> Update(XmlDOModel model)
        {
            var jsonObj = new MessageModel<XmlDOModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _XmlDORepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.NOFGKEL = model.NOFGKEL;
						data.TGFGKL = model.TGFGKL;
						data.KDPART = model.KDPART;
						data.QTY = model.QTY;
						data.DownloadDate = model.DownloadDate;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_XmlDORepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<XmlDOModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlDOModel> Delete(XmlDOModel model)
        {
            var jsonObj = new MessageModel<XmlDOModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _XmlDORepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_XmlDORepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<XmlDOModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlDOModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<XmlDOModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _XmlDORepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new XmlDOModel()
                    {
                        ID = data.ID,
						NOFGKEL = data.NOFGKEL,
						TGFGKL = data.TGFGKL,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlDOModel> FindByID(int id)
        {
            var message = new MessageModel<XmlDOModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new XmlDOModel();

                var data = _XmlDORepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.NOFGKEL = data.NOFGKEL;
						result.TGFGKL = data.TGFGKL;
						result.KDPART = data.KDPART;
						result.QTY = data.QTY;
						result.DownloadDate = data.DownloadDate;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _XmlDORepository.Count(criteria, whereClause);
        }

        public MessageModel<XmlDOModel> FindAll()
        {
            var message = new MessageModel<XmlDOModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlDORepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new XmlDOModel()
                    {
                        ID = data.ID,
						NOFGKEL = data.NOFGKEL,
						TGFGKL = data.TGFGKL,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlDOModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            var message = new MessageModel<XmlDOModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlDORepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new XmlDOModel()
                    {
                        ID = data.ID,
						NOFGKEL = data.NOFGKEL,
						TGFGKL = data.TGFGKL,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


