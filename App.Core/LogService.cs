using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class LogService : ILogService
    {
        #region private	
        private List<LogModel> listModel;
        LogRepository _LogRepository;
        protected Logger logger;
        #endregion

        public LogService()
        {
            _LogRepository = new LogRepository();
            listModel = new List<LogModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<LogModel> Add(LogModel model)
        {
            var jsonObj = new MessageModel<LogModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrLog()
                    {
                        ID = model.ID,
						Username = model.Username,
						LogTime = model.LogTime,
						ModulName = model.ModulName,
						Activity = model.Activity,

                    };
                    int ID = (_LogRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<LogModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<LogModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<LogModel> Update(LogModel model)
        {
            var jsonObj = new MessageModel<LogModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _LogRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.Username = model.Username;
						data.LogTime = model.LogTime;
						data.ModulName = model.ModulName;
						data.Activity = model.Activity;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_LogRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<LogModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<LogModel> Delete(LogModel model)
        {
            var jsonObj = new MessageModel<LogModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _LogRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_LogRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<LogModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<LogModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<LogModel>();
            var message = new MessageModel<LogModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _LogRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new LogModel()
                    {
                        ID = data.ID,
						Username = data.Username,
						LogTime = data.LogTime,
						ModulName = data.ModulName,
						Activity = data.Activity,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<LogModel> FindByID(int id)
        {
            var message = new MessageModel<LogModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new LogModel();

                var data = _LogRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.Username = data.Username;
						result.LogTime = data.LogTime;
						result.ModulName = data.ModulName;
						result.Activity = data.Activity;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _LogRepository.Count(criteria, whereClause);
        }

        public MessageModel<LogModel> FindAll()
        {
            listModel = new List<LogModel>();
            var message = new MessageModel<LogModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _LogRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new LogModel()
                    {
                        ID = data.ID,
						Username = data.Username,
						LogTime = data.LogTime,
						ModulName = data.ModulName,
						Activity = data.Activity,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<LogModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<LogModel>();
            var message = new MessageModel<LogModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _LogRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new LogModel()
                    {
                        ID = data.ID,
						Username = data.Username,
						LogTime = data.LogTime,
						ModulName = data.ModulName,
						Activity = data.Activity,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


