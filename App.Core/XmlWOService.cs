using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class XmlWOService : IXmlWOService
    {
        #region private	
        private List<XmlWOModel> listModel;
        XmlWORepository _XmlWORepository;
        protected Logger logger;
        #endregion

        public XmlWOService()
        {
            _XmlWORepository = new XmlWORepository();
            listModel = new List<XmlWOModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<XmlWOModel> Add(XmlWOModel model)
        {
            var jsonObj = new MessageModel<XmlWOModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new XmlWO()
                    {
                        ID = model.ID,
						NOORDP = model.NOORDP,
						TGORDP = model.TGORDP,
						KDPART = model.KDPART,
						QTY = model.QTY,
						DownloadDate = model.DownloadDate,

                    };
                    int ID = (_XmlWORepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<XmlWOModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<XmlWOModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlWOModel> Update(XmlWOModel model)
        {
            var jsonObj = new MessageModel<XmlWOModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _XmlWORepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.NOORDP = model.NOORDP;
						data.TGORDP = model.TGORDP;
						data.KDPART = model.KDPART;
						data.QTY = model.QTY;
						data.DownloadDate = model.DownloadDate;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_XmlWORepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<XmlWOModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlWOModel> Delete(XmlWOModel model)
        {
            var jsonObj = new MessageModel<XmlWOModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _XmlWORepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_XmlWORepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<XmlWOModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlWOModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<XmlWOModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _XmlWORepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new XmlWOModel()
                    {
                        ID = data.ID,
						NOORDP = data.NOORDP,
						TGORDP = data.TGORDP,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlWOModel> FindByID(int id)
        {
            var message = new MessageModel<XmlWOModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new XmlWOModel();

                var data = _XmlWORepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.NOORDP = data.NOORDP;
						result.TGORDP = data.TGORDP;
						result.KDPART = data.KDPART;
						result.QTY = data.QTY;
						result.DownloadDate = data.DownloadDate;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _XmlWORepository.Count(criteria, whereClause);
        }

        public MessageModel<XmlWOModel> FindAll()
        {
            var message = new MessageModel<XmlWOModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlWORepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new XmlWOModel()
                    {
                        ID = data.ID,
						NOORDP = data.NOORDP,
						TGORDP = data.TGORDP,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlWOModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            var message = new MessageModel<XmlWOModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlWORepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new XmlWOModel()
                    {
                        ID = data.ID,
						NOORDP = data.NOORDP,
						TGORDP = data.TGORDP,
						KDPART = data.KDPART,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


