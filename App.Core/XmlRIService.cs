using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class XmlRIService : IXmlRIService
    {
        #region private	
        private List<XmlRIModel> listModel;
        XmlRIRepository _XmlRIRepository;
        RawMaterialService _RawMaterialService;
        protected Logger logger;
        #endregion

        public XmlRIService()
        {
            _XmlRIRepository = new XmlRIRepository();
            listModel = new List<XmlRIModel>();
            _RawMaterialService = new RawMaterialService();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<XmlRIModel> Add(XmlRIModel model)
        {
            var jsonObj = new MessageModel<XmlRIModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new XmlRI()
                    {
                        ID = model.ID,
						NOBUKMSK = model.NOBUKMSK,
						TGBUKMSK = model.TGBUKMSK,
						KOBAR = model.KOBAR,
						QTY = model.QTY,
						DownloadDate = model.DownloadDate,

                    };
                    int ID = (_XmlRIRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<XmlRIModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<XmlRIModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlRIModel> Update(XmlRIModel model)
        {
            var jsonObj = new MessageModel<XmlRIModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _XmlRIRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.NOBUKMSK = model.NOBUKMSK;
						data.TGBUKMSK = model.TGBUKMSK;
						data.KOBAR = model.KOBAR;
						data.QTY = model.QTY;
						data.DownloadDate = model.DownloadDate;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_XmlRIRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<XmlRIModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlRIModel> Delete(XmlRIModel model)
        {
            var jsonObj = new MessageModel<XmlRIModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _XmlRIRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_XmlRIRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<XmlRIModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<XmlRIModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<XmlRIModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _XmlRIRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    //get Material
                    List<WhereClause> wh = new List<WhereClause>();
                    wh.Add(new WhereClause { Property="KodeMaterial", Value=data.KOBAR });
                    RawMaterialModel rawMaterialModel = new RawMaterialModel(); 
                    var rawMaterial = _RawMaterialService.FindByCriteria("KodeMaterial=@KodeMaterial", wh, "ID").rows.ToList();
                    if(rawMaterial.Count > 0)
                    {
                        rawMaterialModel = rawMaterial[0];
                    }
                    listModel.Add(new XmlRIModel()
                    {
                        ID = data.ID,
						NOBUKMSK = data.NOBUKMSK,
						TGBUKMSK = data.TGBUKMSK,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,
                        RawMaterialModel = rawMaterialModel,
                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlRIModel> FindByID(int id)
        {
            var message = new MessageModel<XmlRIModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new XmlRIModel();

                var data = _XmlRIRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.NOBUKMSK = data.NOBUKMSK;
						result.TGBUKMSK = data.TGBUKMSK;
						result.KOBAR = data.KOBAR;
						result.QTY = data.QTY;
						result.DownloadDate = data.DownloadDate;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _XmlRIRepository.Count(criteria, whereClause);
        }

        public MessageModel<XmlRIModel> FindAll()
        {
            var message = new MessageModel<XmlRIModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlRIRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new XmlRIModel()
                    {
                        ID = data.ID,
						NOBUKMSK = data.NOBUKMSK,
						TGBUKMSK = data.TGBUKMSK,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<XmlRIModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            var message = new MessageModel<XmlRIModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _XmlRIRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new XmlRIModel()
                    {
                        ID = data.ID,
						NOBUKMSK = data.NOBUKMSK,
						TGBUKMSK = data.TGBUKMSK,
						KOBAR = data.KOBAR,
						QTY = data.QTY,
						DownloadDate = data.DownloadDate,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


