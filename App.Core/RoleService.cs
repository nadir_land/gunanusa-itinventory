﻿using App.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Model;
using App.Repository.Repository;
using NLog;
using App.Repository.Domain;
using App.Utility;

namespace App.Core
{
    public class RoleService : IRoleService
    {
        #region private
        private List<RoleModel> listModel;
        RoleRepository _roleRepository;
        protected Logger logger;
        #endregion

        public RoleService()
        {
            _roleRepository = new RoleRepository();
            listModel = new List<RoleModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<RoleModel> Add(RoleModel model)
        {
            var jsonObj = new MessageModel<RoleModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsRole()
                    {
                        RoleName = model.RoleName                        
                    };
                    if (_roleRepository.Add(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<RoleModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<RoleModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<RoleModel> Delete(RoleModel model)
        {
            var jsonObj = new MessageModel<RoleModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _roleRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_roleRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<RoleModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<RoleModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<RoleModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _roleRepository.FindWithPaging(criteria,whereClause, pageSize, page, "ID");
                foreach (var data in List)
                {
                    listModel.Add(new RoleModel()
                    {
                        ID = data.ID,
                        RoleName = data.RoleName                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<RoleModel> Update(RoleModel model)
        {
            var jsonObj = new MessageModel<RoleModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _roleRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.RoleName = model.RoleName;
                       
                    }
                    if (_roleRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<RoleModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _roleRepository.Count(criteria, whereClause);
        }

        public MessageModel<RoleModel> FindByID(int id)
        {
            var message = new MessageModel<RoleModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new RoleModel();

                var data = _roleRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
                    result.RoleName = data.RoleName;                    

                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<RoleModel> FindAll()
        {
            var message = new MessageModel<RoleModel>();
            try
            {
                message.message = "Ok";
                message.success = true;                
                var List = _roleRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new RoleModel()
                    {
                        ID = data.ID,
                        RoleName = data.RoleName
                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}
