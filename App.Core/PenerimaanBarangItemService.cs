using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class PenerimaanBarangItemService : IPenerimaanBarangItemService
    {
        #region private	
        private List<PenerimaanBarangItemModel> listModel;
        PenerimaanBarangItemRepository _PenerimaanBarangItemRepository;
        protected Logger logger;
        #endregion

        public PenerimaanBarangItemService()
        {
            _PenerimaanBarangItemRepository = new PenerimaanBarangItemRepository();
            listModel = new List<PenerimaanBarangItemModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<PenerimaanBarangItemModel> Add(PenerimaanBarangItemModel model)
        {
            var jsonObj = new MessageModel<PenerimaanBarangItemModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrPenerimaanBarangItem()
                    {
                        ID = model.ID,
						PenerimaanBarangID = model.PenerimaanBarangID,
                        KodeBarang = model.KodeBarang,
						NamaBarang = model.NamaBarang,
						HSImport = model.HSImport,
						Satuan = model.Satuan,
						NoSeri = model.NoSeri,
						TarifBM = model.TarifBM,
						CifPIB = model.CifPIB,
						QtyPIB = model.QtyPIB,
						BmPIB = model.BmPIB,
						PpnPIB = model.PpnPIB,
						QtyRI = model.QtyRI,

                    };
                    int ID = (_PenerimaanBarangItemRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<PenerimaanBarangItemModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<PenerimaanBarangItemModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<PenerimaanBarangItemModel> Update(PenerimaanBarangItemModel model)
        {
            var jsonObj = new MessageModel<PenerimaanBarangItemModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _PenerimaanBarangItemRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.PenerimaanBarangID = model.PenerimaanBarangID;
                        data.KodeBarang = model.KodeBarang;
						data.NamaBarang = model.NamaBarang;
						data.HSImport = model.HSImport;
						data.Satuan = model.Satuan;
						data.NoSeri = model.NoSeri;
						data.TarifBM = model.TarifBM;
						data.CifPIB = model.CifPIB;
						data.QtyPIB = model.QtyPIB;
						data.BmPIB = model.BmPIB;
						data.PpnPIB = model.PpnPIB;
						data.QtyRI = model.QtyRI;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_PenerimaanBarangItemRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<PenerimaanBarangItemModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<PenerimaanBarangItemModel> Delete(PenerimaanBarangItemModel model)
        {
            var jsonObj = new MessageModel<PenerimaanBarangItemModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _PenerimaanBarangItemRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_PenerimaanBarangItemRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<PenerimaanBarangItemModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<PenerimaanBarangItemModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<PenerimaanBarangItemModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _PenerimaanBarangItemRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new PenerimaanBarangItemModel()
                    {
                        ID = data.ID,
						PenerimaanBarangID = data.PenerimaanBarangID,
                        KodeBarang = data.KodeBarang,
                        NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						NoSeri = data.NoSeri,
						TarifBM = data.TarifBM,
						CifPIB = data.CifPIB,
						QtyPIB = data.QtyPIB,
						BmPIB = data.BmPIB,
						PpnPIB = data.PpnPIB,
						QtyRI = data.QtyRI,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<PenerimaanBarangItemModel> FindByID(int id)
        {
            var message = new MessageModel<PenerimaanBarangItemModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new PenerimaanBarangItemModel();

                var data = _PenerimaanBarangItemRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.PenerimaanBarangID = data.PenerimaanBarangID;
                    result.KodeBarang = data.KodeBarang;
						result.NamaBarang = data.NamaBarang;
						result.HSImport = data.HSImport;
						result.Satuan = data.Satuan;
						result.NoSeri = data.NoSeri;
						result.TarifBM = data.TarifBM;
						result.CifPIB = data.CifPIB;
						result.QtyPIB = data.QtyPIB;
						result.BmPIB = data.BmPIB;
						result.PpnPIB = data.PpnPIB;
						result.QtyRI = data.QtyRI;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _PenerimaanBarangItemRepository.Count(criteria, whereClause);
        }

        public MessageModel<PenerimaanBarangItemModel> FindAll()
        {
            var message = new MessageModel<PenerimaanBarangItemModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _PenerimaanBarangItemRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new PenerimaanBarangItemModel()
                    {
                        ID = data.ID,
						PenerimaanBarangID = data.PenerimaanBarangID,
                        KodeBarang = data.KodeBarang,
                        NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						NoSeri = data.NoSeri,
						TarifBM = data.TarifBM,
						CifPIB = data.CifPIB,
						QtyPIB = data.QtyPIB,
						BmPIB = data.BmPIB,
						PpnPIB = data.PpnPIB,
						QtyRI = data.QtyRI,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<PenerimaanBarangItemModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            var message = new MessageModel<PenerimaanBarangItemModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _PenerimaanBarangItemRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new PenerimaanBarangItemModel()
                    {
                        ID = data.ID,
						PenerimaanBarangID = data.PenerimaanBarangID,
                        KodeBarang = data.KodeBarang,
                        NamaBarang = data.NamaBarang,
						HSImport = data.HSImport,
						Satuan = data.Satuan,
						NoSeri = data.NoSeri,
						TarifBM = data.TarifBM,
						CifPIB = data.CifPIB,
						QtyPIB = data.QtyPIB,
						BmPIB = data.BmPIB,
						PpnPIB = data.PpnPIB,
						QtyRI = data.QtyRI,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


