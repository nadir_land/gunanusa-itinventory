﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using Lib;
using App.Model;

namespace App.Core
{ 
    public class ClassMenu
    {

        public ClassMenu()
        {

        }

        public List<MenuModel> getMenu(string userName, string Connection)
        {
            List<MenuModel> list = new List<MenuModel>();
            Class1 lib = new Class1();

            string menu=string.Empty;

            DataTable dt = lib.Query(Connection, @"Select Distinct b.*, a.MenuID from MsRoleMenu a inner join MsMenu b on a.MenuID=b.ID 
                                                where a.RoleID in 
                                                (
	                                                Select RoleID from MsUser a, MsUserRole b where a.ID=b.UserID and
	                                                UserName like '"+userName+"')");

            if (dt.Rows.Count > 0)
            {
                //group
                DataView Group = new DataView(dt, "TypeMenu='G'", "MenuID", DataViewRowState.CurrentRows);
                foreach (DataRowView item in Group)
                {

                    MenuModel mdl = new MenuModel();
                    mdl.Link = item["Link"].ToString();
                    mdl.ClassIcon = item["ClassIcon"].ToString();
                    mdl.DisplayMenu = item["DisplayMenu"].ToString();
                    mdl.TypeMenu = item["TypeMenu"].ToString();
                    mdl.Parent = Convert.ToUInt16(item["Parent"]);

                    //child
                    List<ItemMenu> itemMenu = new List<ItemMenu>();
                    DataView child = new DataView(dt);
                    child.RowFilter = "[Parent]='" + item["MenuID"].ToString() + "'";

                    foreach (DataRowView itm in child)
                    {
                        ItemMenu i = new ItemMenu();
                        i.Link = itm["Link"].ToString();
                        i.ClassIcon = itm["ClassIcon"].ToString();
                        i.DisplayMenu = itm["DisplayMenu"].ToString();

                        itemMenu.Add(i);
                    }

                    mdl.childMenu = itemMenu;

                    list.Add(mdl);
                }

                
                //single

                DataView Link = new DataView(dt, "TypeMenu in ('L','H')", "MenuID", DataViewRowState.CurrentRows);
                foreach (DataRowView item in Link)
                {
                    MenuModel mdl = new MenuModel();
                    mdl.Link = item["Link"].ToString();
                    mdl.ClassIcon = item["ClassIcon"].ToString();
                    mdl.DisplayMenu = item["DisplayMenu"].ToString();
                    mdl.TypeMenu = item["TypeMenu"].ToString();
                    mdl.Parent = Convert.ToUInt16(item["Parent"]);

                    list.Add(mdl);
                }


            }

            return list;
        }

        public List<MenuModel> getMenuByRoleID(string roleid, string Connection)
        {
            List<MenuModel> list = new List<MenuModel>();
            Class1 lib = new Class1();

            string menu = string.Empty;

            DataTable dt = lib.Query(Connection, @"Select Distinct b.*, a.MenuID from MsRoleMenu a inner join MsMenu b on a.MenuID=b.ID 
                                                where a.RoleID in (" + roleid + ")");

            if (dt.Rows.Count > 0)
            {
                //group
                DataView Group = new DataView(dt, "TypeMenu='G'", "MenuID", DataViewRowState.CurrentRows);
                foreach (DataRowView item in Group)
                {

                    MenuModel mdl = new MenuModel();
                    mdl.Link = item["Link"].ToString();
                    mdl.ClassIcon = item["ClassIcon"].ToString();
                    mdl.DisplayMenu = item["DisplayMenu"].ToString();
                    mdl.TypeMenu = item["TypeMenu"].ToString();
                    mdl.Parent = Convert.ToUInt16(item["Parent"]);

                    //child
                    List<ItemMenu> itemMenu = new List<ItemMenu>();
                    DataView child = new DataView(dt);
                    child.RowFilter = "[Parent]='" + item["MenuID"].ToString() + "'";

                    foreach (DataRowView itm in child)
                    {
                        ItemMenu i = new ItemMenu();
                        i.Link = itm["Link"].ToString();
                        i.ClassIcon = itm["ClassIcon"].ToString();
                        i.DisplayMenu = itm["DisplayMenu"].ToString();

                        itemMenu.Add(i);
                    }

                    mdl.childMenu = itemMenu;

                    list.Add(mdl);
                }


                //single

                DataView Link = new DataView(dt, "TypeMenu in ('L','H')", "MenuID", DataViewRowState.CurrentRows);
                foreach (DataRowView item in Link)
                {
                    MenuModel mdl = new MenuModel();
                    mdl.Link = item["Link"].ToString();
                    mdl.ClassIcon = item["ClassIcon"].ToString();
                    mdl.DisplayMenu = item["DisplayMenu"].ToString();
                    mdl.TypeMenu = item["TypeMenu"].ToString();
                    mdl.Parent = Convert.ToUInt16(item["Parent"]);

                    list.Add(mdl);
                }


            }

            return list;
        }


    }

}

