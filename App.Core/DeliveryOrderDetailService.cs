using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using App.Core.Interface;
using App.Model;
using App.Repository.Domain;
using App.Repository.Repository;
using App.Utility;
using NLog;

namespace App.Core
{

    public class DeliveryOrderDetailService : IDeliveryOrderDetailService
    {
        #region private	
        private List<DeliveryOrderDetailModel> listModel;
        DeliveryOrderDetailRepository _DeliveryOrderDetailRepository;
        protected Logger logger;
        #endregion

        public DeliveryOrderDetailService()
        {
            _DeliveryOrderDetailRepository = new DeliveryOrderDetailRepository();
            listModel = new List<DeliveryOrderDetailModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }

        public MessageModel<DeliveryOrderDetailModel> Add(DeliveryOrderDetailModel model)
        {
            var jsonObj = new MessageModel<DeliveryOrderDetailModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new TrDeliveryOrderDetail()
                    {
                        ID = model.ID,
						DOID = model.DOID,
						KodeFG = model.KodeFG,
						NamaFG = model.NamaFG,
						HSExport = model.HSExport,
						Satuan = model.Satuan,
						QtyKirim = model.QtyKirim,
						HargaSatuan = model.HargaSatuan,
						JumlahHarga = model.JumlahHarga,

                    };
                    int ID = (_DeliveryOrderDetailRepository.Add(data));
                    model.ID = ID;
                    if (ID <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<DeliveryOrderDetailModel>()
                {
                    success = success,
                    message = message,
                    data = model,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<DeliveryOrderDetailModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<DeliveryOrderDetailModel> Update(DeliveryOrderDetailModel model)
        {
            var jsonObj = new MessageModel<DeliveryOrderDetailModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _DeliveryOrderDetailRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.ID = model.ID;
						data.DOID = model.DOID;
						data.KodeFG = model.KodeFG;
						data.NamaFG = model.NamaFG;
						data.HSExport = model.HSExport;
						data.Satuan = model.Satuan;
						data.QtyKirim = model.QtyKirim;
						data.HargaSatuan = model.HargaSatuan;
						data.JumlahHarga = model.JumlahHarga;

                        //data.ProjectID = model.ProjectID;
                        
                    }
                    if (_DeliveryOrderDetailRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<DeliveryOrderDetailModel>()
                {
                    success = success,
                    data = model,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<DeliveryOrderDetailModel> Delete(DeliveryOrderDetailModel model)
        {
            var jsonObj = new MessageModel<DeliveryOrderDetailModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _DeliveryOrderDetailRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_DeliveryOrderDetailRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<DeliveryOrderDetailModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<DeliveryOrderDetailModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            listModel = new List<DeliveryOrderDetailModel>();
            var message = new MessageModel<DeliveryOrderDetailModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _DeliveryOrderDetailRepository.FindWithPaging(criteria, whereClause, pageSize, page, orderBy);
                foreach (var data in List)
                {
                    listModel.Add(new DeliveryOrderDetailModel()
                    {
                        ID = data.ID,
						DOID = data.DOID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						Satuan = data.Satuan,
						QtyKirim = data.QtyKirim,
						HargaSatuan = data.HargaSatuan,
						JumlahHarga = data.JumlahHarga,

                        //sample
                        //ID = data.ID,
                        
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<DeliveryOrderDetailModel> FindByID(int id)
        {
            var message = new MessageModel<DeliveryOrderDetailModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new DeliveryOrderDetailModel();

                var data = _DeliveryOrderDetailRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
						result.DOID = data.DOID;
						result.KodeFG = data.KodeFG;
						result.NamaFG = data.NamaFG;
						result.HSExport = data.HSExport;
						result.Satuan = data.Satuan;
						result.QtyKirim = data.QtyKirim;
						result.HargaSatuan = data.HargaSatuan;
						result.JumlahHarga = data.JumlahHarga;

                    //sample
                    //result.ID = data.ID;                    
                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _DeliveryOrderDetailRepository.Count(criteria, whereClause);
        }

        public MessageModel<DeliveryOrderDetailModel> FindAll()
        {
            listModel = new List<DeliveryOrderDetailModel>();
            var message = new MessageModel<DeliveryOrderDetailModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _DeliveryOrderDetailRepository.FindAll();
                foreach (var data in List)
                {
                    listModel.Add(new DeliveryOrderDetailModel()
                    {
                        ID = data.ID,
						DOID = data.DOID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						Satuan = data.Satuan,
						QtyKirim = data.QtyKirim,
						HargaSatuan = data.HargaSatuan,
						JumlahHarga = data.JumlahHarga,


                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<DeliveryOrderDetailModel> FindByCriteria(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<DeliveryOrderDetailModel>();
            var message = new MessageModel<DeliveryOrderDetailModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _DeliveryOrderDetailRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    listModel.Add(new DeliveryOrderDetailModel()
                    {
                        ID = data.ID,
						DOID = data.DOID,
						KodeFG = data.KodeFG,
						NamaFG = data.NamaFG,
						HSExport = data.HSExport,
						Satuan = data.Satuan,
						QtyKirim = data.QtyKirim,
						HargaSatuan = data.HargaSatuan,
						JumlahHarga = data.JumlahHarga,

                    });
                }
                message.total = List.Count();
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<DeliveryOrderDetailModel> Report(string criteria, List<WhereClause> whereClause, string sortby)
        {
            listModel = new List<DeliveryOrderDetailModel>();
            var message = new MessageModel<DeliveryOrderDetailModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var List = _DeliveryOrderDetailRepository.FindByCriteria(criteria, whereClause, sortby);
                foreach (var data in List)
                {
                    string NoDO = "";
                    string tglDO = "";
                    DeliveryOrderRepository repDO = new DeliveryOrderRepository();
                    var doData = repDO.FindByID(data.DOID);
                    if (doData != null)
                    {
                        NoDO = doData.NoDO;
                        tglDO = doData.TglDO;
                    }

                    decimal quantityEquivalent = 0;
                    decimal nilai = 0;
                    string kodeBarang = "";
                    string namaBarang = "";
                    string satuanBarang = "";
                    KonversiProductService konversiProductService = new KonversiProductService();
                    List<WhereClause> wh = new List<WhereClause>();
                    wh.Add(new WhereClause { Property = "KodeFG", Value = data.KodeFG });
                    wh.Add(new WhereClause { Property = "Status", Value = "Approve" });
                    var konversiDatas = konversiProductService.FindByCriteria("KodeFG=@KodeFG and Status=@Status", wh, "ID").rows.ToList();
                    if (konversiDatas.Count > 0)
                    {
                        var konversiData = konversiDatas[0];
                        quantityEquivalent = (data.QtyKirim / konversiData.Kandungan) * konversiData.Waste;
                        kodeBarang = konversiData.KodeBarang;
                        namaBarang = konversiData.NamaBarang;
                        satuanBarang = konversiData.Satuan;

                        #region Waste
                        decimal tDOQtyWaste = quantityEquivalent + konversiData.Waste;
                        #endregion

                        IPenerimaanBarangService penerimaanBarangService = new PenerimaanBarangService();
                        List<WhereClause> wh2 = new List<WhereClause>();
                        wh2.Add(new WhereClause { Property = "KodeBarang", Value = konversiData.KodeBarang });
                        wh2.Add(new WhereClause { Property = "Status", Value = "Approve" });
                        var penerimaanBarang = penerimaanBarangService.FindByCriteria("Status=@Status and KodeBarang=@KodeBarang", wh2, "ID").rows.ToList();
                        if (penerimaanBarang.Count > 0)
                        {
                            //decimal hargaSatuan = penerimaanBarang[0].BmPIB / penerimaanBarang[0].QtyPIB;
                            //nilai = data.QtyKirim * hargaSatuan;

                            decimal SisaSaldoQty = 0;
                            decimal bmPIB = 0;
                            decimal qtyPIB = 0;
                            foreach (var p in penerimaanBarang)
                            {
                                bmPIB = p.BmPIB;
                                qtyPIB = p.QtyPIB;
                                SisaSaldoQty = p.QtyPIB - tDOQtyWaste;
                                if (SisaSaldoQty > 0)
                                {
                                    break;
                                }
                            }

                            decimal hargaSatuan = bmPIB / qtyPIB;
                            nilai = quantityEquivalent * hargaSatuan;
                        }

                    }

                    listModel.Add(new DeliveryOrderDetailModel()
                    {
                        ID = data.ID,
                        dNoDO = NoDO,
                        dTglDO = tglDO,
                        KodeFG = data.KodeFG,
                        NamaFG = data.NamaFG,
                        HSExport = data.HSExport,
                        Satuan = data.Satuan,
                        QtyKirim = data.QtyKirim,
                        HargaSatuan = data.HargaSatuan,
                        JumlahHarga = data.JumlahHarga,
                        QuantityEquivalent = quantityEquivalent,
                        Nilai = nilai,
                        KodeBarang = kodeBarang,
                        NamaBarang= namaBarang,
                        SatuanBarang= satuanBarang,
                    });
                }
                message.total = List.Count();
                message.rows = listModel.OrderByDescending(p => p.dTglDO).ToList();
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }
    }
}


