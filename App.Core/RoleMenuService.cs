﻿using App.Core.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Model;
using App.Repository.Repository;
using System.Configuration;
using App.Repository.Domain;
using NLog;
using App.Utility;

namespace App.Core
{
    public class RoleMenuService : IRoleMenuService
    {
        #region private
        private List<RoleMenuModel> listModel;
        private RoleMenuRepository _roleMenuRepository;
        private ClassMenu _menu;
        protected Logger logger;
        #endregion
        public RoleMenuService()
        {
            _roleMenuRepository = new RoleMenuRepository();
            _menu = new ClassMenu();
            listModel = new List<RoleMenuModel>();
            logger = App.Utility.CampusLogger.GetLogger();
        }
        public List<RoleMenuModel> GetMenu()
        {
            List<RoleMenuModel> list = new List<RoleMenuModel>();

            var data = _roleMenuRepository.FindByID(1);
            if (data != null)
            {
                list.Add(new RoleMenuModel
                {
                    RoleID = data.RoleID,
                    RoleName = data.Role.RoleName,
                    DisplayMenu=data.Menu.DisplayMenu,
                    Link = data.Menu.Link,
                    Icon = data.Menu.ClassIcon,
                    Parent = data.Menu.Parent,
                    TypeMenu = data.Menu.TypeMenu
                    
                });
            }

            return list;            
        }

        public List<MenuModel> GetMenuByUserId(string uname)
        {
            return _menu.getMenu(uname, ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }

        public List<MenuModel> GetMenuByRoleId(string roleid)
        {
            return _menu.getMenuByRoleID(roleid, ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        }


        public MessageModel<RoleMenuModel> Add(RoleMenuModel model)
        {
            var jsonObj = new MessageModel<RoleMenuModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model != null && model.IsValid())
                {
                    var data = new MsRoleMenu()
                    {
                        MenuID = model.MenuID,
                        RoleID = model.RoleID
                        
                    };
                    if (_roleMenuRepository.Add(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation error";
                }

                jsonObj = new MessageModel<RoleMenuModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj = new MessageModel<RoleMenuModel>()
                {
                    success = false,
                    message = "System failed"
                };
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<RoleMenuModel> Update(RoleMenuModel model)
        {
            var jsonObj = new MessageModel<RoleMenuModel>();
            try
            {
                var message = "Save success";
                var success = true;

                if (model.IsValid())
                {
                    var data = _roleMenuRepository.FindByID(model.ID);

                    if (data != null)
                    {
                        data.MenuID = model.MenuID;
                        data.RoleID = model.RoleID;                        

                    }
                    if (_roleMenuRepository.Update(data) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Validation failed";
                }
                jsonObj = new MessageModel<RoleMenuModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<RoleMenuModel> Delete(RoleMenuModel model)
        {
            var jsonObj = new MessageModel<RoleMenuModel>();
            try
            {
                var message = "Data Deleted successfully";
                var success = true;

                var data = _roleMenuRepository.FindByID(model.ID);

                if (data != null)
                {
                    if (_roleMenuRepository.Remove(data.ID) <= 0)
                    {
                        success = false;
                        message = "System failed to update data!";
                    };
                }
                else
                {
                    success = false;
                    message = "Data not found!";
                }

                jsonObj = new MessageModel<RoleMenuModel>()
                {
                    success = success,
                    message = message,
                    errors = model.GetErrors()
                };
            }
            catch (Exception e)
            {
                jsonObj.success = false;
                jsonObj.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return jsonObj;
        }

        public MessageModel<RoleMenuModel> FindDataWithPaging(string criteria, List<WhereClause> whereClause, int pageSize, int page, string orderBy)
        {
            var message = new MessageModel<RoleMenuModel>();
            try
            {
                message.message = "Ok";
                message.success = true;
                var sort = orderBy;
                var List = _roleMenuRepository.FindWithPaging(criteria, whereClause, pageSize, page, "ID");
                foreach (var data in List)
                {
                    listModel.Add(new RoleMenuModel()
                    {
                        ID = data.ID,
                        RoleName = data.Role.RoleName,
                        DisplayMenu = data.Menu.DisplayMenu
                    });
                }
                message.total = Count(criteria, whereClause);
                message.rows = listModel;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        public MessageModel<RoleMenuModel> FindByID(int id)
        {
            var message = new MessageModel<RoleMenuModel>();
            message.message = "Ok";
            message.success = true;
            try
            {
                var result = new RoleMenuModel();

                var data = _roleMenuRepository.FindByID(Convert.ToInt32(id));
                if (data != null)
                {
                    result.ID = data.ID;
                    result.RoleID = data.RoleID;
                    result.MenuID = data.MenuID;
                    result.RoleName = data.Role.RoleName;
                    result.DisplayMenu = data.Menu.DisplayMenu;

                }
                message.total = 1;
                message.data = result;
            }
            catch (Exception e)
            {
                message.success = false;
                message.message = "System failed";
                logger.Error(e, "Got exception.");
            }
            return message;
        }

        private int Count(string criteria, List<WhereClause> whereClause)
        {
            return _roleMenuRepository.Count(criteria, whereClause);
        }

    }
}
